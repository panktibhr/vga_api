const express = require("express");
const moment = require('moment');
const router = express.Router();
const multer = require("multer");
const multerS3 = require("multer-s3");
var storage = multer.memoryStorage();
const upload = multer({ storage });
// const upload = multer({ dest: '/tmp/' });
const Promise = require("bluebird");
const uuidv4 = require("uuid/v4");
const AWS = require("aws-sdk");
const fs = require("fs");
var jwt = require('jsonwebtoken');

const user = require("./models/user").default;
const box = require("./models/box").default;
const account = require("./models/account").default;
const accountType = require("./models/account-type").default;
const boxType = require("./models/box-type").default;
const category = require("./models/category").default;
const role = require("./models/role").default;
const customer = require("./models/customer").default;
const brand = require("./models/brand").default;
const BrandDetail = require('./models/brand-detail').default;
var generator = require("generate-password");
var path = require('path')

/*
  Added By : Belani Jaimin
  Added Date: 01/09/2019
  Description : To check consult flag is on or not to kiosk side
*/

const user_consult = require('./models/user_consult').default;
const user_consult_notifications= require('./models/user_consult_notifications').default;
const user_consult_master = require('./models/user_consult_master').default;
const prescriptions_master = require('./models/prescriptions_master').default;
const prescription_detail = require('./models/prescription_detail').default;
const box_schedule = require('./models/box-schedule').default;
const RXSTATUS = require('./models/email-service/rx-status').default;
const sendAWSEmail = require('./models/email-service/aws-template').default;
import BRAND_TYPES from './models/enums/brand-type-enum';
const events = require('./models/events/events').default;
const customer_type = require('./models/customer_type').default;
const customer_type_enum = require('./models/enums/customer_type_enum').default;
const EventType = require('./models/enums/event-type').default;
const PermissionDetail = require('./models/rolepermissions/role_permission_detail').default;
const boxExpansions = require("./models/box-expansions").default;
const AccountConnection = require('./models/account-connections').default;
import QRCode from 'qrcode';
const AWSconfig = require('./models/email-service/aws-config').default;
const gatherEmailConfig = require('./models/email-service/emailInfo').default;
const sendAWSEmailHoldStatus = require('./models/email-service/email-hold-prescription-status').default;
const event_type = require("./models/events/event-type").default;
const ConnectionType = require('./models/enums/connection-type').default;
const customer_collect_detail= require('./models/customer-collect-detail').default;
const sendAWSEmailKioskOffline = require('./models/email-service/email-kiosk-offline').default;
const sendAWSSMS = require('./models/email-service/sms-prescription-status').default;
const AccountConnectionDetails = require('./models/account-connection-details').default;
const prescriptionNotification = require('./models/email-service/pickup_ses').default;
const sendAWSEmailpickUpReminder = require('./models/email-service/pickUp_reminder_notification').default;
const HoursOfOperation = require('./models/hours_of_operation').default;
const BoxTypeEnum = require('./models/enums/box_type').default;
import { EventLogHandlerModule, sendOperationsEmail } from './models/events/event-log-handler';
const sendAWSEmailRemoveRxStatus = require('./models/email-service/email-remove-prescription').default;
const Health_and_information_master = require('./models/Health_and_information_master').default;
const QuizEnroll = require('./models/quiz_enroll').default;
const createAWSCognitoUser  = require('./common/createAWSCognitoUser').default;
import {checkEmailExist} from './common/checkUserExistOrNot';
const congnitoUserType = require('./common/AWSCognitoUserType').default;
// const rxExpirationMail = require('../src/sendRxExpirationMail').default;
const thirdPartyPHIs = require('../src/models/thirdpartyphis').default;
const LegalAgreement = require('./models/legal_agreement_compliance').default;
const CustomerLegalDocs = require('./models/customer_legaldocs').default;
const manual_user_consult = require('./models/manual_user_consult').default;
// const rxExpirationMail = require('./common/AWSLambdaFunction').default;

/*
  Added By : Belani Jaimin
  Added Date: 08/30/2019
  Description : Pioneer Rx RxComplete Transactions Model files
*/

const _pioneerSalesTrans = require('../src/models/pioneer-sales-trans');
const _pioneerSalesTransDetails = require('../src/models/pioneer-Sales-trans-details');
const _pioneerSalesTransErrorLogHistory = require('../src/models/pioneer-sales-trans-error-log-history');
const soapActions = require('./models/enums/Soap_actions');
// const rxExpirationMail =require('../src/sendRxExpirationMail');

/*
  Added By : Varshit Shah
  Added Date : 10-02-2019
  Desc: DI-841-Save Transaction history
*/

const POSTransaction = require('./models/pos_transaction').default;
const POSTransactionDetail = require('./models/pos_transaction_details').default;

/*
  Added By : Varshit Shah
  Added Date : 12-19-2019
  Desc: Epic RxComplete 
*/

const soapRequest = require('easy-soap-request');
const _phisresponses = require('./models/phis-responses').default;
const BoxExpansion = require('./models/box-expansions').default;
AWS.config.setPromisesDependency(Promise);
Promise.promisifyAll(fs);
const axios = require('axios');

AWS.config.update({
  accessKeyId: process.env.NODE_AWS_ACCESS_KEY,
  secretAccessKey: process.env.NODE_AWS_SECRET_ACCESS_KEY,
  region: process.env.NODE_AWS_REGION
});


const cisp = new AWS.CognitoIdentityServiceProvider();
const s3 = new AWS.S3({ region: process.env.NODE_AWS_REGION });
const { epicStatus } = require('./models/enums/epic-status');
const sendAWSpatientResetPasswordMail = require('./models/email-service/resetpassword').default;
const notifications = require('./models/notifications').default;
const Enum_CounselingStatus = require('./models/enums/counseling_status').default;
const paymentModel = require('./models/payments').default;
const LocationTypeMaster = require('./models/locationtypemaster').default;
const Customer_Locations_master = require('./models/customer_locations_master').default;
const GbpConfig = require('./models/gbpconfig').default;
const Crypter = require('cryptr');
const BitlyClient = require('bitly').BitlyClient;
const bitlys = new BitlyClient(process.env.BIYLY_ACCESS_TOKEN, {});
const { signAuth, verifyAuth, decodePayload } = require('./auth');
const sendNewPickupNotification = require('./models/email-service/newPickup_notification').default;
const sendAssignNotificationEmail = require('./models/email-service/preparing_prescription_notification').default;
const Templates = require('./models/templates').default;
const sendPickUpReceipt = require('./models/email-service/pickUp_Receipt').default;
const user_password_history = require('./models/user_password_history').default;
const customer_password_history = require('./models/customer_password_history').default;
const AlternetAgents = require('./models/alternet_agents').default;
const AccountContactPersons = require('./models/account_contact_persons').default;
const UserRoles = require('./models/user_roles').default;
const LocationsType = require('./models/enums/location_type').default;
const SendOnlineNotificationForBox = async (theBox) => {
  const boxWithDetails = await box.query()
      .innerJoin('accounts', 'accounts.id', 'boxes.parent_account_id')
      .innerJoin('contacts', 'contacts.id', 'accounts.contact_id')
      .innerJoin('brands', 'brands.id', 'accounts.brand_id')
      .innerJoin('addresses', 'addresses.id', 'boxes.address_id')
      .select('boxes.id', 'accounts.id as account_id','boxes.name as box_name', 'boxes.parent_account_id', 'boxes.type_id', 'boxes.address_id', 'boxes.kiosk_box_id', 'boxes.is_email_send', 'boxes.email_send_date', 'boxes.last_access_date',
        'accounts.picture_url', 'contacts.name', 'contacts.email', 'contacts.phone',
        'addresses.address_line_1', 'addresses.address_line_2', 'addresses.city', 'addresses.state', 'addresses.zip', 'brands.email as support_pharmacy_email')
      .where('boxes.id', theBox.id)
      .where('boxes.IsActive', true)
      .whereNotNull('boxes.last_access_date')
      .orderBy('boxes.last_access_date', 'asc');
      if(boxWithDetails[0].address_line_2 === 'undefined') {
        boxWithDetails[0].address_line_2 = '';
      }
      if(boxWithDetails && boxWithDetails[0]) {   
          const contactPerson = await AccountContactPersons.query().where('account_id', boxWithDetails[0].account_id).andWhere('isdefault', true);
          if(contactPerson && contactPerson.length > 0) {
            let EmailAddresses = [];
            let sms = [];
            for(let j=0; j<contactPerson.length; j++) {
              EmailAddresses.push(contactPerson[j].email);
              sms.push(contactPerson[j].phone);
            }
            boxWithDetails[0].ContactCCEmails = EmailAddresses;
            boxWithDetails[0].ContactSMS = sms;
          }
          else{
            boxWithDetails[0].ContactCCEmails = [];
            boxWithDetails[0].ContactSMS = [];
          }
      }
      let currentDate = new Date();
      let boxAddress = `${boxWithDetails[0].address_line_1}
      ${boxWithDetails[0].address_line_2}`;
      boxAddress = boxAddress.trim();
      boxAddress += `
    ${boxWithDetails[0].city}, ${boxWithDetails[0].state} ${boxWithDetails[0].zip}`;
      if (boxWithDetails[0].phone !== null && boxWithDetails[0].phone !== undefined && boxWithDetails[0].phone !== '' && boxWithDetails[0].phone !== 'undefined') {
        /* SMS Info starts */
        const PhoneNumber = '+1' + ReplacePhone(boxWithDetails[0].phone);
        // const PhoneNumber = '+19497677339';
        const Message = 
        `Box ${boxWithDetails[0].box_name} is online on ${getDateTime(currentDate)}
    Address:
    ${boxAddress}`;
      // PhoneNumber: ${ReplacePhone(boxWithDetails[0].phone)}`
        sendAWSSMS(PhoneNumber, Message);
        let SMSArray = boxWithDetails[0].ContactSMS;
        if(SMSArray && SMSArray.length > 0) {
          for(let k=0; k<SMSArray.length; k++) {
            const PhoneNumber = '+1' + ReplacePhone(SMSArray[k]);
            sendAWSSMS(PhoneNumber, Message);
          }            
        }
        /* SMS Info ends */
      }
      if (boxWithDetails[0].email !== null && boxWithDetails[0].email !== undefined && boxWithDetails[0].email !== '' && boxWithDetails[0].email !== 'undefined') {
        let Config = {
          firstName: boxWithDetails[0].name,
          lastName: boxWithDetails[0].name,
          PharmacyemailId: boxWithDetails[0].email,
          // emailId:'info@ilocalbox.com',
          emailId: boxWithDetails[0].email,
          boxName: boxWithDetails[0].box_name,
          boxAddress: boxAddress,
          kiosk_box_id: boxWithDetails[0].kiosk_box_id,
          last_access_date: getDateTime(currentDate),
          picture_url: boxWithDetails[0].picture_url ? boxWithDetails[0].picture_url : 'https://s3.amazonaws.com/rpx-dev-ext-images-qr/NoImageFound.png',
          supportPharmacyEmail: boxWithDetails[0].support_pharmacy_email,
          status: 'online',
          ccEmails: boxWithDetails[0].ContactCCEmails
        };
        sendAWSEmailKioskOffline(Config);
        }
};

const CheckAndSendOnlineNotification = async (theBox) => {
  if (theBox.email_send_date !== null) {
    // Send online sms and email notification
    await SendOnlineNotificationForBox(theBox);
  }
};

router.use(async (req, res, next) => {
  // Log all the requests
  // Or validate permissions
  // console.log(req.method, req.url);

  // continue to next middleware
  next();
});

// For common param validation
// router.param('id', async(req, res, next, id) => {
//     // If required validate id here

//     // Enrich req if required

//     // go to the next thing
//     next();
// });


// Routes
router.get("/users", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    const accountIds = await account.query()
    .select('id')
    .where('account_lineage', '<@', u.parentAccount.account_lineage);

    const users = await user.query()
    .where('parent_account_id', 'in', accountIds.map(x => x.id)).where ('is_enabled',true).eager("[roles, contact, parentAccount]");
    
    res.send(users);
  }
});

router.get("/boxes", async (req, res, next) => {
  // console.log(user);
  // const boxes = await box.query().eager("[address, type]");
  // res.send(boxes);

  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    const accountIds = await account.query()
    .select('id')
    .where('account_lineage', '<@', u.parentAccount.account_lineage);

    const boxes = await box.query()
    .where('parent_account_id', 'in', accountIds.map(x => x.id))
    .where('IsActive',true).andWhere('is_deleted','!=','true').eager("[address, type, schedule,hoursof_operations]");

    res.send(boxes);
  }

});


router.get("/box-types", async (req, res, next) => {
  // console.log(user);
  const boxTypes = await boxType.query().orderBy("name");
  res.send(boxTypes);
});

router.get("/categories", async (req, res, next) => {
  const categories = await category.query().orderBy("name");
  res.send(categories);
});

router.get("/account-types", async (req, res, next) => {
  // console.log(user);
  const accountTypes = await accountType.query().orderBy("created_on");
  res.send(accountTypes);
});

router.get("/accounts", async (req, res, next) => {

  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);


    const u = await user.query().where('username', decodedToken.email).eager("[roles, contact, parentAccount]").first();


    const accounts = await account
      .query()
      .whereRaw(`account_lineage <@ '${u.parentAccount.account_lineage}'`)
      .andWhere('is_deleted','!=','true')
      .eager("[parent, type, contact, contact.address, brand, brand_detail,connection,connection_details,hoursof_operations,health_and_informations,legal_compliance]")
      .orderBy("name");
    res.send(accounts);
  }
});

router.get("/customers", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    const accountIds = await account.query()
    .select('id')
    .where('account_lineage', '<@', u.parentAccount.account_lineage);

    const customers = await customer.query()
    .where('parent_account_id', 'in', accountIds.map(x => x.id)).andWhere('is_deleted', false).andWhere('is_enabled', true).eager("[roles, contacts, legaldocs, customerType,customerLocations,address,alternetAgents]")
    .orderBy('created_on','desc');

    res.send(customers);
  }
});

router.get("/users/:id", async (req, res, next) => {
  // console.log(user);
  const users = await user
    .query()
    .findById(req.params.id)
    .eager("[roles, contact]");  
  res.send(users);
});

router.get("/roles", async (req, res, next) => {
  // console.log(user);
  const roles = await role.query();
  res.send(roles);
});

router.delete("/customers/:id", async (req, res, next) => {
  // console.log(user);
  const idsToDelete = [];

  if (req.params.id.indexOf(",") > -1) {
    req.params.id.split(",").map(x => idsToDelete.push(x));
  } else {
    idsToDelete.push(req.params.id);
  }
  const responses = [];
  const promisesToWait = [];
  console.log(idsToDelete);

  idsToDelete.forEach(element => {
    try {
      promisesToWait.push(customer.query().where('id', element).patch({ is_deleted: true, deleted_on: new Date() }));
    } catch (error) {
      responses.push({
        message: error.message,
        stack: error.stack
      });
    }

    try {
      const cognitoParam = {
        Username: element,
        UserPoolId: process.env.PATIENT_POOL_ID
      };
      promisesToWait.push(cisp.adminDisableUser(cognitoParam).promise());
    } catch (error) {
      responses.push({
        message: error.message,
        stack: error.stack
      });
    }
  });

  await Promise.all(promisesToWait).catch(error => {
    res.send(error);
  });
  res.send(responses);
});

router.delete("/users/:id", async (req, res, next) => {
  // console.log(user);
  const idsToDelete = [];

  if (req.params.id.indexOf(",") > -1) {
    req.params.id.split(",").map(x => idsToDelete.push(x));
  } else {
    idsToDelete.push(req.params.id);
  }
  const responses = [];
  const promisesToWait = [];
  console.log(idsToDelete);
  idsToDelete.forEach(element => {
    try {
      promisesToWait.push(user.query().where('id', element).patch({ is_deleted: true, deleted_on: new Date() }));
    } catch (error) {
      responses.push({
        message: error.message,
        stack: error.stack
      });
    }

    try {
      const cognitoParam = {
        Username: element,
        UserPoolId: process.env.USER_POOL_ID
      };
      promisesToWait.push(cisp.adminDisableUser(cognitoParam).promise());
    } catch (error) {
      responses.push({
        message: error.message,
        stack: error.stack
      });
    }
  });

  await Promise.all(promisesToWait).catch(error => {
    res.send(error);
  });
  res.send(responses);
});

router.delete("/accounts/:id", async (req, res, next) => {
  // console.log(user);
  const idsToDelete = [];

  if (req.params.id.indexOf(",") > -1) {
    req.params.id.split(",").map(x => idsToDelete.push(x));
  } else {
    idsToDelete.push(req.params.id);
  }
  const responses = [];
  const promisesToWait = [];
  console.log(idsToDelete);
  idsToDelete.forEach(element => {
    try {
      const deleteac ={
        id: element,
        is_deleted: true,
        deleted_on: new Date()
      }
      promisesToWait.push(account.query().upsertGraphAndFetch(deleteac));
    } catch (error) {
      responses.push({
        message: error.message,
          stack: error.stack
      });
    }
  });

  await Promise.all(promisesToWait).catch(error => {
    res.send(error);
  });
  res.send(responses);
});

router.delete("/boxes/:id", async (req, res, next) => {
    // console.log(user);
    const idsToDelete = [];

    if (req.params.id.indexOf(",") > -1) {
      req.params.id.split(",").map(x => idsToDelete.push(x));
    } else {
      idsToDelete.push(req.params.id);
    }
    const responses = [];
    const promisesToWait = [];
    const isBoxoccupy= [];
    console.log(idsToDelete);
    let is_Boxoccupy = false;
    for(let i=0 ; i<idsToDelete.length; i++){
      const details =  await prescriptions_master.query()
      // .select('prescriptions_master.id','prescriptions_master.bin_id','prescriptions_master.type_id','prescriptions_master.box_id')
      .where('box_id',idsToDelete[i])
      .whereNotNull('bin_id')
      .andWhere('type_id', 'in', [RXSTATUS.ASSIGNED, RXSTATUS.STOCKED, RXSTATUS.HOLD])
      if(details && details.length > 0){
        isBoxoccupy.push(details);
      }      
    }
    if(isBoxoccupy && isBoxoccupy.length > 0){
      is_Boxoccupy = true;
    }else{    
    idsToDelete.forEach(element => {
      try { 
          const deletebox ={
            id: element,
            is_deleted: true,
            deleted_on: new Date()
          }
          promisesToWait.push(box.query().upsertGraphAndFetch(deletebox));
          // promisesToWait.push(box.query().deleteById(element));        
      } catch (error) {
        responses.push({
          message: error.message,
          stack: error.stack
        });
      }
    });
    is_Boxoccupy = false;
  }

    await Promise.all(promisesToWait).catch(error => {
      res.send(error);
    });
    // res.send(responses,is_Boxoccupy);
    res.send({ res: is_Boxoccupy });
  });

router.post("/customers", upload.any(), async (req, res, next) => {
  var s3response = { Location: "" };
  var s3ConsentLocation = [];
  var s3Location = {};

  if (req.files.length > 0 && req.files[0].buffer) {
    
    for(let i=0;i<req.files.length;i++){
      let params = {
        Bucket: process.env.IMAGES_BUCKET,
        Key: uuidv4(),
        Body: req.files[i].buffer,
        ACL: "public-read",
        ContentType: req.files[i].mimetype
      };
      if(req.files[i].fieldname==='file'){
      s3response = await s3.upload(params).promise();
      delete req.files[i].buffer;
      }
      else if(req.files[i].fieldname==='LC'){
        s3Location = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3ConsentLocation.push(s3Location);
      }
    }
  }

  //Modified By : Belani Jaimin. To add Customer Type as 'Portal Customers' in Manage Patients Module.
  let customer_type_id = null; 
  const cus_type = await customer_type.query().where('name', customer_type_enum.Portal_Customer).first();
  if(cus_type && cus_type!==undefined) {
    customer_type_id = cus_type.id;
  }


  const userObject = {
    name: `${req.body.first_name} ${req.body.last_name}`,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    username: req.body.email,
    parentAccount: { "#dbRef": req.body.parent_account_id },
    picture_url: s3response.Location,
    ada_compitible: req.body.ada_compitible,
    sms_notification: req.body.smsnotification,
    email_notification: req.body.emailnotification,
    push_notification: req.body.pushnotification,
    notes: req.body.patient_notes,
    box_id: (req.body.boxid !== "undefined" && req.body.boxid !== undefined && req.body.boxid !== "null") ? req.body.boxid :null,
    //dob: req.body.dob,
    dob_str: req.body.dob,
    password_reset_on: new Date(),
    contacts: [
      {
        name: "Home",
        phone: req.body.phone,
        email: req.body.email
      }
    ],
    type_id: customer_type_id,
    patient_id: req.body.patient_id,
    address: {
      name: "patient_address",
      address_line_1: req.body.address_line_1,
      address_line_2: req.body.address_line_2,
      city: req.body.city,
      state: req.body.state,
      zip: req.body.zip,
      country: req.body.country,
    }
  };
   let boxDetail;
  if(req.body.boxid !== "undefined" && req.body.boxid !== undefined && req.body.boxid !== "null"){
     boxDetail = await box.query().where('id',req.body.boxid).eager("[address]").first();
  }
  console.log(req.body.roles);
  if (req.body.roles) {
    req.body.roles.split(",").forEach(element => {
      userObject.roles.push({ "#dbRef": element });
    });
  }
  try {
    // let emailId_isExist = false;
    const accountId = await account.query()
    .innerJoin('account_connections','account_connections.id','accounts.account_connections_id')
    .where('accounts.id',req.body.parent_account_id)
    .whereNotIn('account_connections.id',[ConnectionType.ILOCALBOX_PHARMACY])
    .select('accounts.id');
    if(accountId.length > 0)
    {
      const userParams={
        username : userObject.username,
        name : userObject.name,
        email : req.body.email,
        phone : req.body.phone,
        parent_account_id : req.body.parent_account_id,
        patientName : userObject.name,
        boxName : boxDetail && boxDetail.name,
        first_name : req.body.first_name,
        boxAddress : boxDetail && boxDetail.address ?
      (boxDetail.address.address_line_1 + ' '
        + boxDetail.address.address_line_2 + ','
        + boxDetail.address.city + ', '
        + boxDetail.address.state + ' '
        + boxDetail.address.zip + '') : '',
      }
      const userRes = await createAWSCognitoUser(userParams);
      console.log(JSON.stringify(userRes));
    }
    let customerDetail;
      customerDetail = await customer.query().insertGraphAndFetch(userObject);

      if(s3ConsentLocation.length === 0){
        const consentFormObj = {
          parent_account_id: req.body.parent_account_id,
          customer_id: customerDetail.id,
          is_docmanual_policy_accepted: false,
          is_active: req.body.IsActive, 
        }
        const consent_form = await CustomerLegalDocs.query().insertGraphAndFetch(consentFormObj);
        console.log(consent_form);
      }
      else
      {
        for(let i=0 ; i<s3ConsentLocation.length ; i++){
          const consentFormObj = {
            docmanual_policy_url:s3ConsentLocation[i].Location,
            parent_account_id: req.body.parent_account_id,
            customer_id: customerDetail.id,
            is_docmanual_policy_accepted: true ,
            docmanual_policy_accepted_on: new Date(),
            is_active: (req.body.IsActive !== null && req.body.IsActive !== "null" && req.body.IsActive !== "undefined" && req.body.IsActive !== undefined) ? req.body.IsActive : false 
          }
          const consent_form = await CustomerLegalDocs.query().insertGraphAndFetch(consentFormObj);
          console.log(consent_form);
        }
      }
    const locationsArray = JSON.parse(req.body.Locations);
    let _locations = [];
    if(locationsArray.length > 0){      
      for(let i=0; i<locationsArray.length; i++)
      {
        const locationobj = {
          customer_id : customerDetail.id,
          locationtype_id : locationsArray[i].locationtype_id,
          box_id : locationsArray[i].box_id,
          name : locationsArray[i].name,
          addressline1 : locationsArray[i].addressline1,
          addressline2 : locationsArray[i].addressline2,
          city : locationsArray[i].city,
          state : locationsArray[i].state,
          zip : locationsArray[i].zip,
          country : locationsArray[i].country,
          is_default_address : locationsArray[i].is_default_address,
          room_no: locationsArray[i].room_no,
          bed_no: locationsArray[i].bed_no,
          source_location: locationsArray[i].source_location,
          destination_location: locationsArray[i].destination_location,
          shelf: locationsArray[i].shelf,
          selectedcouriertypelocation: locationsArray[i].selectedcouriertypelocation,
          notes: locationsArray[i].notes,
          location_notes: locationsArray[i].location_notes,
          patient_notes: locationsArray[i].patient_notes
        }
        const CustomerLocations = await Customer_Locations_master.query().insertGraphAndFetch(locationobj);
        _locations.push(CustomerLocations) ;
      }      
    } 
    customerDetail.customer_locations = _locations;
    const _alternetAgentsArray = JSON.parse(req.body.alternetAgents);
    let _localAlternetAgent = [];
    for (let i = 0; i < _alternetAgentsArray.length; i++) {
      let singleAgent = {
        customer_id: customerDetail.id,
        agent_firstname: _alternetAgentsArray[i].agent_firstname,
        agent_lastname: _alternetAgentsArray[i].agent_lastname,
        agent_email: _alternetAgentsArray[i].agent_email,
        agent_sms: _alternetAgentsArray[i].agent_sms,
        isdefault: _alternetAgentsArray[i].isdefault,
      }
      const _agentResponse = await AlternetAgents.query().insertGraphAndFetch(singleAgent);
      _localAlternetAgent.push(_agentResponse);
    }
    customerDetail.alternetAgents = _localAlternetAgent;
    res.send(
      JSON.stringify({
        customer: customerDetail
      })
    );
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

router.put("/customers", upload.any(), async (req, res, next) => {
  // console.log(req);
  // res.send(JSON.stringify(req.file)); // is the `avatar` file
  // req.body will hold the text fields, if there were any
  // const fileData = await fs.readFileAsync(req.file.path);
  // console.log(fileData);
  var s3response = { Location: req.body.file };
  var ConsentLocation = {Location: req.body.LC};
  var s3ConsentLocation = [];
  var s3Location = {};
  if(ConsentLocation.Location && ConsentLocation.Location!=="null" && ConsentLocation.Location!==null){
    let ad1={
        Location : ConsentLocation.Location
      }
      s3ConsentLocation.push(ad1);
  }
  if (req.files.length > 0 && req.files[0].buffer) {
    for(let i=0;i<req.files.length;i++){
      let params = {
        Bucket: process.env.IMAGES_BUCKET,
        Key: uuidv4(),
        Body: req.files[i].buffer,
        ACL: "public-read",
        ContentType: req.files[i].mimetype
      };
      if(req.files[i].fieldname==='file'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        }
        else if(req.files[i].fieldname==='LC'){
          s3Location = await s3.upload(params).promise();
          delete req.files[i].buffer;
          s3ConsentLocation.push(s3Location);
        }
  }
  }
  const userObject = {
    id: req.body.id,
    name: `${req.body.first_name} ${req.body.last_name}`,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    username: req.body.email,
    parentAccount: { "#dbRef": req.body.parent_account_id },
    picture_url: s3response.Location,
    ada_compitible: req.body.ada_compitible,
    sms_notification: req.body.smsnotification,
    email_notification: req.body.emailnotification,
    push_notification: req.body.pushnotification,
    notes: req.body.patient_notes,
    box_id:  (req.body.boxid !== "undefined" && req.body.boxid !== undefined && req.body.boxid !== "null") ? req.body.boxid :null,
    //dob: req.body.dob,
    dob_str: req.body.dob,
    contacts: [
      {
        name: "Home",
        phone: req.body.phone,
        email: req.body.email
      }
    ],
    patient_id: req.body.patient_id,
    address: {
      name: "patient_address",
      address_line_1: req.body.address_line_1,
      address_line_2: req.body.address_line_2,
      city: req.body.city,
      state: req.body.state,
      zip: req.body.zip,
      country: req.body.country
    }
    // roles: [],
    // roles: [
    //     {
    //         "#dbRef": "eff549c7-aef0-43f8-b5c6-f1066e6becbd",
    //         name: "Super Admin",
    //     },
    //     {
    //         "#dbRef": "aa4658f1-7ea9-4b21-9b6b-7a83735aea71",
    //         // name: "Administrator",
    //     },
    //     {
    //         "#dbRef": "f11df23e-ee45-4f13-8c99-81dc3562c945",
    //         // name: "Pharmacist",
    //     },
    //     {
    //         "#dbRef": "fb6fa7c3-3cfc-43db-ae9a-7d2e96556704",
    //         // name: "Technician",
    //     }
    // ]
  };
  console.log(req.body.roles);
  if (req.body.roles) {
    req.body.roles.split(",").forEach(element => {
      userObject.roles.push({ "#dbRef": element });
    });
  }

  console.log(userObject);

  try {
    // const cognitoUser = await cisp.adminCreateUser(cognitoParam).promise();
    // console.log(cognitoUser);
    // userObject.id = cognitoUser.User.Username;
    const u = await customer.query()
    .upsertGraphAndFetch(userObject, { relate: true, unrelate: true });

    if(s3ConsentLocation.length > 0){
      if(req.body.signature !== undefined && req.body.signature !== "undefined" && req.body.signature !== null && req.body.signature !== "null"){
      for(let i=0 ; i<s3ConsentLocation.length ; i++){
        const consentFormObj = {
          id: req.body.consent_id,
          is_active: (req.body.IsActive !== null && req.body.IsActive !== "null" && req.body.IsActive !== "undefined" && req.body.IsActive !== undefined) ? req.body.IsActive : false
        }
        const consent_form = await CustomerLegalDocs.query().upsertGraphAndFetch(consentFormObj);
        console.log(consent_form);
      }
    }
    else if (req.body.consent_id !== undefined && req.body.consent_id !== "undefined" && req.body.consent_id !== null && req.body.consent_id !== "null"){
      for(let i=0 ; i<s3ConsentLocation.length ; i++){
        const consentFormObj = {
          id: req.body.consent_id,
          docmanual_policy_url:s3ConsentLocation[i].Location,
          parent_account_id: req.body.parent_account_id,
          customer_id: u.id,
          is_docmanual_policy_accepted: true ,
          docmanual_policy_accepted_on: new Date(),
          is_active: (req.body.IsActive !== null && req.body.IsActive !== "null" && req.body.IsActive !== "undefined" && req.body.IsActive !== undefined) ? req.body.IsActive : false,
          signature_url: null,
          is_docone_policy_accepted: null ,
          docone_policy_accepted_hash: null,
          docone_policy_label: null,
          is_doctwo_policy_accepted: null ,
          doctwo_policy_accepted_hash: null,
          doctwo_policy_label: null,
          is_docthree_policy_accepted: null ,
          docthree_policy_accepted_hash: null,
          docthree_policy_label: null
        }
        const consent_form = await CustomerLegalDocs.query().upsertGraphAndFetch(consentFormObj);
        console.log(consent_form);
      }
    }
    else{
      for(let i=0 ; i<s3ConsentLocation.length ; i++){
        const consentFormObj = {
          docmanual_policy_url:s3ConsentLocation[i].Location,
          parent_account_id: req.body.parent_account_id,
          customer_id: u.id,
          is_docmanual_policy_accepted: true ,
          //docmanual_policy_accepted_hash: false,
          docmanual_policy_accepted_on: new Date(),
          is_active: (req.body.IsActive !== null && req.body.IsActive !== "null" && req.body.IsActive !== "undefined" && req.body.IsActive !== undefined) ? req.body.IsActive : false
        }
        const consent_form = await CustomerLegalDocs.query().insertGraphAndFetch(consentFormObj);
        console.log(consent_form);
      }
    }
   }
   else{
     if(req.body.consent_id !== undefined && req.body.consent_id !== "undefined" && req.body.consent_id !== null && req.body.consent_id !== "null"){
      const consentFormObj = {
        id: req.body.consent_id,
        is_active: (req.body.IsActive !== null && req.body.IsActive !== "null" && req.body.IsActive !== "undefined" && req.body.IsActive !== undefined) ? req.body.IsActive : false
      }
      const consent_form = await CustomerLegalDocs.query().upsertGraphAndFetch(consentFormObj);
      console.log(consent_form);
     }else{
      const consentFormObj = {
        parent_account_id: req.body.parent_account_id,
        customer_id: u.id,
        is_active: (req.body.IsActive !== null && req.body.IsActive !== "null" && req.body.IsActive !== "undefined" && req.body.IsActive !== undefined) ? req.body.IsActive : false
      }
      const consent_form = await CustomerLegalDocs.query().insertGraphAndFetch(consentFormObj);
      console.log(consent_form);
     }
   
  }
  const CustomerLocations = await Customer_Locations_master.query().skipUndefined().where('customer_id', u.id);   
    if(CustomerLocations && CustomerLocations.length >0){
     for(let j=0 ; j<CustomerLocations.length ; j++){
       const customerlocationdelete =  await Customer_Locations_master.query().deleteById(CustomerLocations[j].id);
     }
   }
   const customerlocationArray = JSON.parse(req.body.Locations);
   let _Locations = [];
    if(customerlocationArray.length > 0){      
      for(let i=0; i<customerlocationArray.length; i++)
      {
        const Locationsobj = {
          customer_id : u.id,
          locationtype_id : customerlocationArray[i].locationtype_id,
          box_id : customerlocationArray[i].box_id,
          name : customerlocationArray[i].name,
          addressline1 : customerlocationArray[i].addressline1,
          addressline2 : customerlocationArray[i].addressline2,
          city : customerlocationArray[i].city,
          state : customerlocationArray[i].state,
          zip : customerlocationArray[i].zip,
          country : customerlocationArray[i].country,
          is_default_address : customerlocationArray[i].is_default_address,
          room_no: customerlocationArray[i].room_no,
          bed_no: customerlocationArray[i].bed_no,
          source_location: customerlocationArray[i].source_location,
          destination_location: customerlocationArray[i].destination_location,
          shelf: customerlocationArray[i].shelf,
          selectedcouriertypelocation: customerlocationArray[i].selectedcouriertypelocation,
          notes: customerlocationArray[i].notes,
          location_notes: customerlocationArray[i].location_notes,
          patient_notes: customerlocationArray[i].patient_notes
        }
        const customerlocations = await Customer_Locations_master.query().insertGraphAndFetch(Locationsobj);
        _Locations.push(customerlocations);
      }
    } 
    u.customer_locations = _Locations;
    const alternetAgents = await AlternetAgents.query().skipUndefined().where('customer_id', u.id);
    if(alternetAgents && alternetAgents.length >0){
      for(let j=0 ; j<alternetAgents.length ; j++){
        const alternetagentdelete =  await AlternetAgents.query().deleteById(alternetAgents[j].id);
      }
    }
    const _alternetAgentsArray = JSON.parse(req.body.alternetAgents);
    let _localAlternetAgent = [];
    for (let i = 0; i < _alternetAgentsArray.length; i++) {
      let singleAgent = {
        customer_id: u.id,
        agent_firstname: _alternetAgentsArray[i].agent_firstname,
        agent_lastname: _alternetAgentsArray[i].agent_lastname,
        agent_email: _alternetAgentsArray[i].agent_email,
        agent_sms: _alternetAgentsArray[i].agent_sms,
        isdefault: _alternetAgentsArray[i].isdefault,
      }
      const _agentResponse = await AlternetAgents.query().insertGraphAndFetch(singleAgent);
      _localAlternetAgent.push(_agentResponse);
    }
    u.alternetAgents = _localAlternetAgent;
    res.send(
      JSON.stringify({
        // body: req.body,
        // files: req.files,
        // s3: s3response,
        customer: u
        // cognito: cognitoUser,
      })
    );
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

router.put("/users", upload.any(), async (req, res, next) => {
  // console.log(req);
  // res.send(JSON.stringify(req.file)); // is the `avatar` file
  // req.body will hold the text fields, if there were any
  // const fileData = await fs.readFileAsync(req.file.path);
  // console.log(fileData);

  // const originalUser = user.query().findById(req.body.id).eager('[roles, contact]');

  let s3response = { Location: req.body.file };

  if (req.files.length > 0 && req.files[0].buffer) {
    var params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(), // req.file.filename,
      Body: req.files[0].buffer,
      ACL: "public-read",
      ContentType: req.files[0].mimetype
    };
    s3response = await s3.upload(params).promise();
    delete req.files[0].buffer;
  }

  const userObject = {
    id: req.body.id,
    name: `${req.body.first_name} ${req.body.last_name}`,
    parentAccount: { "#dbRef": req.body.parent_account_id },
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    username: req.body.email,
    badge_id: req.body.badge_id,
    picture_url: s3response.Location,
    contact: {
      id: req.body.contact_id,
      name: "Home",
      phone: req.body.phone,
      email: req.body.email
    },
    roles: []
    // roles: [
    //     {
    //         "#dbRef": "eff549c7-aef0-43f8-b5c6-f1066e6becbd",
    //         name: "Super Admin",
    //     },
    //     {
    //         "#dbRef": "aa4658f1-7ea9-4b21-9b6b-7a83735aea71",
    //         // name: "Administrator",
    //     },
    //     {
    //         "#dbRef": "f11df23e-ee45-4f13-8c99-81dc3562c945",
    //         // name: "Pharmacist",
    //     },
    //     {
    //         "#dbRef": "fb6fa7c3-3cfc-43db-ae9a-7d2e96556704",
    //         // name: "Technician",
    //     }
    // ]
  };
  const RolesToBeDeleteForUsers = await UserRoles.query().where('user_id', req.body.id);
  if (RolesToBeDeleteForUsers && RolesToBeDeleteForUsers.length > 0) {
    for (let i = 0; i < RolesToBeDeleteForUsers.length; i++) {
      const removeSelectedRole = await UserRoles.query().deleteById(RolesToBeDeleteForUsers[i].id);
      console.log(`${RolesToBeDeleteForUsers[i].id} User role removed with user role details ${JSON.stringify(removeSelectedRole, null, 2)}!`);
    }
  }
  console.log(req.body.roles);
  if (req.body.roles) {
    req.body.roles.split(",").forEach(element => {
      // if(originalUser.roles.map(x => x.id).indexOf(element)){
      userObject.roles.push({ id: element });
      // }
    });
  }
  console.log(userObject);

  // try {
  // const cognitoUser = await cisp.adminCreateUser(cognitoParam).promise();
  // console.log(cognitoUser);
  // userObject.id = cognitoUser.User.Username;
  const u = await user
    .query()
    .upsertGraphAndFetch(userObject, { relate: true, unrelate: true });

  res.send(
    JSON.stringify({
      body: req.body,
      files: req.files,
      s3: s3response,
      user: u
      // cognito: cognitoUser,
    })
  );
  // }
  // catch (error) {
  //     res.send(JSON.stringify({
  //         error
  //     }));
  // }
});

router.post("/users", upload.any(), async (req, res, next) => {
  // console.log(req);
  // res.send(JSON.stringify(req.file)); // is the `avatar` file
  // req.body will hold the text fields, if there were any
  // const fileData = await fs.readFileAsync(req.file.path);
  // console.log(fileData);
  var s3response = { Location: "" };

  if (req.files.length > 0 && req.files[0].buffer) {
    var params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(), // req.file.filename,
      Body: req.files[0].buffer,
      ACL: "public-read",
      ContentType: req.files[0].mimetype
    };
    s3response = await s3.upload(params).promise();
    delete req.files[0].buffer;
  }

  const userObject = {
    name: `${req.body.first_name} ${req.body.last_name}`,
    parentAccount: { "#dbRef": req.body.parent_account_id },
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    username: req.body.email,
    badge_id: req.body.badge_id,
    picture_url: s3response.Location,
    password_reset_on: new Date(),
    contact: {
      name: "Home",
      phone: req.body.phone,
      email: req.body.email
    },
    roles: []
  };
  console.log(req.body.roles);
  if (req.body.roles) {
    req.body.roles.split(",").forEach(element => {
      userObject.roles.push({ "#dbRef": element });
    });
  }
  console.log(userObject);

  const cognitoParam = {

    UserPoolId: process.env.USER_POOL_ID /* required */,
    Username: userObject.username /* required */,
    DesiredDeliveryMediums: [
      "EMAIL"
      //   SMS | EMAIL,
      /* more items */
    ],
    // ForceAliasCreation: true || false,
    // MessageAction: RESEND | SUPPRESS,
    TemporaryPassword: generator.generate({
      length: 8,
      numbers: true,
      excludeSimilarCharacters: true,
      symbols : '!@#$%^&*()+_-=}{[]|:;"/?.,`~',
      //symbols: true,
      uppercase: true,
      strict: true
    }),
    UserAttributes: [
      {
        Name: "name" /* required */,
        Value: userObject.name
      },
      {
        Name: "email",
        Value: req.body.email
      },
      {
        Name: "phone_number",
        Value:
          "+1" +
          req.body.phone
            .replace("(", "")
            .replace(")", "")
            .replace("-", "")
            .replace(" ", "")
      },
      {
        Name: "email_verified",
        Value: "true"
      },
      {
        Name: "phone_number_verified",
        Value: "true"
      }
      /* more items */
    ]
    // ValidationData: [
    //   {
    //     Name: 'STRING_VALUE', /* required */
    //     Value: 'STRING_VALUE'
    //   },
    //  /* more items */
    // ]
  };

  try {
    const cognitoUser = await cisp.adminCreateUser(cognitoParam).promise();
    userObject.id = cognitoUser.User.Username;
    const u = await user.query().insertGraphAndFetch(userObject);
    res.send(
      JSON.stringify({
        body: req.body,
        files: req.files,
        s3: s3response,
        user: u,
        cognito: cognitoUser
      })
    );
  } catch (error) {
    console.error({
      message: error.message,
      stack: error.stack
    });
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

router.put("/accounts", upload.any(), async (req, res, next) => {
    let s3response = {};
    let s3AvatarLocation = { Location: req.body.avatar };
    let s3BrandLocation = { Location: req.body.blogo };
    let s3ClosedImgLocation = {Location: req.body.cloesdimg};
    let s3Advertisement1Location = { Location: req.body.ad1 };
    let s3Advertisement2Location = {Location: req.body.ad2 };
    let s3Advertisement3Location = {Location: req.body.ad3 };
    let s3videoLocation = {Location: req.body.vd};
    let AdertiseGobj = [];
    let s3DocOneLocation = {Location: req.body.hippa};
    let s3DocTwoLocation = {Location: req.body.LC};
    let s3DocThreeLocation = {Location: req.body.OTHER};
    let s3BottomAdvertisement1Location = { Location: req.body.bottomad1 };
    let s3BottomAdvertisement2Location = {Location: req.body.bottomad2 };
    let s3BottomAdvertisement3Location = {Location: req.body.bottomad3 };
    //let s3ETLocation = {Location: req.body.ET}
    let sha256 = require('js-sha256');


    if(s3Advertisement1Location.Location && s3Advertisement1Location.Location!=="null" && s3Advertisement1Location.Location!==null){
      let ad1={
        s3response: {
          Location : s3Advertisement1Location.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"ad1"
        }
        AdertiseGobj.push(ad1);
    }

    if(s3Advertisement2Location.Location && s3Advertisement2Location.Location!=="null" && s3Advertisement2Location.Location!==null){
      let ad2={
        s3response: {
          Location : s3Advertisement2Location.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"ad2"
      }
      AdertiseGobj.push(ad2);
    }

    if(s3Advertisement3Location.Location && s3Advertisement3Location.Location!=="null" && s3Advertisement3Location.Location!==null){
      let ad3={
        s3response: {
          Location : s3Advertisement3Location.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"ad3"
      }
      AdertiseGobj.push(ad3);
    }

    if(s3videoLocation.Location && s3videoLocation.Location!=="null" && s3videoLocation.Location!==null){
      let vd={
        s3response: {
          Location : s3videoLocation.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"vd"
      }

      AdertiseGobj.push(vd);
    }
    if(s3BottomAdvertisement1Location.Location && s3BottomAdvertisement1Location.Location!=="null" && s3BottomAdvertisement1Location.Location!==null){
      let bottomad1={
        s3response: {
          Location : s3BottomAdvertisement1Location.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"bottomad1"
        }
        AdertiseGobj.push(bottomad1);
    }

    if(s3BottomAdvertisement2Location.Location && s3BottomAdvertisement2Location.Location!=="null" && s3BottomAdvertisement2Location.Location!==null){
      let bottomad2={
        s3response: {
          Location : s3BottomAdvertisement2Location.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"bottomad2"
      }
      AdertiseGobj.push(bottomad2);
    }

    if(s3BottomAdvertisement3Location.Location && s3BottomAdvertisement3Location.Location!=="null" && s3BottomAdvertisement3Location.Location!==null){
      let bottomad3={
        s3response: {
          Location : s3BottomAdvertisement3Location.Location
        },
        key:"ADVERTISEMENT",
        value:BRAND_TYPES.ADVERTISEMENT,
        ad_number:"bottomad3"
      }
      AdertiseGobj.push(bottomad3);
    }

    if (req.files.length > 0 && req.files[0].buffer) {
      for(let i=0;i<req.files.length;i++){
        let params = {
          Bucket: process.env.IMAGES_BUCKET,
          Key: uuidv4(),
          Body: req.files[i].buffer,
          ACL: "public-read",
          ContentType: req.files[i].mimetype
        };

        if(req.files[i].fieldname==='avatar'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          s3AvatarLocation = s3response;
        }else if(req.files[i].fieldname==='ad1'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"ADVERTISEMENT",
            value:BRAND_TYPES.ADVERTISEMENT,
            ad_number:"ad1"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }else if(req.files[i].fieldname==='ad2'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"ADVERTISEMENT",
            value:BRAND_TYPES.ADVERTISEMENT,
            ad_number:"ad2"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }else if(req.files[i].fieldname==='ad3'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"ADVERTISEMENT",
            value:BRAND_TYPES.ADVERTISEMENT,
            ad_number:"ad3"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }else if(req.files[i].fieldname==='blogo'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          s3BrandLocation = s3response;
        }else if(req.files[i].fieldname==='vd'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"VIDEO_CONTENT",
            value:BRAND_TYPES.VIDEO_CONTENT,
            ad_number:"vd"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }
        else if(req.files[i].fieldname === 'cloesdimg'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          s3ClosedImgLocation = s3response;
        }
        else if(req.files[i].fieldname === 'hippa'){
            s3response = await s3.upload(params).promise();
            delete req.files[i].buffer;
            s3DocOneLocation = s3response;
        }
        else if(req.files[i].fieldname === 'LC'){
            s3response = await s3.upload(params).promise();
            delete req.files[i].buffer;
            s3DocTwoLocation = s3response;
        }
        else if(req.files[i].fieldname === 'other'){
            s3response = await s3.upload(params).promise();
            delete req.files[i].buffer;
            s3DocThreeLocation = s3response;
        }
        else if(req.files[i].fieldname==='bottomad1'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"ADVERTISEMENT",
            value:BRAND_TYPES.ADVERTISEMENT,
            ad_number:"bottomad1"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }else if(req.files[i].fieldname==='bottomad2'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"ADVERTISEMENT",
            value:BRAND_TYPES.ADVERTISEMENT,
            ad_number:"bottomad2"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }else if(req.files[i].fieldname==='bottomad3'){
          s3response = await s3.upload(params).promise();
          delete req.files[i].buffer;
          let s3AdvertisementLocationObj={
            s3response:s3response,
            key:"ADVERTISEMENT",
            value:BRAND_TYPES.ADVERTISEMENT,
            ad_number:"bottomad3"
          }
          AdertiseGobj.push(s3AdvertisementLocationObj);
        }
      //   else if(req.files[i].fieldname === 'ET'){
      //     s3response = await s3.upload(params).promise();
      //     delete req.files[i].buffer;
      //     s3ETLocation = s3response;
      // }
      }
    }

    let brandId;
    if(req.body.brand_id && req.body.brand_id!==null && req.body.brand_id!=='null' && req.body.brand_id!==undefined && req.body.brand_id!=='undefined'){
      brandId = req.body.brand_id
    }

    let brand_response;
    if(brandId){
      const brandObject={
        id: brandId,
        name: req.body.brandname,
        phone: req.body.generalphone,
        website: req.body.website,
        fax: req.body.fax,
        email: req.body.generalemail,
        type_id: BRAND_TYPES.BRAND_LOGO,
        logo_url: s3BrandLocation.Location,
        //email_template_url: s3ETLocation.Location
      };
      brand_response = await brand.query().upsertGraphAndFetch(brandObject);
    }else{
      const brandObject={
        name: req.body.brandname,
        phone: req.body.generalphone,
        website: req.body.website,
        fax: req.body.fax,
        email: req.body.generalemail,
        type_id: BRAND_TYPES.BRAND_LOGO,
        logo_url: s3BrandLocation.Location,
        //email_template_url: s3ETLocation.Location
      };
      brand_response = await brand.query().insertGraphAndFetch(brandObject);
    }
    const accountObject = {
      id: req.body.id,
      name: `${req.body.name}`,
      brand_id: brand_response.id===null || brand_response.id==="" ? null : brand_response.id,
      picture_url: s3AvatarLocation.Location,
      account_connections_id: req.body.account_connections_id, //Added by Naresh
      is_payment_required: req.body.is_payment_required, //Added by Naresh
      demo_email: req.body.demo_email, //Added by Kishan
      demo_phone: req.body.demo_smsphone, //Added by Kishan
      contract_language: `${req.body.contractLang}`,
      expiration_days: `${req.body.Expiration_Day}`,
      pick_up_reminder_day:req.body.pick_up_reminder_day,
      is_advance_advertise: req.body.is_advance_advertise,
      is_allow_consult_video: req.body.allow_consult_video,
      is_allow_consult_mobile: req.body.allow_consult_mobile,
      is_allow_consult_schedule: req.body.allow_consult_schedule,
      isContactLessPickupEnabled: req.body.isContactLessPickupEnabled,
      contact: {
        name: `${req.body.first_name} ${req.body.last_name}`,
        phone: req.body.phone,
        email: req.body.email,
        address: {
          name: "account_address",
          address_line_1: req.body.address1,
          address_line_2: req.body.address2,
          city: req.body.city,
          state: req.body.state,
          zip: req.body.zip
        }
      },
      parent: {
        "#dbRef": req.body.parent_id
      },
      type: {
        "#dbRef": req.body.type_id
      },
    };
    try {
      let a = await account
      .query()
       .upsertGraphAndFetch(accountObject, { relate: true, unrelate: true });


      a.brand = brand_response;
      if(AdertiseGobj.length > 0){
        const brands_detail_ac = await BrandDetail.query().where('parent_account_id',a.id);

        if(brands_detail_ac && brands_detail_ac.length > 0){
          for(let j=0 ; j<brands_detail_ac.length ; j++){
           const branddelete =  await BrandDetail.query().deleteById(brands_detail_ac[j].id);
          }
        }
        let _brandDetailResponseArray = [];
        for(let i=0 ; i<AdertiseGobj.length ; i++){
          const brandDetailObj = {
            type_id: AdertiseGobj[i].value,
            logo_url:AdertiseGobj[i].s3response.Location,
            parent_account_id: a.id,
            ad_number: AdertiseGobj[i].ad_number
          }

         const brand_detail = await BrandDetail.query().insertGraphAndFetch(brandDetailObj);
         _brandDetailResponseArray.push(brand_detail);         
        }
        a.brand_detail = _brandDetailResponseArray;
     }  
     const Hours_OF_Operation_ac = await HoursOfOperation.query().skipUndefined().where('account_id', a.id);   
     if(Hours_OF_Operation_ac && Hours_OF_Operation_ac.length >0){
      for(let j=0 ; j<Hours_OF_Operation_ac.length ; j++){
        const HOPdelete =  await HoursOfOperation.query().deleteById(Hours_OF_Operation_ac[j].id);
      }
    }

 let sunday ;
    if(req.body.is_sunday === null || req.body.is_sunday === '' || req.body.is_sunday === undefined){
      sunday = false;
    }else{
      sunday = req.body.is_sunday;
    }
    let monday;
    if(req.body.is_monday === null || req.body.is_monday === '' || req.body.is_monday === undefined){
      monday = false;
    }
    else{
      monday = req.body.is_monday;
    }
    let tuesday;
    if(req.body.is_tuesday === null || req.body.is_tuesday === '' || req.body.is_tuesday === undefined){
      tuesday = false;
    }
    else{
      tuesday = req.body.is_tuesday;
    }
    let wednesday;
    if(req.body.is_wednesday === null || req.body.is_wednesday === '' || req.body.is_wednesday === undefined){
      wednesday = false;
    }
    else{
      wednesday = req.body.is_wednesday;
    }
    let thursday;
    if(req.body.is_thursday === null || req.body.is_thursday === '' || req.body.is_thursday === undefined){
      thursday = false;
    }
    else{
      thursday = req.body.is_thursday;
    }
    let friday;
    if(req.body.is_friday === null || req.body.is_friday === '' || req.body.is_friday === undefined){
      friday = false;
    }
    else{
      friday = req.body.is_friday; 
    }
    let saturday;
    if(req.body.is_saturday === null || req.body.is_saturday === '' || req.body.is_saturday === undefined){
      saturday = false;
    }
    else{
      saturday = req.body.is_saturday;
    }
    let ObjHoursOfOperations = {
      account_id: a.id,
      is_sunday: sunday,
      is_monday: monday,
      is_tuesday: tuesday,
      is_wednesday: wednesday,
      is_thursday: thursday,
      is_friday: friday,
      is_saturday: saturday,
      open_time: req.body.OpenTime,
      close_time: req.body.CloseTime,
      hop_type: req.body.HopType,
      closedimg_url: s3ClosedImgLocation.Location,
      closed_msg: req.body.closedMsg
    }
    const Hours_Of_Operation = await HoursOfOperation.query().insertGraphAndFetch(ObjHoursOfOperations);
    a.hoursof_operations = Hours_Of_Operation

    const conn = await AccountConnection.query().where('id',req.body.account_connections_id);
    if(conn)
    {
     a.connection = {
       id: conn[0].id,
       name: conn[0].name
     }
    }
    const conn_details = await AccountConnectionDetails.query()
                          .where('account_id',req.body.id);
    if(conn_details.length > 0)
    {
      let objConnectionDetails = {
        id: conn_details[0].id,
        account_id: conn_details[0].account_id,
        account_connection_id: req.body.account_connections_id,
        endpoint: req.body.endpoint,
        pharmacyid: req.body.pharmacyid,
        vendor_key:req.body.vendor_key,
        data_exchange_id:req.body.data_exchange_id,
        epic_client_id : req.body.epicclientid,
        epic_username: req.body.epicusername,
        epic_password: req.body.epicpassword,
        medbank_dblogin: req.body.medbank_dblogin,
        medbank_db_password: req.body.medbank_db_password,
        opc_apim_subscription_key: req.body.opc_apim_subscription_key
      }
     
      const conResult = await AccountConnectionDetails
      .query()
       .upsertGraphAndFetch(objConnectionDetails, { relate: true, unrelate: true });

     a.connection_details = conResult;
    }
    else
    {
      let objConnectionDetails = {
        account_id: req.body.id,
        account_connection_id: req.body.account_connections_id,
        endpoint: req.body.endpoint,
        pharmacyid: req.body.pharmacyid,
        epic_client_id: req.body.epicclientid,
        epic_username: req.body.epicusername,
        epic_password: req.body.epicpassword,
        medbank_dblogin: req.body.medbank_dblogin,
        medbank_db_password: req.body.medbank_db_password,
        opc_apim_subscription_key: req.body.opc_apim_subscription_key
      }
      console.log('objConnectionDetails:=',objConnectionDetails);
      const conResult = await AccountConnectionDetails
      .query()
       .insertGraphAndFetch(objConnectionDetails, { relate: true, unrelate: true });
       console.log('conResult:=',conResult);
      a.connection_details = conResult;
    }
    console.log("up Acount: ", a);

    const Health_AND_Info_ac = await Health_and_information_master.query().skipUndefined().where('account_id', a.id);   
    if(Health_AND_Info_ac && Health_AND_Info_ac.length >0){
     for(let j=0 ; j<Health_AND_Info_ac.length ; j++){
       const HealthANDInfodelete =  await Health_and_information_master.query().deleteById(Health_AND_Info_ac[j].id);
     }
   }
   const healthandinfoArray = JSON.parse(req.body.Health_and_info);
   let _healthInformation = [];
    if(healthandinfoArray.length > 0){      
      for(let i=0; i<healthandinfoArray.length; i++)
      {
        const Health_and_infoobj = {
          title: healthandinfoArray[i].title,
          description : healthandinfoArray[i].description,
          video_url : healthandinfoArray[i].video_url,
          category_id : healthandinfoArray[i].category_id,
          primarylibrary_id: healthandinfoArray[i].primarylibrary_id,
          account_id : a.id,
          primarylibrary_title1: req.body.primaryLibtitle1,
          primarylibrary_title2: req.body.primaryLibtitle2
        }
        const Healthandinformations = await Health_and_information_master.query().insertGraphAndFetch(Health_and_infoobj);
        _healthInformation.push(Healthandinformations);
      }
    } 
    a.health_and_informations = _healthInformation;
    const legal_compliance_ac =  await LegalAgreement.query().where('account_id',a.id);
      if(legal_compliance_ac && legal_compliance_ac.length > 0){
        const legalcompliancedelete = await LegalAgreement.query().deleteById(legal_compliance_ac[0].id);
      }      
      const LegalAgreementObj = {
        account_id: a.id,
        legal_compliance_docone_label: req.body.doc_one,
        legal_compliance_docone_url: s3DocOneLocation.Location,
        legal_compliance_doctwo_label: req.body.doc_two,
        legal_compliance_doctwo_url: s3DocTwoLocation.Location,
        legal_compliance_docthree_label: req.body.doc_three,
        legal_compliance_docthree_url: s3DocThreeLocation.Location,
        legal_compliance_docone_hash: sha256(`${s3DocOneLocation.Location}`),
        legal_compliance_doctwo_hash: sha256(`${s3DocTwoLocation.Location}`),
        legal_compliance_docthree_hash: sha256(`${s3DocThreeLocation.Location}`)
      }
      const Legal_agreement = await LegalAgreement.query().insertGraphAndFetch(LegalAgreementObj);
      a.legal_compliance = Legal_agreement

      const isAlreadyExist = await GbpConfig.query().where('parent_account_id', req.body.id).first();
      try {
        const secretKey = process.env.CIPHER;
        const crypter = new Crypter(secretKey);
        if(isAlreadyExist && isAlreadyExist !== null) {
          isAlreadyExist.tokenapikey = (req.body.tokenxgbapikey === undefined || req.body.tokenxgbapikey === 'undefined' || req.body.tokenxgbapikey === null || req.body.tokenxgbapikey === 'null' || req.body.tokenxgbapikey === '') ? isAlreadyExist.tokenapikey : crypter.encrypt(req.body.tokenxgbapikey),
          isAlreadyExist.transapikey = (req.body.transxgbapikey === undefined || req.body.transxgbapikey === 'undefined' || req.body.transxgbapikey === null || req.body.transxgbapikey === 'null' || req.body.transxgbapikey === '') ? isAlreadyExist.transapikey : crypter.encrypt(req.body.transxgbapikey),
          isAlreadyExist.transapisecret = (req.body.transxgbapisecret === undefined || req.body.transxgbapisecret === 'undefined' || req.body.transxgbapisecret === null || req.body.transxgbapisecret === 'null' || req.body.transxgbapisecret === '') ? isAlreadyExist.transapisecret : crypter.encrypt(req.body.transxgbapisecret),
          isAlreadyExist.allow_payment = (req.body.allow_payment === "true" || req.body.allow_payment === true) ? true : false
          const g = await GbpConfig.query().upsertGraphAndFetch(isAlreadyExist,
            {
                relate: true,
                unrelate: true
            }
          );
          a.gbpconfig = g;
        } else {
          const gbpObject = {
            parent_account_id: req.body.id,
            tokenapikey: (req.body.tokenxgbapikey === undefined || req.body.tokenxgbapikey === 'undefined' || req.body.tokenxgbapikey === null || req.body.tokenxgbapikey === 'null' || req.body.tokenxgbapikey === '') ? '' : crypter.encrypt(req.body.tokenxgbapikey),
            transapikey: (req.body.transxgbapikey === undefined || req.body.transxgbapikey === 'undefined' || req.body.transxgbapikey === null || req.body.transxgbapikey === 'null' || req.body.transxgbapikey === '') ? '' : crypter.encrypt(req.body.transxgbapikey),
            transapisecret: (req.body.transxgbapisecret === undefined || req.body.transxgbapisecret === 'undefined' || req.body.transxgbapisecret === null || req.body.transxgbapisecret === 'null' || req.body.transxgbapisecret === '') ? '' : crypter.encrypt(req.body.transxgbapisecret),
            allow_payment: (req.body.allow_payment === "true" || req.body.allow_payment === true) ? true : false
          }
          const g = await GbpConfig.query().insertGraphAndFetch(gbpObject);
          a.gbpconfig = g;
        }
      } catch(error) {
        console.log(`Error while updating iPayConfiguration with details : ${JSON.stringify(error)}`);
      }
      
      if (req.body.account_connections_id === ConnectionType.EPIC) {
        try {
          const isTemplateExist = await Templates.query().where('account_id', req.body.id).first();
          if (isTemplateExist === undefined || isTemplateExist === null) {
            const templateObject = {
              account_id: req.body.id,
              versions: (req.body.epicEndpointVersion === undefined || req.body.epicEndpointVersion === 'undefined' || req.body.epicEndpointVersion === null || req.body.epicEndpointVersion === 'null' || req.body.epicEndpointVersion === '') ? '' : req.body.epicEndpointVersion,
              request_template: (req.body.epicRequestTempate === undefined || req.body.epicRequestTempate === 'undefined' || req.body.epicRequestTempate === null || req.body.epicRequestTempate === 'null' || req.body.epicRequestTempate === '') ? '' : req.body.epicRequestTempate,
              pharmacy_ncpdpid: (req.body.epicPharmacyNcpdpId === undefined || req.body.epicPharmacyNcpdpId === 'undefined' || req.body.epicPharmacyNcpdpId === null || req.body.epicPharmacyNcpdpId === 'null' || req.body.epicPharmacyNcpdpId === '') ? '' : req.body.epicPharmacyNcpdpId,
            }
            const temp = await Templates.query().insertGraphAndFetch(templateObject);
            a.template = temp;
          } else {
            isTemplateExist.account_id = req.body.id;
            isTemplateExist.versions = (req.body.epicEndpointVersion === undefined || req.body.epicEndpointVersion === 'undefined' || req.body.epicEndpointVersion === null || req.body.epicEndpointVersion === 'null' || req.body.epicEndpointVersion === '') ? '' : req.body.epicEndpointVersion;
            isTemplateExist.request_template = (req.body.epicRequestTempate === undefined || req.body.epicRequestTempate === 'undefined' || req.body.epicRequestTempate === null || req.body.epicRequestTempate === 'null' || req.body.epicRequestTempate === '') ? '' : req.body.epicRequestTempate;
            isTemplateExist.pharmacy_ncpdpid = (req.body.epicPharmacyNcpdpId === undefined || req.body.epicPharmacyNcpdpId === 'undefined' || req.body.epicPharmacyNcpdpId === null || req.body.epicPharmacyNcpdpId === 'null' || req.body.epicPharmacyNcpdpId === '') ? '' : req.body.epicPharmacyNcpdpId;
            const temp = await Templates.query().upsertGraphAndFetch(isTemplateExist,
              {
                  relate: true,
                  unrelate: true
              }
            );
            a.template = temp;
          }
        } catch (error) {
          console.log(`Error while saving epic request template with details : ${JSON.stringify(error)}`);
        }
      }
      if(req.body.accContactPerson && req.body.accContactPerson.length>0){
        let accContacts = JSON.parse(req.body.accContactPerson);
        const _personsToDelete = await AccountContactPersons.query().skipUndefined().where('account_id', req.body.id);
        if(_personsToDelete && _personsToDelete.length >0){
          for(let j=0 ; j<_personsToDelete.length ; j++){
            const _deletedPersons =  await AccountContactPersons.query().deleteById(_personsToDelete[j].id);
            console.log(`Contact Person removed with details : ${_deletedPersons}`);
          }
        }
      let _localAcp = [];
      if(accContacts && accContacts.length > 0) {      
        for (let i = 0; i < accContacts.length; i++) {
          let singleAgent = {
            account_id: a.id,
            first_name: accContacts[i].first_name,
            last_name: accContacts[i].last_name,
            email: accContacts[i].email,
            phone: accContacts[i].phone,
            isdefault: accContacts[i].isdefault,
          }
          const _acpResponse = await AccountContactPersons.query().insertGraphAndFetch(singleAgent);
          _localAcp.push(_acpResponse);
        }  
      }
      a.accContactPersons = _localAcp;
      } 
      else{
        a.accContactPersons = [];
      }
      
      res.send(
        JSON.stringify({
          account: a
        })
      );
    } catch (error) {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
});
router.post("/accounts", upload.any(), async (req, res, next) => {

  let s3response = {};
  let s3AvatarLocation ={};
  let s3BrandLocation = {};
  let s3ClosedImgLocation = {};
  let s3AdvertisementLocation =  [];
  let s3DocOneLocation = {};
  let s3DocTwoLocation = {};
  let s3DocThreeLocation = {};
  //let s3ETLocation = {};
  let sha256 = require('js-sha256');


  if (req.files.length > 0 && req.files[0].buffer) {
    for(let i=0;i<req.files.length;i++){
      let params = {
        Bucket: process.env.IMAGES_BUCKET,
        Key: uuidv4(),
        Body: req.files[i].buffer,
        ACL: "public-read",
        ContentType: req.files[i].mimetype
      };

      if(req.files[i].fieldname==='avatar'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3AvatarLocation = s3response;
      }else if(req.files[i].fieldname==='ad1'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"ADVERTISEMENT",
          value:BRAND_TYPES.ADVERTISEMENT,
          ad_number:"ad1"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }else if(req.files[i].fieldname==='ad2'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"ADVERTISEMENT",
          value:BRAND_TYPES.ADVERTISEMENT,
          ad_number:"ad2"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }else if(req.files[i].fieldname==='ad3'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"ADVERTISEMENT",
          value:BRAND_TYPES.ADVERTISEMENT,
          ad_number:"ad3"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }else if(req.files[i].fieldname==='blogo'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3BrandLocation = s3response;
      }else if(req.files[i].fieldname==='vd'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"VIDEO_CONTENT",
          value:BRAND_TYPES.VIDEO_CONTENT,
          ad_number:"vd"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }
      else if(req.files[i].fieldname === 'cloesdimg'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3ClosedImgLocation = s3response;
      }
      else if(req.files[i].fieldname === 'hippa'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3DocOneLocation = s3response;
      }
      else if(req.files[i].fieldname === 'LC'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3DocTwoLocation = s3response;
      }
      else if(req.files[i].fieldname === 'other'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        s3DocThreeLocation = s3response;
      }
      else if(req.files[i].fieldname==='bottomad1'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"ADVERTISEMENT",
          value:BRAND_TYPES.ADVERTISEMENT,
          ad_number:"bottomad1"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }else if(req.files[i].fieldname==='bottomad2'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"ADVERTISEMENT",
          value:BRAND_TYPES.ADVERTISEMENT,
          ad_number:"bottomad2"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }else if(req.files[i].fieldname==='bottomad3'){
        s3response = await s3.upload(params).promise();
        delete req.files[i].buffer;
        let s3AdvertisementLocationObj={
          s3response:s3response,
          key:"ADVERTISEMENT",
          value:BRAND_TYPES.ADVERTISEMENT,
          ad_number:"bottomad3"
        }
        s3AdvertisementLocation.push(s3AdvertisementLocationObj);
      }
      // else if(req.files[i].fieldname === 'ET'){
      //   s3response = await s3.upload(params).promise();
      //   delete req.files[i].buffer;
      //   s3ETLocation = s3response;
      // }
    }
  }

  const parent = await account.query().findById(req.body.parent_id);
  console.log(parent.account_lineage);

  const brandObject={
    name: req.body.brandname,
    phone: req.body.generalphone,
    website: req.body.website,
    fax: req.body.fax,
    email: req.body.generalemail,
    type_id: BRAND_TYPES.BRAND_LOGO,
    logo_url: s3BrandLocation.Location,
    //email_template_url: s3ETLocation.Location
  };
  
  const brand_response = await brand.query().insertGraphAndFetch(brandObject);
  const accountObject = {
    name: `${req.body.name}`,
    brand_id: brand_response.id===null || brand_response.id==="" ? null : brand_response.id,
    picture_url: s3AvatarLocation.Location,
    account_connections_id: req.body.account_connections_id, 
     is_payment_required: req.body.is_payment_required,
     demo_email: req.body.demo_email, //Added by Kishan
     demo_phone: req.body.demo_smsphone, //Added by Kishan
    contract_language: `${req.body.contractLang}` ,
    expiration_days: `${req.body.Expiration_Day}`,
    pick_up_reminder_day: `${req.body.pick_up_reminder_day}`,
    is_advance_advertise: req.body.is_advance_advertise,
    is_allow_consult_video: req.body.allow_consult_video,
    is_allow_consult_mobile: req.body.allow_consult_mobile,
    is_allow_consult_schedule: req.body.allow_consult_schedule,
    isContactLessPickupEnabled: req.body.isContactLessPickupEnabled,
    contact: {
      name: `${req.body.first_name} ${req.body.last_name}`,
      phone: req.body.phone,
      email: req.body.email,
      address: {
        name: "account_address",
        address_line_1: req.body.address1,
        address_line_2: req.body.address2,
        city: req.body.city,
        state: req.body.state,
        zip: req.body.zip
      }
    },
    parent: {
      "#dbRef": req.body.parent_id
    },
    type: {
      "#dbRef": req.body.type_id
    },
  };
  try {
    const a = await account.query().insertGraphAndFetch(accountObject);
    const b = Object.assign({}, a, { account_lineage:  parent.account_lineage + '.' + a.id.split('-').join('') });
    let c = await account.query().upsertGraphAndFetch(b);
    c.brand = brand_response;
    let _brandDetails = [];
    for(let i=0 ; i<s3AdvertisementLocation.length ; i++){
      const brandDetailObj = {
        type_id: s3AdvertisementLocation[i].value,
        logo_url:s3AdvertisementLocation[i].s3response.Location,
        parent_account_id: a.id,
        ad_number: s3AdvertisementLocation[i].ad_number
      }
      const brand_detail = await BrandDetail.query().insertGraphAndFetch(brandDetailObj);
      _brandDetails.push(brand_detail);
    }
    c.brand_detail = _brandDetails;

    let ObjHoursOfOperations = {
      account_id: a.id,
      is_sunday: req.body.is_sunday,
      is_monday: req.body.is_monday,
      is_tuesday: req.body.is_tuesday,
      is_wednesday: req.body.is_wednesday,
      is_thursday: req.body.is_thursday,
      is_friday: req.body.is_friday,
      is_saturday: req.body.is_saturday,
      open_time: req.body.OpenTime,
      close_time: req.body.CloseTime,
      hop_type: req.body.HopType,
      closedimg_url: s3ClosedImgLocation.Location,
      closed_msg: req.body.closedMsg
    }
    const Hours_Of_Operation = await HoursOfOperation.query().insertGraphAndFetch(ObjHoursOfOperations);
    c.hoursof_operations = Hours_Of_Operation;

    const conn = await AccountConnection.query().where('id',c.account_connections_id);
    if(conn)
    {
     c.connection = {
       id: conn[0].id,
       name: conn[0].name
     }
    }

    let objConnectionDetails = {
      account_id: a.id,
      account_connection_id: req.body.account_connections_id,
      endpoint: req.body.endpoint,
      pharmacyid: req.body.pharmacyid,
      vendor_key:req.body.vendor_key,
      data_exchange_id:req.body.data_exchange_id,
      epic_client_id : req.body.epicclientid,
      epic_username: req.body.epicusername,
      epic_password: req.body.epicpassword,
      medbank_dblogin: req.body.medbank_dblogin,
      medbank_db_password: req.body.medbank_db_password,
      opc_apim_subscription_key: req.body.opc_apim_subscription_key
    }
    const conResult = await AccountConnectionDetails
    .query()
    .insertGraphAndFetch(objConnectionDetails, { relate: true, unrelate: true });

    c.connection_details = conResult;

    const healthandinfoArray = JSON.parse(req.body.Health_and_info);
    let _healthAndInformationVideos = [];
    if(healthandinfoArray.length > 0){      
      for(let i=0; i<healthandinfoArray.length; i++)
      {
        const Health_and_infoobj = {
          title: healthandinfoArray[i].title,
          description : healthandinfoArray[i].description,
          video_url : healthandinfoArray[i].video_url,
          category_id : healthandinfoArray[i].category_id,
          primarylibrary_id: healthandinfoArray[i].primarylibrary_id,
          account_id : a.id,
          primarylibrary_title1: req.body.primaryLibtitle1,
          primarylibrary_title2: req.body.primaryLibtitle2

        }
        const Healthandinformations = await Health_and_information_master.query().insertGraphAndFetch(Health_and_infoobj);
        _healthAndInformationVideos.push(Healthandinformations) ;
      }      
    } 
    c.health_and_informations = _healthAndInformationVideos;
    const LegalAgreementObj = {
            account_id: a.id,
            legal_compliance_docone_label: req.body.doc_one,
            legal_compliance_docone_url: s3DocOneLocation.Location,
            legal_compliance_doctwo_label: req.body.doc_two,
            legal_compliance_doctwo_url: s3DocTwoLocation.Location,
            legal_compliance_docthree_label: req.body.doc_three,
            legal_compliance_docthree_url: s3DocThreeLocation.Location,
            legal_compliance_docone_hash: sha256(`${s3DocOneLocation.Location}`),
            legal_compliance_doctwo_hash: sha256(`${s3DocTwoLocation.Location}`),
            legal_compliance_docthree_hash: sha256(`${s3DocThreeLocation.Location}`),
          }                
          const Legal_agreement = await LegalAgreement.query().insertGraphAndFetch(LegalAgreementObj);
          c.legal_compliance = Legal_agreement;

    try {
      const secretKey = process.env.CIPHER;
      const crypter = new Crypter(secretKey);
      const gbpObject = {
        parent_account_id: c.id,
        tokenapikey: (req.body.tokenxgbapikey === undefined || req.body.tokenxgbapikey === 'undefined' || req.body.tokenxgbapikey === null || req.body.tokenxgbapikey === 'null' || req.body.tokenxgbapikey === '') ? '' : crypter.encrypt(req.body.tokenxgbapikey),
        transapikey: (req.body.transxgbapikey === undefined || req.body.transxgbapikey === 'undefined' || req.body.transxgbapikey === null || req.body.transxgbapikey === 'null' || req.body.transxgbapikey === '') ? '' : crypter.encrypt(req.body.transxgbapikey),
        transapisecret: (req.body.transxgbapisecret === undefined || req.body.transxgbapisecret === 'undefined' || req.body.transxgbapisecret === null || req.body.transxgbapisecret === 'null' || req.body.transxgbapisecret === '') ? '' : crypter.encrypt(req.body.transxgbapisecret),
        allow_payment: (req.body.allow_payment === "true" || req.body.allow_payment === true) ? true : false
      }      
      const g = await GbpConfig.query().insertGraphAndFetch(gbpObject);
      c.gbpconfig = g;
    } catch(error) {
      console.log(`Error while saving iPayConfiguration with details : ${JSON.stringify(error)}`);
    }

    if (req.body.account_connections_id === ConnectionType.EPIC) {
      try {
        const isTemplateExist = await Templates.query().where('account_id', c.id).first();
        if (isTemplateExist === undefined || isTemplateExist === null) {
          const templateObject = {
            account_id: c.id,
            versions: (req.body.epicEndpointVersion === undefined || req.body.epicEndpointVersion === 'undefined' || req.body.epicEndpointVersion === null || req.body.epicEndpointVersion === 'null' || req.body.epicEndpointVersion === '') ? '' : req.body.epicEndpointVersion,
            request_template: (req.body.epicRequestTempate === undefined || req.body.epicRequestTempate === 'undefined' || req.body.epicRequestTempate === null || req.body.epicRequestTempate === 'null' || req.body.epicRequestTempate === '') ? '' : req.body.epicRequestTempate,
            pharmacy_ncpdpid: (req.body.epicPharmacyNcpdpId === undefined || req.body.epicPharmacyNcpdpId === 'undefined' || req.body.epicPharmacyNcpdpId === null || req.body.epicPharmacyNcpdpId === 'null' || req.body.epicPharmacyNcpdpId === '') ? '' : req.body.epicPharmacyNcpdpId,
          }
          const temp = await Templates.query().insertGraphAndFetch(templateObject);
          c.template = temp;
        }
      } catch (error) {
        console.log(`Error while saving epic request template with details : ${JSON.stringify(error)}`);
        res.send(
          JSON.stringify({
            message: error.message,
            stack: error.stack
          })
        );
      }
    }
    if(req.body.accContactPersons && req.body.accContactPersons.length > 0){
      let accContacts = JSON.parse(req.body.accContactPersons);
      let _localAcp = [];
      if(accContacts && accContacts.length > 0) {      
        for (let i = 0; i < accContacts.length; i++) {
          let singleAgent = {
            account_id: c.id,
            first_name: accContacts[i].first_name,
            last_name: accContacts[i].last_name,
            email: accContacts[i].email,
            phone: accContacts[i].phone,
            isdefault: accContacts[i].isdefault,
          }
          const _acpResponse = await AccountContactPersons.query().insertGraphAndFetch(singleAgent);
          _localAcp.push(_acpResponse);
        }  
      }
      c.accContactPersons = _localAcp;
    }
    else{
      c.accContactPersons = [];
    }
    res.send(
      JSON.stringify({
        account: c
      })
    );
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

router.put("/boxes", upload.any(), async (req, res, next) => {  
  var s3response = {
    Location: req.body.file
  };
  if (req.files && req.files.length > 0 && req.files[0].buffer) {
    var params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(), // req.file.filename,
      Body: req.files[0].buffer,
      ACL: "public-read",
      ContentType: req.files[0].mimetype
    };
    s3response = await s3.upload(params).promise();
    delete req.files[0].buffer;
  }
  const boxObject = {
    id: req.body.id,
    name: `${req.body.name}`,
    description: `${req.body.description}`,
    picture_url: s3response.Location,
    IsActive: req.body.IsActive,
    kiosk_box_id: req.body.kiosk_box_id,
    location_notes: req.body.location_notes,
    location_type_id: req.body.location_type_id,
    address: {
          name: "box_address",
          address_line_1: req.body.address_line_1,
          address_line_2: req.body.address_line_2,
          city: req.body.city,
          state: req.body.state,
          zip: req.body.zip
      },
      account: {
        "#dbRef": req.body.parent_account_id
      },
      // type: {
      //   "#dbRef": req.body.type_id === 'null' ? null : req.body.type_id
      // },
      type_id: req.body.type_id === 'null' ? null : req.body.type_id,
    xwebid: req.body.xwebid,
    xwebauthkey: req.body.xwebauthkey,
    xwebterminalid: req.body.xwebterminalid
  };
  console.log(boxObject);

  try {
    const b = await box.query() // .insertGraphAndFetch(boxObject);
      .upsertGraphAndFetch(boxObject, {
        relate: true,
        unrelate: true
      });
    console.log(b);
    const Hours_OF_Operation_ac = await HoursOfOperation.query().skipUndefined().where('account_id', req.body.parent_account_id);
    if(Hours_OF_Operation_ac && Hours_OF_Operation_ac.length >0){
      for(let j=0 ; j<Hours_OF_Operation_ac.length ; j++){
        const HOPdelete =  await HoursOfOperation.query().deleteById(Hours_OF_Operation_ac[j].id);
      }
    }
    let sunday ;
    if(req.body.Sunday === null || req.body.Sunday === '' || req.body.Sunday === undefined){
      sunday = false;
    }else{
      sunday = req.body.Sunday;
    }
    let monday;
    if(req.body.Monday === null || req.body.Monday === '' || req.body.Monday === undefined){
      monday = false;
    }
    else{
      monday = req.body.Monday;
    }
    let tuesday;
    if(req.body.Tuesday === null || req.body.Tuesday === '' || req.body.Tuesday === undefined){
      tuesday = false;
    }
    else{
      tuesday = req.body.Tuesday;
    }
    let wednesday;
    if(req.body.Wednesday === null || req.body.Wednesday === '' || req.body.Wednesday === undefined){
      wednesday = false;
    }
    else{
      wednesday = req.body.Wednesday;
    }
    let thursday;
    if(req.body.Thursday === null || req.body.Thursday === '' || req.body.Thursday === undefined){
      thursday = false;
    }
    else{
      thursday = req.body.Thursday;
    }
    let friday;
    if(req.body.Friday === null || req.body.Friday === '' || req.body.Friday === undefined){
      friday = false;
    }
    else{
      friday = req.body.Friday; 
    }
    let saturday;
    if(req.body.Saturday === null || req.body.Saturday === '' || req.body.Saturday === undefined){
      saturday = false;
    }
    else{
      saturday = req.body.Saturday;
    }
    const HoursOfOperationObject = {
      account_id: req.body.parent_account_id,
      is_sunday: sunday,
      is_monday: monday,
      is_tuesday: tuesday,
      is_wednesday: wednesday,
      is_thursday: thursday,
      is_friday: friday,
      is_saturday: saturday,
      open_time: req.body.OpenTime,
      close_time: req.body.CloseTime,
      hop_type: req.body.HopType
    }
    const Hours_Of_Operation = await HoursOfOperation.query().insertGraphAndFetch(HoursOfOperationObject);
    b.hoursof_operations = Hours_Of_Operation

    let expansion = await ExpansionOrdering(req.body.box_expansions);
    let unitStringified = expansion;
    const getBoxExpansonsByBoxId = await boxExpansions.query().where('box_id', req.body.boxId).andWhere('is_deleted', false).andWhere('is_enabled', true);
    if (expansion && expansion.length > 0) {
      if (getBoxExpansonsByBoxId && getBoxExpansonsByBoxId.length > 0) {
        unitStringified.forEach(async (element) => {
          try {
            await boxExpansions.query().where('id', element.id).patch({
              box_id: req.body.id,
              expansion_id: element.expansion_id,
              connected_box_id: element.connected_box_id,
              direction: element.direction,
              box_position: element.ExpIndex,
              max_temp: element.max_temp,
              target_temp: element.target_temp,
              min_temp: element.min_temp
            });
          } catch (error) {
            console.log(error);
          }
        });
      } else {
        unitStringified.forEach(async (element) => {
          try {
            const expansionObject = {
              box_id: req.body.boxId,
              expansion_id: element.expansion_id,
              connected_box_id: element.connected_box_id,
              direction: element.direction,
              box_position: element.ExpIndex,
              max_temp: (element.max_temp === undefined || element.max_temp === 'undefined' || element.max_temp === '' || element.max_temp === null || element.max_temp === 'null') ? 45 : element.max_temp,
              target_temp: (element.target_temp === undefined || element.target_temp === 'undefined' || element.target_temp === '' || element.target_temp === null || element.target_temp === 'null') ? 40 : element.target_temp,
              min_temp: (element.min_temp === undefined || element.min_temp === 'undefined' || element.min_temp === '' || element.min_temp === null || element.min_temp === 'null') ? 35 : element.min_temp
            }
            await boxExpansions.query().insertGraphAndFetch(expansionObject)
          } catch (error) {
            responses.push({
              message: error.message,
              stack: error.stack
            });
          }
        });
      }
    } else {
      for (let b = 0; b < getBoxExpansonsByBoxId.length; b++) {
        await boxExpansions.query().where('id', getBoxExpansonsByBoxId[b].id).patch({
          is_deleted: true,
          is_enabled: false
        });
      }
    }
    res.send(
      JSON.stringify({
        box: b
      })
    );
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

router.post("/boxes", upload.any(), async (req, res, next) => {

    var s3response = { Location: "" };

    if (req.files && req.files.length > 0 && req.files[0].buffer) {
        var params = {
        Bucket: process.env.IMAGES_BUCKET,
        Key: uuidv4(), // req.file.filename,
        Body: req.files[0].buffer,
        ACL: "public-read",
        ContentType: req.files[0].mimetype
        };
        s3response = await s3.upload(params).promise();
        delete req.files[0].buffer;
    }

    const boxObject = {
      name: `${req.body.name}`,
      description: `${req.body.description}`,
      picture_url: s3response.Location,
      IsActive: req.body.IsActive,
      kiosk_box_id: req.body.kiosk_box_id,
      location_notes: req.body.location_notes,
      location_type_id: req.body.location_type_id,
      address: {
          name: "box_address",
          address_line_1: req.body.address_line_1,
          address_line_2: req.body.address_line_2,
          city: req.body.city,
          state: req.body.state,
          zip: req.body.zip
      },
      account: {
        "#dbRef": req.body.parent_account_id
      },
      type: {
        "#dbRef": req.body.type_id === 'undefined' ? null : req.body.type_id
      },
      xwebid: req.body.xwebid,
      xwebauthkey: req.body.xwebauthkey,
      xwebterminalid: req.body.xwebterminalid
    };
    let boxId;
    console.log(boxObject);
    try {
      const b = await box.query().insertGraphAndFetch(boxObject);
      boxId = b.id;
      console.log("Box Id is : " , boxId);
      console.log(b);
      const HoursOfOperationObject = {
        account_id: req.body.parent_account_id,
        is_sunday: req.body.Sunday,
        is_monday: req.body.Monday,
        is_tuesday: req.body.Tuesday,
        is_wednesday: req.body.Wednesday,
        is_thursday: req.body.Thursday,
        is_friday: req.body.Friday,
        is_saturday: req.body.Saturday,
        open_time: req.body.OpenTime,
        close_time: req.body.CloseTime,
        hop_type: req.body.HopType
      }
      const Hours_Of_Operation = await HoursOfOperation.query().insertGraphAndFetch(HoursOfOperationObject);
      b.hoursof_operations = Hours_Of_Operation

      let expansion =await ExpansionOrdering(req.body.box_expansions);
      let unitStringified = expansion; 
      let promisesInsertToWait=[];
      unitStringified.forEach(element => {
        try {
          const expansionObject = {
            box_id: boxId,
            expansion_id: element.expansion_id,
            connected_box_id: element.connected_box_id,
            direction: element.direction,
            box_position:element.ExpIndex,
            max_temp: (element.max_temp === undefined || element.max_temp === 'undefined' || element.max_temp === '' || element.max_temp === null || element.max_temp === 'null') ? 45 : element.max_temp,
            target_temp: (element.target_temp === undefined || element.target_temp === 'undefined' || element.target_temp === '' || element.target_temp === null || element.target_temp === 'null') ? 40 : element.target_temp,
            min_temp: (element.min_temp === undefined || element.min_temp === 'undefined' || element.min_temp === '' || element.min_temp === null || element.min_temp === 'null') ? 35 : element.min_temp
          }          
          promisesInsertToWait.push(boxExpansions.query().insertGraphAndFetch(expansionObject));
        } catch (error) {
          responses.push({
            message: error.message,  
              stack: error.stack
          });
        }
      });
      await Promise.all(promisesInsertToWait).catch(error => {
        res.send(error);
      });   
      res.send(
        JSON.stringify({
          // body: req.body,
          // files: req.files,
          // s3: s3response,
          box: b
        })
      );
    } catch (error) {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }      
  });

/*
  Added By : Belani Jaimin
  Added Date : 01/09/2019
  Description : to get consult flag is on or not
*/

//Get all entries for consult flag is on or off
router.get("/userconsult", async (req, res, next) => {
  const user_consults = await user_consult.query().where('is_user_consult',true);
  res.send(user_consults);
});

//Get flag value by RxPortal user id
router.get("/userconsultflag/:id", async (req, res, next) => {
  const user_consults = await user_consult.query().findById(req.params.id);
  res.send(user_consults);
});


//Update or save RxPortal flag
router.post("/userconsultupdate", async (req, res, next) => {
  const consultObject = {
    is_user_consult:req.body.is_user_consult,
    user_id:req.body.user_id
  }

  try{
    const user_consults = await user_consult.query().findById(consultObject.user_id);
    if(user_consults!=null && user_consults!=""){
      const uc = await user_consult.query()
      .upsertGraphAndFetch(consultObject,
      {
        relate:true,
        unrelate:false
      });
      res.send(
        JSON.stringify({
          user_consult : uc
        })
      );
    }else{
      const ucai = await user_consult.query().insertGraphAndFetch(consultObject);
      res.send(
        JSON.stringify({
          user_consult : ucai
        })
      );
    }

  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

/*
  Added By : Belani Jaimin
  Added Date : 01/10/2019
  Description : Notifications
*/

//Push Notification for RxPortal user
router.post("/pushconsultnotification", async (req, res, next) => {
  const consultnotifyObject = {
    User_name:req.body.User_name,
    is_consult_accepted:req.body.is_consult_accepted,
    is_consult_end_call:req.body.is_consult_end_call,
    is_user_end_call:req.body.is_user_end_call,
    resource_id:req.body.resource_id,
    user_id:req.body.user_id,
    consult_start_date:null,
    consult_end_date:null,
    auth_token:req.body.auth_token,
    is_call_status:req.body.is_call_status,
    is_anyone_accepted:false,
    accounts_id:req.body.accounts_id,
    store_name:req.body.store_name,
    box_id:req.body.box_id,
    kiosk_name:req.body.kiosk_name,
    is_releaseorder:req.body.is_releaseorder
  }
  try{
    const uca = await user_consult_notifications.query().insertGraphAndFetch(consultnotifyObject);
    res.send(
      JSON.stringify({
        user_consult_notifications : uca
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Get Notification for Kiosk User
router.get("/getNotifications", async (req, res, next) => {
  const user_consult_notification = await user_consult_notifications.query().where('is_consult_accepted',true);
  res.send(user_consult_notification);
});


//Get Notifications for other RxPortal users except current user
router.get("/getNotificationsforother", async (req, res, next) => {
  const user_consult_notification = await user_consult_notifications.query();
  res.send(user_consult_notification);
});

//Get Notification by RxPortal user id
router.get("/getNotificationsByUserId/:id", async (req, res, next) => {
  try {
    if (req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined) {
      const user_consult_notification
        = await user_consult_notifications.query().where('user_id', req.params.id);
      res.send(user_consult_notification);
    }
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Update notification status to connected when RxPortal user accept consult call
router.post("/notificationUpdate", async (req, res, next) => {
  try{
    const ucn = await user_consult_notifications.query()
    .update({is_consult_accepted: req.body.is_consult_accepted, consult_start_date: new Date(),is_anyone_accepted:true,consultant_name:req.body.consultant_name})
    .where('user_consult_notification_id', req.body.user_consult_notification_id);
    res.send(
      JSON.stringify({
        user_consult_notifications : ucn
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Update status from Kiosk side if RxPortal user accept call
router.post("/updateStatusOnSuccess", async (req, res, next) => {
  try{
    const ucn = await user_consult_notifications.query()
    .update({is_call_status: req.body.is_call_status})
    .where('user_consult_notification_id', req.body.user_consult_notification_id);
    res.send(
      JSON.stringify({
        user_consult_notifications : ucn
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Update status of call if user end call for Kiosk and RxPortal
router.post("/endconsultsession", async (req, res, next) => {
  try{
    const ucne = await user_consult_notifications.query()
    .update({
      is_call_status: req.body.is_call_status,
      consult_end_date:new Date(),
      is_consult_end_call:req.body.is_consult_end_call,
      is_user_end_call:req.body.is_user_end_call,
      is_anyone_accepted:false
    })
    .where('user_consult_notification_id', req.body.user_consult_notification_id);

    if(req.body.is_call_status === "Decline")
    {
      const updateConsultDetail = await user_consult_notifications.query().where('user_consult_notification_id', req.body.user_consult_notification_id).first();
      if(updateConsultDetail && updateConsultDetail !== undefined)
      {
        await saveConsultMaster(updateConsultDetail,req.body.is_call_status);
      }
    }
    res.send(
      JSON.stringify({
        user_consult_notifications : ucne
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
async function saveConsultMaster(notificationDetail,status)
{
  const userconsultmasterObject = {
    kiosk_name:notificationDetail.kiosk_name,
    store_name:notificationDetail.store_name,
    consult_start_date:notificationDetail.consult_start_date,
    consult_end_date:notificationDetail.consult_end_date,
    pickup_code:notificationDetail.resource_id,
    user_name:notificationDetail.User_name,
    call_duration:null,
    user_id:notificationDetail.user_id,
    consultation_name:notificationDetail.consultant_name,
    Status:status,
    accounts_id:notificationDetail.accounts_id,
    box_id:notificationDetail.box_id
  }
  try{
    const ucas = await user_consult_master.query().insertGraphAndFetch(userconsultmasterObject);
    console.log(JSON.stringify(ucas));
  }catch(error){
    console.log(JSON.stringify(error));
  }
}
//Delete notifications for specific box if 10 sec is passed.
router.post("/notificationdelete/:box_id", async (req, res, next) => {
  try{
    const notificationDetail = await user_consult_notifications.query()
    .where('box_id', req.params.box_id)
    .andWhere('is_call_status','Pending').first();

    const ucn = await user_consult_notifications.query()
    .del()
    .where('box_id', req.params.box_id)
    .andWhere('is_call_status','Pending');

    res.send(
      JSON.stringify({
        user_consult_notifications : ucn,
        notificationDetail : notificationDetail
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Delete unnecessary notifications if call is accepted.
router.delete("/usernotificationdelete/:id", async (req, res, next) => {
  const idsToDelete = [];
  if (req.params.id.indexOf(",") > -1) {
    req.params.id.split(",").map(x => idsToDelete.push(x));
  } else {
    idsToDelete.push(req.params.id);
  }
  const responses = [];
  const promisesToWait = [];
  idsToDelete.forEach(element => {
    try {
      promisesToWait.push(user_consult_notifications.query().deleteById(element));
    } catch (error) {
      responses.push({
        message: error.message,
          stack: error.stack
      });
    }
  });

  await Promise.all(promisesToWait).catch(error => {
    res.send(error);
  });
  res.send(responses);
});


//Get Notification by id
router.get("/getNotificationByid/:id", async (req, res, next) => {
  const user_consult_notification = await user_consult_notifications.query()
  .select('user_consult_notification_id',
  'is_consult_accepted',
  'is_consult_end_call',
  'is_user_end_call',
  'User_name',
  'resource_id',
  'user_id',
  user_consult_notifications.raw("to_char(user_consult_notifications.consult_start_date,'yyyy/mm/dd HH24:MI:SS') as consult_start_date"),
  user_consult_notifications.raw("to_char(user_consult_notifications.consult_end_date,'yyyy/mm/dd HH24:MI:SS') as consult_end_date"),
  'auth_token',
  'is_call_status',
  'is_anyone_accepted',
  'consultant_name',
  'accounts_id',
  'store_name',
  'box_id',
  'kiosk_name'
  )
  .findById(req.params.id).first();
  res.send(user_consult_notification);
});

//Save entry to user consult master for KPI Dashboard and Reports.
router.post("/saveuserconsult", async (req, res, next) => {
  try{
    let userConsultID;
    if(req.body.Status === 'Offline'){
       userConsultID = await user_consult_master.query().select('user_consult_master_id').where('pickup_code', req.body.pickup_code ).andWhere('user_name',req.body.user_name).andWhere('Status','Offline').first();  
    }else {
       userConsultID = await user_consult_master.query().select('user_consult_master_id').where('pickup_code', req.body.pickup_code ).andWhere('user_name',req.body.user_name).andWhere('Status','Disconnected').first();
    }
    if(userConsultID && userConsultID.user_consult_master_id) {
      const UpdateuserconsultmasterObject = {
        user_consult_master_id:userConsultID.user_consult_master_id,
        kiosk_name:req.body.kiosk_name,
        store_name:req.body.store_name,
        consult_start_date:req.body.consult_start_date,
        consult_end_date:req.body.consult_end_date,
        pickup_code:req.body.pickup_code,
        user_name:req.body.user_name,
        call_duration:req.body.call_duration,
        user_id:req.body.user_id,
        consultation_name:req.body.consultation_name,
        Status:req.body.Status,
        accounts_id:req.body.accounts_id,
        box_id:req.body.box_id,
        consultation_notes:req.body.consultation_notes,
        is_releaseorder:req.body.is_releaseorder
      }
      const ucas = await user_consult_master.query().upsertGraphAndFetch(UpdateuserconsultmasterObject);  
      res.send(
        JSON.stringify({
          user_consult_master : ucas
        })
      );
    }
    else{
      const userconsultmasterObject = {
        kiosk_name:req.body.kiosk_name,
        store_name:req.body.store_name,
        consult_start_date:req.body.consult_start_date,
        consult_end_date:req.body.consult_end_date,
        pickup_code:req.body.pickup_code,
        user_name:req.body.user_name,
        call_duration:req.body.call_duration,
        user_id:req.body.user_id,
        consultation_name:req.body.consultation_name,
        Status:req.body.Status,
        accounts_id:req.body.accounts_id,
        box_id:req.body.box_id,
        consultation_notes:req.body.consultation_notes,
        is_releaseorder:req.body.is_releaseorder
      }
      const ucas = await user_consult_master.query().insertGraphAndFetch(userconsultmasterObject);
      res.send(
        JSON.stringify({
          user_consult_master : ucas
        })
      );
    }   
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

// function GenerateMaxNumber(maxPatientId){
//   let maxid = maxPatientId=="" && maxPatientId==undefined ? 100001 : parseInt(maxPatientId) + 1;
//   return maxid;
// }

// Save customer entry in customer table
router.post("/savecustomer", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
    const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
    const boxes = await box.query()
      .andWhere('id', '=', req.body.parent_box_id)
      .eager("[address, type, schedule]");
    let patient_id = req.body.patient_id !== 'null' && req.body.patient_id !== null && req.body.patient_id !== undefined && req.body.patient_id !== 'undefined' && req.body.patient_id !== '' ? req.body.patient_id : null;
    let p = await customer.query()
      .where('patient_id', patient_id.toString())
      .andWhere('is_deleted', false)
      .andWhere('is_enabled', true)
      .andWhere('parent_account_id', 'in', accountIds.map(x => x.id));
    const accountId = await account.query()
      .innerJoin('account_connections', 'account_connections.id', 'accounts.account_connections_id')
      .where('accounts.id', boxes[0].parent_account_id)
      .select('accounts.id');
    const _ac = await AccountConnectionDetails.query().where('account_id', boxes[0].parent_account_id).eager("[accountConnections]").first();
    console.log(JSON.stringify(_ac.accountConnections));
    let _custType = null;
    switch (_ac.accountConnections.id) {
      case ConnectionType.UNPLUGGED:
        _custType = customer_type_enum.Portal_Customer
        break;
      case ConnectionType.ILOCALBOX_PHARMACY:
        _custType = customer_type_enum.Portal_Customer
        break;
      case ConnectionType.QS1:
        _custType = customer_type_enum.PMS_CUSTOMERS
        break;
      case ConnectionType.PIONEERRX:
        _custType = customer_type_enum.PMS_CUSTOMERS
        break;
      case ConnectionType.EPIC:
        _custType = customer_type_enum.PMS_CUSTOMERS
        break;
      case ConnectionType.MEDBANK:
        _custType = customer_type_enum.PMS_CUSTOMERS
        break;
      default:
        break;
    }

    //added by Naresh | Date:09-04-2018 | get customer type 
    let customer_type_id = null;
    // const cus_type = await customer_type.query().where('name', customer_type_enum.Portal_Customer);  // Modified By : Belani Jaimin, To print status in Manage Patient.
    const cus_type = await customer_type.query().where('name', _custType);  // Changed to PMS customer. The customer will be save through Rx Assignment Screen.

    const cus_type_params = {
      name: customer_type_enum.Portal_Customer,
    };
    if (cus_type.length !== 0) {
      customer_type_id = cus_type[0].id;
    }
    else {
      try {
        const cus_type = await customer_type.query().insertGraphAndFetch(cus_type_params);
        customer_type_id = cus_type.id;
      }
      catch (error) {
        res.send(
          JSON.stringify({
            message: error.message,
            stack: error.stack
          })
        );
      }
    }
    //End Naresh
    const savecustomerobj = {
      name: `${req.body.first_name} ${req.body.last_name ? req.body.last_name : ''}`,
      parent_account_id: boxes[0].parent_account_id,
      first_name: req.body.first_name ? req.body.first_name : '',
      middle_name: req.body.middle_name ? req.body.middle_name : '',
      last_name: req.body.last_name ? req.body.last_name : '',
      dob_str: req.body.dob,
      username: req.body.email,
      patient_id: req.body.patient_id,
      type_id: customer_type_id, //added by Naresh | Date :09-04-2019
      locationtype_id: req.body.locationtype_id, // added by Varshit | Date :06-09-2020 | TASK :DI-1185 
      contacts: [
        {
          name: "Home",
          phone: req.body.phone,
          email: req.body.email
        }
      ]
    };
    try {
      let boxDetail;
      if (req.body.parent_box_id !== "undefined" && req.body.parent_box_id !== undefined && req.body.parent_box_id !== "null") {
        boxDetail = await box.query().where('id', req.body.parent_box_id).eager("[address]").first();
      }
      let pms;
      if (p.length !== 0) {
        // Upsert
        savecustomerobj.id = p[0].id;
        savecustomerobj.is_deleted = false;
        pms = await customer.query().upsertGraphAndFetch(savecustomerobj, {
          relate: true,
          unrelate: false
        });
      }
      else {
        if (accountId.length > 0) {
          const userParams = {
            username: savecustomerobj.username,
            name: savecustomerobj.name,
            email: req.body.email,
            phone: req.body.phone,
            parent_account_id: savecustomerobj.parent_account_id,
            patientName: savecustomerobj.name,
            first_name: req.body.first_name,
            boxName: boxDetail && boxDetail.name,
            boxAddress: boxDetail && boxDetail.address ?
              (boxDetail.address.address_line_1 + ' '
                + boxDetail.address.address_line_2 + ','
                + boxDetail.address.city + ', '
                + boxDetail.address.state + ' '
                + boxDetail.address.zip + '') : '',
          }
          try {
            const userRes = await createAWSCognitoUser(userParams);
            console.log(JSON.stringify(userRes));
          } catch (error) {
            console.log(`Error while saving cognito user : ${error}`);
          }
        }
        savecustomerobj.password_reset_on = new Date(); // TODO: Use getUTC for UTC date.
        // savecustomerobj.is_deleted = false;
        pms = await customer.query().insertGraphAndFetch(savecustomerobj);
      }

      let isactive = await CustomerLegalDocs.query()
        .innerJoin('customers', 'customers.id', 'customers_legal_documents.customer_id')
        .select('customers_legal_documents.is_active')
        .where('customers_legal_documents.customer_id', '=', pms.id);

      if(isactive.length > 0) {
        pms.isactive = isactive && isactive[0] && isactive[0].is_active;
      } else {
        pms.isactive = false;
      }
      

      let locations = await Customer_Locations_master.query().skipUndefined().where('customer_id', pms.id);
      if (locations && locations.length === 0) {
        const locationobj = {
          customer_id: pms.id,
          locationtype_id: req.body.locationtype_id,
          box_id: req.body.parent_box_id,
          name: '',
          addressline1: req.body.address1,
          addressline2: req.body.address2,
          city: req.body.city,
          state: req.body.state,
          zip: req.body.zip,
          country: req.body.country,
          is_default_address: true
        }
        const customerlocations = await Customer_Locations_master.query().insertGraphAndFetch(locationobj);
      }

      res.send(
        JSON.stringify({
          customer: pms
        })
      );
    } catch (error) {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
});

//Save entry to prescription master assignment prescription.
router.post("/saveprescription", async (req, res, next) => {
  let pickup_code_url =null;
  pickup_code_url = await generateQR(req.body.pucode)
  const saveprescriptions = {
    bin_id:req.body.bin_id,
    box_position:req.body.box_position,
    type_id: req.body.type_id, //awaiting
    locationtype_id: req.body.locationtype_id, 
    // parent_account_id:req.body.parent_account_id,
    customer: { "#dbRef": req.body.customer_id},
    stock_code:req.body.stock_code,
    pucode:req.body.pucode,
    // is_deleted:false,
    // is_enabled:true,
    // created_on:new Date(),
    // modified_on:null,
    // deleted_on:null,
    // stocked_date:new Date(),
    // fill_date:null,
    box_id:req.body.box_id,
    pickup_code_url:pickup_code_url,
    source_location: req.body.source_location,
    delivery_notes: req.body.delivery_notes,
    storage_location: req.body.storage_location,
    order_notes: req.body.order_notes
  }
  try{
    const pm = await prescriptions_master.query().insertGraphAndFetch(saveprescriptions);
    if(pm!==undefined && pm!=='undefined' && pm!=='' && pm!=='null' && pm!==null){
      try{
        const data = await prescriptions_master.query()
        .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
        .innerJoin('accounts', 'accounts.id', 'customers.parent_account_id')
        .innerJoin('brands', 'brands.id', 'accounts.brand_id')
        .where('prescriptions_master.id', pm.id)
        .select(
          'prescriptions_master.id as prescriptionID',
          'prescriptions_master.type_id as transactiontypeID',
          'customers.first_name',
          'customers.username as email_id',
          'customers.email_notification',
          'customers.sms_notification',
          'accounts.id as accountID',
          'accounts.name as accountName',
          'brands.id as brandID',
          'brands.name as Pharmacy_name',
          'brands.phone as Pharmacy_contact',
          'brands.email as Pharmacy_email'
        ).first();   
        const agentData = await AlternetAgents.query().select('agent_email').where('customer_id', req.body.customer_id).andWhere('isdefault', true);
        let ccEmailAddresses = [];
        for(let i=0; i<agentData.length; i++) {
          ccEmailAddresses.push(agentData[i].agent_email);
        }    
        let _emailData = {
          firstName : data.first_name,
          emailId : data.email_id,
          PharmacyName : data.accountName,
          pharmacyContact : data.Pharmacy_contact,
          ccEmails : ccEmailAddresses,
          locationtype_id: req.body.locationtype_id,
          location_notes: req.body.location_notes,
        } 
        
        if(req.body.locationtype_id === LocationsType.Kiosk) {
          _emailData.payNowLink = await GeneratePayNowLink(data.prescriptionID, data.accountID, data.transactiontypeID, data.brandID)
          if(data.email_notification === true) {
            const sentEmailResponse = await sendAssignNotificationEmail(_emailData);
            console.log(sentEmailResponse);
          }
        }        
        res.send(
          JSON.stringify({
            prescriptions_master : pm
          })
        );        
      }
      catch(error){
        res.send(error.message);
      }
    }
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Save entry to prescription detail assignment prescription.
router.post("/saveprescriptiondetail", async (req, res, next) => {
  const saveprescriptionsdetails = {
    parent_account_id:req.body.parent_account_id,
    rx_no:req.body.rx_no.toString(),
    drug_name:req.body.drug_name,
    ndc_code:req.body.ndc_code,
    price:req.body.price,
    expires_on:req.body.expires_on,
    qty:req.body.qty,
    strength:req.body.strength,
    route:req.body.route,
    doseage_form:req.body.doseage_form,
    non_proprietary_name:req.body.non_proprietary_name,
    is_deleted:false,
    is_enabled:true,
    created_on:new Date(),
    modified_on:null,
    deleted_on:null,
    fillnumber: req.body.fillNumber,
    is_consultrequire: req.body.is_consultrequire,
    prescriptionnumber: req.body.prescriptionnumber
  }
  try{
    const pd = await prescription_detail.query().insertGraphAndFetch(saveprescriptionsdetails);    
    res.send(
      JSON.stringify({
        prescription_detail : pd
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Get box details with retry
async function checkBox(thBoxId){
  let theBox  = null;
  if(thBoxId!==undefined && thBoxId!=='undefined' && thBoxId!=='' && thBoxId!=='null' && thBoxId!==null){
    try
    {
      console.log("Getting box details for kiosk box id : " + thBoxId);
      theBox = await box.query()
              .where('kiosk_box_id', thBoxId)
              .skipUndefined()
              .eager("[address, type, account.contact.address, account.connection, schedule]")
              .first();
      if(!theBox){
        console.log("Retry to get box details again...");
        theBox = await box.query()
                .where('kiosk_box_id', thBoxId)
                .skipUndefined()
                .eager("[address, type, account.contact.address, schedule]")
                .first();
        if(theBox) {
          // Check if email_send_date is not null then send online notification
          await CheckAndSendOnlineNotification(theBox);
          const boxobj={
            id: theBox.id,
            is_email_send: false,
            email_send_date: null,
            last_access_date: new Date()
          }
         const boxupdate = await box.query().upsertGraphAndFetch(boxobj, {
                            relate:true,
                            unrelate:false
                          } );
          console.log("box details update successfully!",JSON.stringify(boxupdate));
          return theBox;
        }
      } else{
        console.log("box details getting successfully!");
        // Check if email_send_date is not null then send online notification
        await CheckAndSendOnlineNotification(theBox);
        const boxobj={
          id: theBox.id,
          is_email_send: false,
          email_send_date: null,
          last_access_date:new Date()
        }
        const boxupdate = await box.query().upsertGraphAndFetch(boxobj, {
                          relate:true,
                          unrelate:false
                        } );
        console.log("box details update successfully!",JSON.stringify(boxupdate));
        return theBox;
      }
    }
    catch(error){
      console.log("Getting error for box with message : " + error.message);
      return error.message;
    }
  }
  return theBox;
}

//Get Brand Details for box by brand id stored in Account table
async function brandbyBox(brand_id){
  let brands = {};
  try{
    console.log("Getting brand for brand id : " + brand_id);
    const theBrand = await account.query()
    .innerJoin('brands','brands.id','accounts.brand_id')
    .select('brands.id','brands.name','brands.type_id','brands.logo_url', 'brands.phone', 'brands.website', 'brands.fax', 'brands.email')
    .where('brands.id', '=', brand_id).first();
    brands = theBrand;
    console.log("get brand successfully with details : " + JSON.stringify(brands));
  }
  catch(error){
    console.log("Getting error while getting brand with message : " + error.message);
    return error.message;
  }
  return brands;
}

//Get brand details for box by account
async function brandDetailsByBoxAccount(parent_account_id){
  let brand_details = [];
  try
  {
    console.log("Getting brand details for parent account : " + parent_account_id);
    const theBrandDetails = await account.query()
    .innerJoin('brand_detail','brand_detail.parent_account_id','accounts.id')
    .select('brand_detail.id','brand_detail.type_id','brand_detail.logo_url','brand_detail.parent_account_id','brand_detail.ad_number')
    .where('brand_detail.parent_account_id', '=', parent_account_id);
    brand_details = theBrandDetails;
    console.log("Get brand details done successfully with details : " + JSON.stringify(brand_details));
  }
  catch(error){
    console.log("Getting error while getting brand details by parent account with message : " + error.message);
    return error.message;
  }
  return brand_details;
}

//Get Permission details for box by account
async function permissionDetailsByBoxAccount(parent_account_id){
  let permission_details = [];
  try
  {
    console.log("Getting Permission details for parent account : " + parent_account_id);
    const thePermissionDetails = await account.query()
    .innerJoin('role_permission_details','role_permission_details.account_id','accounts.id')
    .innerJoin('permissions','permissions.id','role_permission_details.permission_id')
    .select('role_permission_details.id','role_permission_details.role_id','role_permission_details.permission_id','role_permission_details.permission_entity_type_id','role_permission_details.is_allowed')
    .where('role_permission_details.account_id', '=', parent_account_id)
    .andWhere('permissions.type_id','!=','6b163db7-f5e2-4cf8-b6d4-0345ed0bb6f3');
    permission_details = thePermissionDetails;
    console.log("Get permission details done successfully with details : " + JSON.stringify(permission_details));
  }
  catch(error){
    console.log("Getting error while getting permission details by parent account with message : " + error.message);
    return error.message;
  }
  return permission_details;
}
//Get Hours of Operation details For box
async function HoursOfOperationByBoxAccount(parent_account_id){
  let hoursofoperation = {};
  try{
    console.log("Getting Hours Of Operation Details for parent account:" + parent_account_id);
    const thehoursofoperationdetails = await account.query()
    .innerJoin('hours_of_operations' , 'hours_of_operations.account_id','accounts.id')
    .select('hours_of_operations.id','hours_of_operations.is_sunday','hours_of_operations.is_monday','hours_of_operations.is_tuesday','hours_of_operations.is_wednesday','hours_of_operations.is_thursday','hours_of_operations.is_friday','hours_of_operations.is_saturday','hours_of_operations.open_time','hours_of_operations.close_time','hours_of_operations.hop_type','hours_of_operations.closedimg_url','hours_of_operations.closed_msg')
    .where('hours_of_operations.account_id','=', parent_account_id)
    hoursofoperation = thehoursofoperationdetails;
    console.log("Get Hours OF Operations details succesfully with details : " + JSON.stringify(hoursofoperation));
  }
  catch(error){
    console.log("Getting error while getting Hours Of Operation details by box with message : " + error.message);
    return error.message;
  }
  return hoursofoperation;
}
//Get Health And Information details for Box
async function HealthAndInfoByBoxAccount(parent_account_id){
  let healthandinfo = [];
  try{
     console.log("Getting Health and information Details for parent account:" + parent_account_id);
     const  thehealthandinfodetails = await account.query()
     .innerJoin('health_and_information_master','health_and_information_master.account_id','accounts.id')
     .innerJoin('category_master','category_master.id','health_and_information_master.category_id')
     .select('health_and_information_master.id','health_and_information_master.account_id','health_and_information_master.category_id','category_master.name as category_name','health_and_information_master.title','health_and_information_master.description','health_and_information_master.video_url','health_and_information_master.primarylibrary_title1','health_and_information_master.primarylibrary_title2','health_and_information_master.primarylibrary_id')
     .where('health_and_information_master.account_id', '=',parent_account_id)
     healthandinfo = thehealthandinfodetails;
     console.log("Get Health And Information details Succesfully eith the details:" + JSON.stringify(healthandinfo));
    }
  catch(error){
    console.log("Getting error while getting Health and Information details by box with message: " + error.message);
    return error.message;
  }
  return healthandinfo;
}
//Get Legal_Compliances for Box By Account
async function legalcomplianceByBoxAccount(parent_account_id){
    let legal_compliances = [];
    try
    {
      console.log("Getting legal compliances for parent account : " + parent_account_id);
      const theLegalCompliances = await account.query()
      .innerJoin('legal_agreement_compliance','legal_agreement_compliance.account_id','accounts.id')
      .select('legal_agreement_compliance.legal_compliance_docone_hash',
              'legal_agreement_compliance.legal_compliance_docone_label',
              'legal_agreement_compliance.legal_compliance_docone_url',
              'legal_agreement_compliance.legal_compliance_doctwo_hash',
              'legal_agreement_compliance.legal_compliance_doctwo_label',
              'legal_agreement_compliance.legal_compliance_doctwo_url',
              'legal_agreement_compliance.legal_compliance_docthree_hash',
              'legal_agreement_compliance.legal_compliance_docthree_label',
              'legal_agreement_compliance.legal_compliance_docthree_url')
      .where('legal_agreement_compliance.account_id', '=', parent_account_id);
      legal_compliances = theLegalCompliances;
      console.log("Get legal compliances done successfully with details : " + JSON.stringify(legal_compliances));
    }
    catch(error){
      console.log("Getting error while getting brand details by parent account with message : " + error.message);
      return error.message;
    }
    return legal_compliances;
  }
//Check ComplianceIdentical 
async function customerComplianceIdentical(parent_account_id){
      let compliance = [];
      let agreement = [];
      let isIdentical = false;
      try{
                const theCustomerLegalDocs = await  CustomerLegalDocs.query() 
                    .where('customers_legal_documents.parent_account_id','=', parent_account_id);
                
                compliance  = theCustomerLegalDocs;
  
                console.log("Compliance==>",compliance);
  
                const theLegalAgreement =  await   LegalAgreement.query()  
                .where('legal_agreement_compliance.account_id','=', parent_account_id)      
                  agreement = theLegalAgreement;
                console.log("Legalagreement ==>",agreement);
  
        if(compliance.docone_policy_accepted_hash === agreement.legal_compliance_docone_hash &&
          compliance.doctwo_policy_accepted_hash === agreement.legal_compliance_doctwo_hash &&
          compliance.docthree_policy_accepted_hash === agreement.legal_compliance_docthree_hash){
              isIdentical = true;
          }
          else{
            isIdentical = false;
          }
          console.log("ISIDENTICAL==>",isIdentical);
      }
      catch(error){
        console.log("Getting error while getting Customer Compliance Identical details by parent account with message : " + error.message);
        return error.message;
      }
      return isIdentical;
  }
//get prescriptions for box by box
async function getPrescriptions(box_id){
  let prescriptions = [];
  try
  {
    console.log("Getting prescriptions for box with box id : " + box_id);
    const prescription_master = await prescriptions_master.query()
    .where('box_id',box_id)
    .andWhere('type_id', 'in', [RXSTATUS.ASSIGNED, RXSTATUS.STOCKED, RXSTATUS.HOLD])
    .skipUndefined().where ('is_deleted',false)
    .eager("[items, customer, customer.contacts]");
    if (prescription_master && prescription_master.length > 0) {
      for (let i = 0; i < prescription_master.length; i++) {
        const payments = await paymentModel.query().where('order_id', prescription_master[i].id).select('payments.is_paid').first();
        prescription_master[i].payments = { is_paid: false };
        if (payments) {
          prescription_master[i].payments = payments
        }
      }
    }
    prescriptions = prescription_master;
    console.log("The lenght of prescription is : " + prescriptions.length);
  }
  catch(error){
    console.log("Getting error while getting prescriptions for box with message : " + error.message);
    return error.message;
  }
  return prescriptions;
}

async function getBoxExpansions(box_id){
  let box_expansions = [];
  try{
    console.log("Getting box expansions with box id : " + box_id)
    if(box_id!==null && box_id!=='null' && box_id!==undefined && box_id!='undefined' && box_id!==''){
      // const be = await boxExpansions.query().where('box_id',box_id).andWhere('is_deleted',false).andWhere('is_enabled',true);

      const be = await boxExpansions.query()
      .eager('[box, boxes_type, connected_box_type]')                        
      .modifyEager('box', builder => builder.select('name'))
      .modifyEager('boxes_type', builder => builder.select('name'))
      .modifyEager('connected_box_type', builder => builder.select('name'))
      .skipUndefined()
      .where('box_id',box_id).andWhere('is_deleted',false).andWhere('is_enabled',true);

      box_expansions = be;
      console.log('Total exapansions are available for box id ' + box_id + ' is ' + box_expansions.length);
      console.log('Get box expansions done successfully with details : ' + JSON.stringify(box_expansions));
    }
  }
  catch(error)
  {
    console.log("Getting error while getting box extensions with message : " + error.message)
    return error.message;
  }
  return box_expansions;
}

router.get("/getCustomerLegalDocs/:id",async (req,res,next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
      const legalDocs = await CustomerLegalDocs.query().where('customer_id',req.params.id);
       res.send(legalDocs);       
    }
  catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
  //res.send(response);    
});

//Get all entries for prescriptions by box
router.get("/getprescriptions/:id", async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      res.status(401).send({
        message: 'You are not authorized to perform this action request.'
      })
    } else {
      const token = req.headers.authorization.split(' ')[1];
      // Note: Please do not allow this token verification step till we implement the middleware security.
      // Will continue with this step in phase 2 of security tokenization encryption.
      /* ------------------------------------------------------------------------------------------------ */
      // const verufyToken = verifyAuth(token, (error, decoded) => {
      //   if (!error)
      //     return true;
      // })
      /* ------------------------------------------------------------------------------------------------ */
      const verufyToken = true;
      if (verufyToken === true) {
        const response = {};
        let theBox = {};
        let prescription_master = [];
        if (req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined) {
          theBox = await checkBox(req.params.id);
          if (theBox !== undefined && theBox !== 'undefined' && theBox !== '' && theBox !== 'null' && theBox !== null) {
            console.log('Getting box with details : ' + JSON.stringify(theBox));
            if (theBox !== 'null' && theBox !== null && theBox !== undefined && theBox !== 'undefined' && theBox !== '') {
              theBox.account.brand = await brandbyBox(theBox.account.brand_id);
              theBox.account.brand_detail = await brandDetailsByBoxAccount(theBox.parent_account_id);
              theBox.account.permission_detail = await permissionDetailsByBoxAccount(theBox.parent_account_id);
              theBox.account.hoursof_operations = await HoursOfOperationByBoxAccount(theBox.parent_account_id);
              theBox.account.health_and_informations = await HealthAndInfoByBoxAccount(theBox.parent_account_id);
              theBox.account.legal_compliance = await legalcomplianceByBoxAccount(theBox.parent_account_id);
              theBox.account.customer_legalDocs = await customerComplianceIdentical(theBox.parent_account_id);
              prescription_master = await getPrescriptions(theBox.id);
              response.box = theBox;
              response.box.box_expansions = await getBoxExpansions(theBox.id);
              if (prescription_master.length !== 0) {
                response.box.customers = prescription_master.map(x => x.customer);
                for (let j = 0; j < response.box.customers.length; j++) {
                  if (response.box !== null
                    && response.box !== 'null'
                    && response.box !== undefined && response.box !== 'undefined'
                    && response.box !== '') {
                    if (response.box.customers[j] !== null
                      && response.box.customers[j] !== 'null'
                      && response.box.customers[j] !== ''
                      && response.box.customers[j] !== undefined
                      && response.box.customers[j] !== 'undefined') {
                      response.box.customers[j].orders = [];
                      for (let i = 0; i < prescription_master.length; i++) {
                        if (prescription_master[i].customer_id === response.box.customers[j].id) {
                          delete prescription_master[i].customer;
                          response.box.customers[j].orders.push(prescription_master[i]);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        res.send(response);
      } else {
        res.status(401).send({
          message: 'You are not authorized to perform this action request.',
          status: 401
        })
      }
    }
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Get all entries for boxes by store
router.get("/getBoxesByStoreId/:id", async (req, res, next) => {
  const presctiptions = await box.query().skipUndefined().where('parent_account_id', req.params.id).orderBy('name');
  res.send(presctiptions);
});

//Get all location boxes by location type id
router.get("/getBoxesById/:id", async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
        next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id)).andWhere('location_type_id', req.params.id)
      .select(
        'boxes.id',
        'boxes.name',
        'boxes.description',
        'boxes.parent_account_id',
        'boxes.type_id',
        'boxes.picture_url',
        'boxes.kiosk_box_id',
        'boxes.IsActive',
        'boxes.location_type_id',
        'boxes.location_notes'
      )
      .orderBy('name');
        // const boxes = await box.query().skipUndefined().where('location_type_id', req.params.id).orderBy('name');
        res.send(boxes);
    }
  }
  catch (error) {
      res.send(
          JSON.stringify({
              message: error.message,
              stack: error.stack
          })
      );
  }
});

// Update status of prescription in Event tabal and prescriptions master table and send the mail regarding operation
router.post("/updateStatusOfRx", async (req, res, next) => {
  const updateRxStatus = {
    id: req.body.id,
    type_id: req.body.type_id,
    stocked_date: req.body.stocked_date,
    pickup_date: req.body.pickup_date,
    pick_up_reminder_date: req.body.pick_up_reminder_date,
  }
  let multiplePickup = req.body.multiplePickup,
    rx_expiration_day = 1,
    pick_up_reminder_day = 1,
    rx_expiration_date = new Date(),
    rx_expiration_dateString = '',
    pick_up_reminder_date = new Date(),
    address_line1, address_line2, city, state, Zipcode;

  try {
    let accountSetting = await account.query()
      .innerJoin('boxes', 'boxes.parent_account_id', 'accounts.id')
      .innerJoin('prescriptions_master', 'prescriptions_master.box_id', 'boxes.id')
      .innerJoin('addresses', 'addresses.id', 'boxes.address_id')
      .where('prescriptions_master.id', updateRxStatus.id)
      .select(
        'accounts.id',
        'accounts.expiration_days',
        'accounts.pick_up_reminder_day',
        'prescriptions_master.stocked_date',
        'prescriptions_master.rx_expiration_date',
        'addresses.address_line_1',
        'addresses.address_line_2',
        'addresses.city',
        'addresses.state',
        'addresses.zip',
        'accounts.brand_id'
      ).first();
    
    rx_expiration_day = accountSetting.expiration_days !== null && accountSetting.expiration_days !== 'null' ? parseInt(accountSetting.expiration_days) : rx_expiration_day;
    if (rx_expiration_day > 0) {
      rx_expiration_date.setDate(rx_expiration_date.getDate() + rx_expiration_day);
      const mDate = moment(rx_expiration_date);
      rx_expiration_dateString = `${mDate.format('MM/DD/YYYY')} ${mDate.format('hh:mm A')} UTC`;
    }
    pick_up_reminder_day = accountSetting.pick_up_reminder_day !== null && accountSetting.pick_up_reminder_day !== 'null' ? parseInt(accountSetting.pick_up_reminder_day) : pick_up_reminder_day;
    if (pick_up_reminder_day > 0) {
      pick_up_reminder_date.setDate(pick_up_reminder_date.getDate() + pick_up_reminder_day);
    }
    updateRxStatus.rx_expiration_date = rx_expiration_date;
    updateRxStatus.pick_up_reminder_date = pick_up_reminder_date;

    // If operation is stocking.
    if (req.body.type_id === RXSTATUS.STOCKED) {
      const prescriptionsMaster = await prescriptions_master.query()
        .upsertGraphAndFetch(updateRxStatus,
          {
            relate: true,
            unrelate: false
          });
      const updatedPrescriptionMaster = await prescriptions_master.query()
        .skipUndefined()
        .where('is_deleted', false)
        .where('id', prescriptionsMaster.id)
        .eager("[items]").first();
      let customerDetails = await customer.query().where('id', prescriptionsMaster.customer_id).andWhere('is_deleted', false).andWhere('is_enabled', true).eager("[contacts]").first();
      let _emailData = req.body.email_data;
      _emailData.rx_expiration_date = rx_expiration_dateString;
      const alternetAgents = await AlternetAgents.query().where('customer_id', prescriptionsMaster.customer_id).andWhere('isdefault', true);
      let ccEmailAddresses = [];
      if (alternetAgents && alternetAgents.length > 0) {
        for (let i = 0; i < alternetAgents.length; i++) {
          ccEmailAddresses.push(alternetAgents[i].agent_email);
        }
      }
      _emailData.ccEmailAddresses = ccEmailAddresses;
      _emailData.email_notification = customerDetails.email_notification;
      if (_emailData.email_notification === true) {
        _emailData.payNowLink = await GeneratePayNowLink(req.body.id, accountSetting.id, req.body.type_id, accountSetting.brand_id);
      }
      const eventRes = await EventLogHandlerModule(req.body.event_params, _emailData);
      const accountConnections = await prescription_detail.query()
        .innerJoin('prescriptions_master', 'prescriptions_master.id', 'prescription_detail.parent_account_id')
        .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin('account_connection_details', 'account_connection_details.account_id', 'boxes.parent_account_id')
        .where('prescriptions_master.id', updatedPrescriptionMaster.id)
        .select(
          'prescription_detail.rx_no',
          'account_connection_details.account_id',
          'account_connection_details.account_connection_id',
          'account_connection_details.endpoint',
          'account_connection_details.epic_client_id',
          'account_connection_details.epic_username',
          'account_connection_details.epic_password',
          'prescription_detail.fillnumber'
        );
      if (accountConnections && accountConnections.length > 0) {
        for (let j = 0; j < accountConnections.length; j++) {
          if (accountConnections[j].account_connection_id === ConnectionType.EPIC) {
            const advanceFillStatusParams = {
              endPoint: accountConnections[j].endpoint,
              epicclientid: accountConnections[j].epic_client_id,
              userName: accountConnections[j].epic_username,
              passwd: accountConnections[j].epic_password,
              status: parseInt(epicStatus.ReadyToDispense),
              fillNumber: accountConnections[j].fillnumber,
              prescriptionId: accountConnections[j].rx_no,
              accountId: accountSetting.id
            };
            console.log(`Getting advance fill status params for prescription id ${advanceFillStatusParams.prescriptionId} is : ${JSON.stringify(advanceFillStatusParams, null, 2)}`);
            let advanceFillStatusResponse;
            try {
              advanceFillStatusResponse = await AdvanceFillStatus(advanceFillStatusParams);
              console.log(`Getting advance fill status Response for prescription id ${advanceFillStatusParams.prescriptionId} is : ${JSON.stringify(advanceFillStatusResponse, null, 2)}`);
            } catch (err) {
              console.log(`Error from EPIC server while executing advance fill status api for prescription id ${advanceFillStatusParams.prescriptionId} with error ${err}`);
            }
          }
        }
      }
      const customerPhone = customerDetails.contacts[0].phone ? customerDetails.contacts[0].phone : '';
      const alternetAgentsPhone = await AlternetAgents.query().where('customer_id', customerDetails.id).andWhere('isdefault', true);
      let agentSms = [];
      if (alternetAgentsPhone && alternetAgentsPhone.length > 0) {
        for (let i = 0; i < alternetAgentsPhone.length; i++) {
          agentSms.push(alternetAgentsPhone[i].agent_sms ? alternetAgentsPhone[i].agent_sms : '');
        }
      }
      let totalItemsAmount = 0;
      for (let itemPrice = 0; itemPrice < updatedPrescriptionMaster.items.length; itemPrice++) {
        totalItemsAmount = totalItemsAmount + updatedPrescriptionMaster.items[itemPrice].price;
      }
      if (accountSetting.address_line_1 !== null || accountSetting.address_line_1 !== 'null' || accountSetting.address_line_1 !== undefined || accountSetting.address_line_1 !== 'undefined' || accountSetting.address_line_1 !== '') {
        address_line1 = accountSetting.address_line_1
      }
      if (accountSetting.address_line_2 !== null || accountSetting.address_line_2 !== 'null' || accountSetting.address_line_2 !== undefined || accountSetting.address_line_2 !== 'undefined' || accountSetting.address_line_2 !== '') {
        address_line2 = accountSetting.address_line_2
      }
      if (accountSetting.city !== null || accountSetting.city !== 'null' || accountSetting.city !== undefined || accountSetting.city !== 'undefined' || accountSetting.city !== '') {
        city = accountSetting.city
      }
      if (accountSetting.state !== null || accountSetting.state !== 'null' || accountSetting.state !== undefined || accountSetting.state !== 'undefined' || accountSetting.state !== '') {
        state = accountSetting.state
      }
      if (accountSetting.zip !== null || accountSetting.zip !== 'null' || accountSetting.zip !== undefined || accountSetting.zip !== 'undefined' || accountSetting.zip !== '') {
        Zipcode = accountSetting.zip
      }
      if (customerDetails.sms_notification === true) {
        /* SMS Info starts */
        let data = req.body.email_data;
        const PhoneNumber = '+1' + ReplacePhone(customerPhone);
        const isPayentFacilityAllowed = await GbpConfig.query().where('parent_account_id', accountSetting.id).andWhere('allow_payment', true).first();
        if (isPayentFacilityAllowed && isPayentFacilityAllowed !== null) {
          const _paymentBaseUrl = process.env.PAYMENT_BASE_URL;
          const orderId = req.body.id;
          const _payNowLink = `${_paymentBaseUrl}?q=${orderId}`;
          const Message = `Hi ${data.firstName}, your order is ready for pick up at ${data.boxName} kiosk located \n at ${data.boxAddress} ${data.boxAddress2}.\nThe amount due for this order is $${totalItemsAmount}.\n If you would like to pay for it now, please click here ${_payNowLink}.\n Contactless pick up is just a few short clicks away.\n Bring this code ${updatedPrescriptionMaster.pucode} with you and \n you can type it in on the touch screen keyboard.\n Thank you for choosing ${data.pharmAddress.companyName}.`;
          sendAWSSMS(PhoneNumber, Message);
          for (let i = 0; i < agentSms.length; i++) {
            const agentPhoneNumber = '+1' + ReplacePhone(agentSms[i]);
            sendAWSSMS(agentPhoneNumber, Message);
          }
        } else {
          const Message = `Hi ${data.firstName}, your order is ready for pick up at ${data.boxName} kiosk located \n at ${data.boxAddress} ${data.boxAddress2}.\nThe amount due for this order is $${totalItemsAmount}.\n Bring this code ${updatedPrescriptionMaster.pucode} with you and \n you can type it in on the touch screen keyboard.\n Thank you for choosing ${data.pharmAddress.companyName}.`;
          sendAWSSMS(PhoneNumber, Message);
          for (let i = 0; i < agentSms.length; i++) {
            const agentPhoneNumber = '+1' + ReplacePhone(agentSms[i]);
            sendAWSSMS(agentPhoneNumber, Message);
          }
        }
      }
      res.send(
        JSON.stringify({
          prescriptions_master: prescriptionsMaster
        })
      );
    } 
    else if (req.body.type_id === RXSTATUS.PICKED_UP) {
      let data = req.body.email_data;
      let orders = req.body.email_data.orders;
      let totalAmount = 0;
      const _paymentBaseUrl = process.env.PAYMENT_BASE_URL;
      const _payNowLink = `${_paymentBaseUrl}?q=${data.orderId}`;

      // if(!req.body.email_data.orders[0].payments){
      for(let i=0; i<orders.length; i++){
        let items = orders[i].items;
        for(let j=0; j<items.length; j++){
          totalAmount += items[j].CoPayAmount;
        }
      }
    //  }
      let prescriptionsMaster, isAlreadyPickedUp, customerDetails;
      if (multiplePickup && multiplePickup.length > 0) {
        for (let j = 0; j < multiplePickup.length; j++) {
          // If Pickup type is 1 then this block will be executed. Pickup type 1 stands for normal pickup and 2 stands for pickup with patient login
          const isPickedUp = await prescriptions_master.query().where('id', multiplePickup[j].tofor_id).first(); // Check if pickup entry avalable in DB or not with type PICKUP
         
          if (isPickedUp && isPickedUp.type_id === RXSTATUS.PICKED_UP) {
            isAlreadyPickedUp = 1; // If Rx is already picked up then it will set to 1 for SMS sending.
            res.send({ res: 'Status for this order has already been updated.' });
          } else {
            isAlreadyPickedUp = 0;
            if (req.body.event_params !== undefined) {
              if (isAlreadyPickedUp === 0) {
                updateRxStatus.id = multiplePickup[j].tofor_id;
                prescriptionsMaster = await prescriptions_master.query()
                  .upsertGraphAndFetch(updateRxStatus,
                    {
                      relate: true,
                      unrelate: false
                    });
                const eventRes = await EventLogHandlerModule(multiplePickup[j], null);
              }
            }
            customerDetails = await customer.query().where('id', prescriptionsMaster.customer_id).andWhere('is_deleted', false).andWhere('is_enabled', true).eager("[contacts, address]").first();
            const customerPhone = customerDetails.contacts[0].phone ? customerDetails.contacts[0].phone : '';
            const alternetAgentsPhone = await AlternetAgents.query().where('customer_id', customerDetails.id).andWhere('isdefault', true);
            let agentSms = [];
            if (alternetAgentsPhone && alternetAgentsPhone.length > 0) {
              for (let i = 0; i < alternetAgentsPhone.length; i++) {
                agentSms.push(alternetAgentsPhone[i].agent_sms ? alternetAgentsPhone[i].agent_sms : '');
              }
            }
            if (isAlreadyPickedUp === 0) {
              if (customerDetails.sms_notification === true) {
                /* SMS Info starts */
                const PhoneNumber = '+1' + ReplacePhone(customerPhone);
                const Message = 'Your order has been Picked up \n' + 'Pickup Code: ' + isPickedUp.pucode +  '\n Thank you.'
                sendAWSSMS(PhoneNumber, Message);
                for (let i = 0; i < agentSms.length; i++) {
                  const agentPhoneNumber = '+1' + ReplacePhone(agentSms[i]);
                  sendAWSSMS(agentPhoneNumber, Message);
                }
              }
            }
          }
        }        
        let _emailData = req.body.email_data;
        _emailData.rx_expiration_date = rx_expiration_dateString;
        const alternetAgents = await AlternetAgents.query().where('customer_id', prescriptionsMaster.customer_id).andWhere('isdefault', true);
        let ccEmailAddresses = [];
        if (alternetAgents && alternetAgents.length > 0) {
          for (let i = 0; i < alternetAgents.length; i++) {
            ccEmailAddresses.push(alternetAgents[i].agent_email);
          }
        }
        _emailData.ccEmailAddresses = ccEmailAddresses;
        _emailData.email_notification = customerDetails.email_notification;
        if (_emailData.email_notification === true) {
          sendOperationsEmail(_emailData)
        }
      }
      res.send(
        JSON.stringify({
          prescriptions_master: prescriptionsMaster
        })
      );
    } else if (req.body.type_id === RXSTATUS.HOLD) {
      const prescriptionsMaster = await prescriptions_master.query()
        .upsertGraphAndFetch(updateRxStatus,
          {
            relate: true,
            unrelate: false
          });
      const updatedprescriptionMaster = await prescriptions_master.query()
        .skipUndefined()
        .where('is_deleted', false)
        .where('id', updatedprescriptionMaster.id)
        .eager("[items]").first();

      const customerDetails = await customer.query()
        .select('*')
        .where('id', updatedprescriptionMaster.customer_id)
        .skipUndefined()
        .andWhere('is_deleted', false)
        .andWhere('is_enabled', true)
        .eager("[contacts]")
      if (customerDetails.email_notification === true) {
        sendHoldNotificationEmail(req.body.email_data);
      }
      res.send(
        JSON.stringify({
          prescriptions_master: prescriptionsMaster
        })
      );
    } else {
      const prescriptionsMaster = await prescriptions_master.query().upsertGraphAndFetch(updateRxStatus, { relate: true, unrelate: false });
      if (req.body.event_params !== undefined) {
        let _emailData = req.body.email_data;
        _emailData.rx_expiration_date = rx_expiration_dateString;
        _emailData.payNowLink = '';
        const eventRes = await EventLogHandlerModule(req.body.event_params, _emailData); // Save Event logs and send an email for current pickup. This function will also send an email.
        console.log(JSON.stringify(eventRes));
      }
      res.send(
        JSON.stringify({
          prescriptions_master: prescriptionsMaster
        })
      );
    }
    
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});


//Get all entries for boxes by store -- Required for Selected beens while assigning prescription
// router.get("/getBinStatusByAccount/:id", async (req, res, next) => {
//   const presctiptions = await prescriptions_master.query().skipUndefined().where('parent_account_id', req.params.id);
//   res.send(presctiptions);
// });

//Update Box ID and Save schedule for Box
router.post("/saveBoxSchedule", async (req, res, next) => {
  const box_info = await box.query().findById(req.body.id);
  if (box_info!=null && box_info!=undefined){
    try
    {
      const box_update = await box.query().update({ kiosk_box_id: req.body.kiosk_box_id }).where('id', req.body.id);
      const box_schedule_info = {
        parent_account_id: req.body.id,
        selected_days:req.body.selected_days,
        open_hours:req.body.open_hours,
        closing_hours:req.body.closing_hours,
        is_deleted:false,
        is_enabled:true,
        created_on:new Date(),
        modified_on:null,
        deleted_on:null
      }
      const bsi = await box_schedule.query().insertGraphAndFetch(box_schedule_info);
      res.send(
        JSON.stringify({
          box_info : box_update,
          box_schedule_info : bsi
        })
      );
    }
    catch(error){
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
});

//Check if Rx No is available or not in DB
router.get("/checkrxexistornot/:id", async (req, res, next) => {
  let isExist = false;
  const presctiptions = await prescription_detail.query()
  .innerJoin("prescriptions_master", 'prescriptions_master.id', 'prescription_detail.parent_account_id')
  .skipUndefined()
  .where('rx_no', req.params.id)
  .where('prescriptions_master.is_deleted',false)
  .andWhere('prescriptions_master.type_id','!=',RXSTATUS.REMOVE_RX); //updated By Kishan | Bug : allowed duplicate Rx {date:29-07-2019 | modified by:naresh | desc:allowed same Rx after remove it}
  console.log(presctiptions);
  if(presctiptions.length > 0){
    isExist = true;
  }else{
    isExist = false;
  }
  res.send(
    JSON.stringify({
      Rx_Is_Exist : isExist
    })
  )

});

router.get("/boxes/:id", async (req, res, next) => {
  const presctiptions = await box.query().skipUndefined().where('id', req.params.id).eager("[address, type,hoursof_operations]").orderBy('name');
  res.send(presctiptions);
});

router.get("/account/:id", async (req, res, next) => {
  const accounts = await account.query().skipUndefined().where('id', req.params.id).eager("[type,contact,contact.address,connection,connection_details,hoursof_operations,legal_compliance]").orderBy('name');
  res.send(accounts);
});

router.get("/binsbybox/:id", async (req, res, next) => {
  const accounts = await prescriptions_master.query()
                        .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
                        .skipUndefined()
                        .select('prescriptions_master.bin_id','prescriptions_master.type_id', 'boxes.disabled_bins','prescriptions_master.id as pm_id')
                        .where('prescriptions_master.box_id', req.params.id)
                        .andWhere('prescriptions_master.is_enabled',true);
  res.send(accounts);
});

/*
  Added By : Belani Jaimin
  Added Date : 02/19/2019
  Description : Update disabled bins for maintenance
*/

router.put("/boxs", async (req, res, next) => {
  try{
    const boxes = await box.query()
    .update({disabled_bins: req.body.disabled_bins})
    .where('id', req.body.id);
    res.send(
      JSON.stringify({
        box : boxes
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

router.post("/rxbybin", async (req, res, next) => {
  const response = {}
  const theBox = await box.query().where('id', req.body.id).skipUndefined().first();
  const prescription_master = await prescriptions_master.query()
  .where('box_id',theBox.id)
  .andWhere('bin_id',req.body.bin_id)
  .andWhere('id',req.body.pm_id)
  .eager("[items, customer, customer.contacts , customerLocations ]");
  if(prescription_master.length !== 0){
    response.box = theBox;
    response.box.customers = prescription_master.map(x => x.customer);
    for(let j=0;j<response.box.customers.length;j++)
    {
      response.box.customers[j].orders = [];
      for(let i=0;i<prescription_master.length;i++)
      {
        if(prescription_master[i].customer_id === response.box.customers[j].id)
        {
          delete prescription_master[i].customer;
          response.box.customers[j].orders.push(prescription_master[i]);
        }
      }
    }
  }
  res.send(
    JSON.stringify({
      response : response
    })
  )
 });




//Check if Kiosk BoxID is exist or not in DB
router.post("/checkBoxIdExist", async (req, res, next) => {
  let isExist = false;
  const boxes = await box.query().skipUndefined().where('kiosk_box_id', '=' , req.body.kiosk_box_id);
  console.log(boxes);
  if(boxes.length > 0){
    isExist = true;
  }else{
    isExist = false;
  }
  res.send(
    JSON.stringify({
      BoxID_Is_Exist : isExist
    })
  )

});
//Get all entries for boxes by multiple store
router.post("/getBoxesByStoreIds", async (req, res, next) => {
  if (!req.headers.authorization) {
		next();
	  }
	  else {
		const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const boxes = await box.query().skipUndefined().where('parent_account_id','in',req.body.store_ids).orderBy('name');

    try{
      res.send(boxes
      );
    }catch(error){
      res.send(
        JSON.stringify({
          error,
          stack: error.stack
        })
      );
    }
  }

});
//  Get Transactions Report detail
router.post("/getTransactions", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
      
        const TransactionDetail = await events.query()
          .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
          .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
          .innerJoin('accounts','accounts.id', 'customers.parent_account_id')
          .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
          .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
          .innerJoin('event_type', 'event_type.id', 'events.type_id')
          .leftJoin('users','events.subject_id','users.id')
          .leftJoin('customer_collect_detail','customer_collect_detail.order_id','prescriptions_master.id')
          .leftJoin('locationtype_master', 'locationtype_master.id', 'customers.locationtype_id')
          // .where ('event_type.name','in',(EventType.ASSIGNED))
          .andWhere(
            function()
            {
              if(req.body.store_ids.length > 0)
              {
                this.where('accounts.id', 'in', req.body.store_ids)
              }
              else{
                this.where('accounts.id', 'in', accountIds.map(x => x.id))
              }
              if(req.body.box_ids.length > 0)
              {
                this.where('boxes.id', 'in', req.body.box_ids)
              }
              else
              {
                this.where('boxes.id', 'in',boxes.map(x=>x.id))
               }
                if(req.body.fromdate !== ""){
                  this.whereRaw('??::date >= ?', ['events.created_on', req.body.fromdate])
                }
                if(req.body.todate !== ""){
                  this.whereRaw('??::date <= ?', ['events.created_on', req.body.todate])
                }
            }
          )
          .andWhere(function () {
            if(req.body.searchtext !== "")
            {
              this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
              // this.orWhere('prescription_detail.fillnumber', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.prescriptionnumber', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('locationtype_master.name', 'like', '%' + req.body.searchtext + '%')
            }
          })
          .select(
             'customers.first_name as firstname'
            , 'customers.last_name as lastname'
            , 'accounts.name as store'
            , 'boxes.name as box'
            , 'prescriptions_master.bin_id as binid'
            , 'prescription_detail.rx_no as rxno'
            , 'prescription_detail.fillnumber as fillid'
            , 'prescription_detail.prescriptionnumber'
            ,'prescriptions_master.stock_code'
            ,'prescriptions_master.pucode'
            , 'prescription_detail.ndc_code as ndccode'
            , 'prescription_detail.drug_name as drugname'
            , 'prescription_detail.qty as qty'
            , 'prescription_detail.price as price'
            , 'prescriptions_master.fill_date as filldate'
            , 'prescriptions_master.created_on as createdon'
            , 'prescriptions_master.stocked_date as stockeddate'
            , 'prescriptions_master.pickup_date as pickupdate'
            , 'customers.signature_url as signatureurl'
            , 'event_type.name as status'
            ,'users.name AS username'
           , 'customer_collect_detail.first_name as cusUser_first_name'
           , 'customer_collect_detail.last_name as cusUser_last_name'
           ,'locationtype_master.name as deliverytype'
           ,'events.created_on as transactiondate'
          )
          .orderBy(
            'events.created_on','desc'
          );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
 
});


//get Boxes
router.post("/getBoxes", async (req, res, next) => {
  console.log('------------------------getBoxes Start 1---------------------------');
  console.log(req);
  console.log(req.body);
  console.log('------------------------getBoxes End---------------------------');
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    const accountIds = await account.query()
      .select('id')
      .where('account_lineage', '<@', u.parentAccount.account_lineage);

    const boxes = await box.query()
      .leftJoin("addresses", 'boxes.address_id', 'addresses.id')
      .leftJoin("box_type", 'boxes.type_id', 'box_type.id')
      .where('parent_account_id', 'in', accountIds.map(x => x.id))
      .andWhere('boxes.is_deleted','!=','true')
      // .andWhere('name','like','%'+ req.body.searchtext +'%')
      .andWhere(function () {
        this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
        this.orWhere('boxes.description', 'like', '%' + req.body.searchtext + '%')
        this.orWhere('box_type.name', 'like', '%' + req.body.searchtext + '%')
        this.orWhere('addresses.city', 'like', '%' + req.body.searchtext + '%')
      })
      // .orWhere('description','like','%'+ req.body.searchtext +'%')
      // .andWhere('type','like','%'+ req.body.searchtext +'%')
      .eager("[account, address, type, schedule,hoursof_operations]");
      for(let i=0; i<boxes.length; i++) {
        boxes[i].box_expansions = [];
        const be = await BoxExpansion.query().where('box_id', boxes[i].id).andWhere('is_deleted', false).andWhere('is_enabled', true);
        boxes[i].box_expansions = be;
      }
      // box_expansions
    try {
      res.send(boxes
        // JSON.stringify({
        //   boxes : boxes
        // })
      );
    } catch (error) {
      res.send(
        JSON.stringify({
          error,
          stack: error.stack
        })
      );
    }
  }
});

//  Get Stock List Report detail
router.post("/getStocklist", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
     
      const TransactionDetail = await events.query()
      .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
      .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
      .innerJoin('accounts', 'accounts.id','customers.parent_account_id')
      .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
      .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
      .innerJoin('users','events.subject_id','users.id')
      .innerJoin('event_type','event_type.id','events.type_id')
      .leftJoin('addresses','addresses.id','boxes.address_id')
      .leftJoin('customer_contacts','customer_contacts.customer_id','customers.id')
      .leftJoin('contacts','contacts.id','customer_contacts.contact_id')
      .leftJoin('box_type','box_type.id','boxes.type_id')
      .leftJoin('locationtype_master', 'locationtype_master.id', 'customers.locationtype_id')
      .where('prescriptions_master.type_id',RXSTATUS.STOCKED)
      .where ('event_type.name',EventType.STOCKED)
      .andWhere(
        function()
        {
          if(req.body.store_ids.length > 0)
          {
            this.where('accounts.id', 'in', req.body.store_ids)
          }
          else{
            this.where('accounts.id', 'in', accountIds.map(x => x.id))
          }
          if(req.body.box_ids.length > 0)
          {
            this.where('boxes.id', 'in', req.body.box_ids)
          }
          else
          {
            this.where('boxes.id', 'in',boxes.map(x=>x.id))
           }
            if(req.body.fromdate !== ""){
              this.whereRaw('??::date >= ?', ['prescriptions_master.stocked_date', req.body.fromdate])
            }
            if(req.body.todate !== ""){
              this.whereRaw('??::date <= ?', ['prescriptions_master.stocked_date', req.body.todate])
            }
        }
      )
      .andWhere(function () {
        if(req.body.searchtext !== "")
        {
          this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.bin_id', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
          // this.orWhere('prescription_detail.fillnumber', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.prescriptionnumber', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.stock_code', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.pucode', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('users.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('locationtype_master.name', 'like', '%' + req.body.searchtext + '%')
        }
      })
      .select(
        'accounts.id as accid'
        ,'accounts.name as store'
        ,'boxes.id as boxid'
        , 'boxes.name as box'
        ,'prescriptions_master.id as pmid'
        , 'prescriptions_master.bin_id as binid'
        , 'prescription_detail.rx_no as rxno'
        , 'prescription_detail.fillnumber as fillid'
        , 'prescription_detail.prescriptionnumber'
        , 'prescriptions_master.fill_date as filldate'
        , 'prescriptions_master.stocked_date as stockeddate'
        , 'prescriptions_master.stock_code'
        , 'prescriptions_master.pucode as pucode'
        ,'customers.first_name as firstname'
        , 'customers.last_name as lastname'
        , 'prescription_detail.ndc_code as ndccode'
        , 'prescription_detail.drug_name as drugname'
        , 'prescription_detail.qty as qty'
        , 'prescription_detail.price as price'
        ,'users.name as username'
        ,'users.id as userid'
        ,'addresses.address_line_1 as boxaddress_line_1'
        ,'addresses.address_line_2 as boxaddress_line_2'
        ,'addresses.city as boxcity'
        ,'addresses.state as boxstate' 
        ,'addresses.zip as boxzip' 
        ,'boxes.kiosk_box_id'
        ,'customers.dob'
        ,'customers.dob_str'
        ,'box_type.name as box_type'
        ,'contacts.email'
        ,'contacts.phone'
        ,'prescriptions_master.customer_id'
        ,'accounts.picture_url'
        ,'locationtype_master.name as deliverytype'
      )
      .orderBy(
        'prescriptions_master.stocked_date','desc'
      );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
 
});

//  Get Pick Up detail
router.post("/getPickup", async (req, res, next) => {
try
{
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
    const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
    const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
    const TransactionDetail = await events.query()
        .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')  
        .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
        .innerJoin('accounts',  'accounts.id','customers.parent_account_id')
        .innerJoin("boxes", 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin('addresses', 'addresses.id', 'boxes.address_id')
        .innerJoin("prescription_detail", 'prescription_detail.parent_account_id', 'prescriptions_master.id')
        .leftJoin('customer_collect_detail','customer_collect_detail.order_id','prescriptions_master.id')
        .innerJoin('event_type', 'event_type.id', 'events.type_id')
        .leftJoin('locationtype_master', 'locationtype_master.id', 'customers.locationtype_id')
        .where('prescriptions_master.type_id',RXSTATUS.PICKED_UP)
        .where('event_type.name','in', [EventType.PICKED_UP,EventType.MANUAL_OVERRIDE])
        // .where('events.subject_id',u.id)
        .andWhere(
          function()
          {
            if(req.body.store_ids.length > 0)
            {
              this.where('accounts.id', 'in', req.body.store_ids)
            }
            else{
              this.where('accounts.id', 'in', accountIds.map(x => x.id))
            }
            if(req.body.box_ids.length > 0)
            {
              this.where('boxes.id', 'in', req.body.box_ids)
            }
            else
            {
              this.where('boxes.id', 'in',boxes.map(x=>x.id))
             }
              if(req.body.fromdate !== ""){
                this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', req.body.fromdate])
              }
              if(req.body.todate !== ""){
                this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', req.body.todate])
              }
          }
        )
        .andWhere(function () {
          if(req.body.searchtext !== "")
          {
            this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
            // this.orWhere('prescription_detail.fillnumber', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.prescriptionnumber', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('locationtype_master.name', 'like', '%' + req.body.searchtext + '%')
          }
        })
        .select('accounts.id', 'customers.first_name as firstname', 'customers.last_name as lastname', 'accounts.name as store', 'boxes.id as boxid', 'boxes.name as box'
          , 'prescriptions_master.bin_id as binid', 'customers.picture_url as pictureurl'
          , 'prescription_detail.rx_no as rxno', 'prescription_detail.fillnumber as fillid', 'prescription_detail.prescriptionnumber','prescription_detail.ndc_code as ndccode', 'prescription_detail.drug_name as drugname', 'prescription_detail.qty as qty' , 'prescription_detail.price as price'
          , 'prescriptions_master.fill_date as filldate', 'prescriptions_master.stocked_date as stockeddate', 'prescriptions_master.pickup_date as pickupdate'
          , 'customers.signature_url as signatureurl'
          ,'prescriptions_master.stock_code'
          ,'prescriptions_master.pucode'
          ,'customers.name as username'
          ,'customer_collect_detail.order_id as pmid'
          ,'customer_collect_detail.first_name as pfirst_name'
          ,'customer_collect_detail.last_name as plast_name'
          ,'customer_collect_detail.drivers_license_no as pdrivers_license_no'
          ,'customer_collect_detail.dob as pdob'
          ,'customer_collect_detail.relationship as prelationship'
          ,'customer_collect_detail.picture_url as ppicture_url'
          ,'customer_collect_detail.signature_url as psignature_url'
          ,'customer_collect_detail.relationship as relationship'
          /*
            Modified By    : Belani Jaimin
            Modified Date  : 02/10/2020
            Description    : Record of declined consultations. Each pick up where the patient says "I'm Good" is a decline
          */
          ,'customer_collect_detail.is_consult_requested as isConsultRequested'
          ,'prescriptions_master.iscontactlessactivated'
          ,'locationtype_master.name as deliverytype'
          ,'addresses.address_line_1' 
          ,'addresses.address_line_2'
          ,'addresses.city'
          ,'addresses.state'
          ,'addresses.zip'

        )
        .orderBy(
          'prescriptions_master.pickup_date','desc'
        );
      res.send(TransactionDetail);
  }
}
catch(error){
  res.send(
    JSON.stringify({
      message: error.message,
      stack: error.stack
    })
  );
} 
 
});

//  Get History Report detail
router.post("/getHistory", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    if (req.body.store_ids.length > 0 && req.body.box_ids.length > 0) {
      const TransactionDetail = await customer.query()
        .innerJoin("events", 'events.tofor_id', 'customers.id')
        .innerJoin("accounts", 'accounts.id', 'events.parent_account_id')
        .innerJoin("prescriptions_master", 'prescriptions_master.customer_id', 'events.tofor_id')
        .innerJoin("boxes", 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin("prescription_detail", 'prescription_detail.parent_account_id', 'prescriptions_master.id')
        .innerJoin("event_type", 'event_type.id', 'events.type_id')
        .where('events.parent_account_id', 'in', req.body.store_ids)
        .where('boxes.id', 'in', req.body.box_ids)
        .where('events.subject_id', u.id)
        .whereRaw('??::date >= ?', ['events.created_on', req.body.fromdate])
        .whereRaw('??::date <= ?', ['events.created_on', req.body.todate])
        .andWhere('customers.is_deleted', false).andWhere('customers.is_enabled', true)
        .andWhere(function () {
          this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
        })
        .select('customers.first_name as first_name'
          , 'customers.last_name as last_name'
          , 'accounts.id as store_id'
          , 'accounts.name as store_name'
          , 'boxes.id as box_id'
          , 'boxes.name as box_name'
          , 'prescription_detail.rx_no as rx_no'
          , 'prescriptions_master.stocked_date as stocked_date'
          , 'prescriptions_master.pickup_date as pickup_date'
          , 'event_type.name as status'
          , 'events.description'
        );
      res.send(TransactionDetail);
    }
    else if (req.body.store_ids.length > 0) {
      const TransactionDetail = await customer.query()
        .innerJoin("events", 'events.tofor_id', 'customers.id')
        .innerJoin("accounts", 'accounts.id', 'events.parent_account_id')
        .innerJoin("prescriptions_master", 'prescriptions_master.customer_id', 'events.tofor_id')
        .innerJoin("boxes", 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin("prescription_detail", 'prescription_detail.parent_account_id', 'prescriptions_master.id')
        .innerJoin("event_type", 'event_type.id', 'events.type_id')
        .where('events.parent_account_id', 'in', req.body.store_ids)
        .whereRaw('??::date >= ?', ['events.created_on', req.body.fromdate])
        .whereRaw('??::date <= ?', ['events.created_on', req.body.todate])
        .where('events.subject_id', u.id)
        .andWhere('customers.is_deleted', false).andWhere('customers.is_enabled', true)
        .andWhere(function () {
          this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
        })
        .select('customers.first_name as first_name'
          , 'customers.last_name as last_name'
          , 'accounts.id as store_id'
          , 'accounts.name as store_name'
          , 'boxes.id as box_id'
          , 'boxes.name as box_name'
          , 'prescription_detail.rx_no as rx_no'
          , 'prescriptions_master.stocked_date as stocked_date'
          , 'prescriptions_master.pickup_date as pickup_date'
          , 'event_type.name as status'
          , 'events.description'
        );
      res.send(TransactionDetail);
    }
    else {
      const TransactionDetail = [];
      res.send(TransactionDetail);
    }
  }
});


// //Check if Kiosk BoxID is exist or not in DB
// router.post("/checkBoxIdExist", async (req, res, next) => {
//   let isExist = false;
//   const boxes = await box.query().skipUndefined().where('kiosk_box_id', req.body.kiosk_box_id).andWhere('id', '!=', req.body.id);
//   console.log(boxes);
//   if (boxes.length > 0) {
//     isExist = true;
//   } else {
//     isExist = false;
//   }
//   res.send(
//     JSON.stringify({
//       BoxID_Is_Exist: isExist
//     })
//   )

// });

//  Get Assigned items Report detail
router.post("/getAssigneditems", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
      
        const TransactionDetail = await events.query()
        .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
        .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
        .innerJoin('accounts',  'accounts.id','events.parent_account_id')
        .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'events.tofor_id')
        .innerJoin('users','users.id','events.subject_id')
        .innerJoin('event_type','event_type.id','events.type_id')
        .innerJoin('locationtype_master', 'locationtype_master.id', 'customers.locationtype_id')
        //.where('events.parent_account_id',u.parent_account_id)
        .where('prescriptions_master.type_id',RXSTATUS.ASSIGNED)
        .where('event_type.name',EventType.ASSIGNED)
        .andWhere(
          function()
          {
            if(req.body.store_ids.length > 0)
            {
              this.where('accounts.id', 'in', req.body.store_ids)
            }
            else{
              this.where('accounts.id', 'in', accountIds.map(x => x.id))
            }
            if(req.body.box_ids.length > 0)
            {
              this.where('boxes.id', 'in', req.body.box_ids)
            }
            else
            {
              this.where('boxes.id', 'in',boxes.map(x=>x.id))
             }
              if(req.body.fromdate !== ""){
                this.whereRaw('??::date >= ?', ['prescriptions_master.created_on', req.body.fromdate])
              }
              if(req.body.todate !== ""){
                this.whereRaw('??::date <= ?', ['prescriptions_master.created_on', req.body.todate])
              }
          }
        )
        .andWhere(function () {
          if(req.body.searchtext !== "")
          {
            this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
            // this.orWhere('prescription_detail.fillnumber', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.prescriptionnumber', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescriptions_master.stock_code', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('prescriptions_master.pucode', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('users.name', 'like', '%' + req.body.searchtext + '%')
            this.orWhere('locationtype_master.name', 'like', '%' + req.body.searchtext + '%')
          }
        })
        .select(
           'customers.first_name as firstname'
          , 'customers.last_name as lastname'
          , 'accounts.name as store'
          , 'boxes.name as box'
          , 'prescriptions_master.bin_id as binid'
          , 'prescription_detail.rx_no as rxno'
          , 'prescription_detail.fillnumber as fillid'
          , 'prescription_detail.prescriptionnumber'
          , 'prescription_detail.ndc_code as ndccode'
          , 'prescription_detail.drug_name as drugname'
          , 'prescription_detail.qty as qty'
          , 'prescription_detail.price as price'
          , 'prescriptions_master.created_on as createdon'
          ,'prescriptions_master.stock_code'
          ,'prescriptions_master.pucode'
          ,'users.name as username'
          ,'locationtype_master.name as deliverytype'
        )
        .orderBy(
          'prescriptions_master.created_on','desc'
        );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
  
});
// Save Enroll Customer from Kiosk side entry in customer table
router.post("/savekioskcustomer", async (req, res, next) => { 
  let customer_type_id=''; 
  
  const cus_type = await customer_type.query().where('name', req.body.customer_type);  
        
  const cus_type_params = {
      name: req.body.customer_type,
  };
  if(cus_type.length !== 0) {    
    customer_type_id = cus_type[0].id;
  }
  else
  {            
      try{                                
          const cus_type = await customer_type.query().insertGraphAndFetch(cus_type_params);                
          customer_type_id = cus_type.id;
      }   
      catch(error){
          res.send(
              JSON.stringify({
                  message: error.message,
                  stack: error.stack
              })
          );
      }
  }
  if(customer_type_id !== "")
  {
    const savekioskcustomerobj = {   
      name: `${req.body.first_name} ${req.body.last_name}`,
      drivers_license_no:req.body.drivers_license_no,
      first_name:req.body.first_name,
      middle_name:req.body.middle_name,
      last_name:req.body.last_name,
      //dob:req.body.dob,
      dob_str:req.body.dob,
      username:req.body.email,
      type_id:customer_type_id,
      parent_account_id:req.body.parent_account_id,
      box_id:req.body.box_id,
      password_reset_on: new Date(),
      contacts: [
        {
          name: "Kiosk",
          phone: req.body.phone,
          email: req.body.email
        }
      ]
    };
    try{
      let agreement;
      let customerDetail = null;
      const accountId = await account.query()
      .innerJoin('account_connections','account_connections.id','accounts.account_connections_id')
      .where('accounts.id', req.body.parent_account_id)
      .whereNotIn('account_connections.id',[ConnectionType.ILOCALBOX_PHARMACY])
      .select('accounts.id');
      const emailParam={
        email : req.body.email,
        UserType : congnitoUserType.PATIENT
      }
      let emailId_isExist = false;
      emailId_isExist = await checkEmailExist(emailParam);
      
      let boxDetail;
      if(req.body.box_id !== "undefined" && req.body.box_id !== undefined && req.body.box_id !== "null"){
         boxDetail = await box.query().where('id',req.body.box_id).eager("[address]").first();
      }
	   if(accountId.length > 0 && emailId_isExist === false)
      {
            const userParams={
              username : savekioskcustomerobj.username,
              name : savekioskcustomerobj.name,
              email : req.body.email,
              phone : req.body.phone,
              parent_account_id : savekioskcustomerobj.parent_account_id,
              patientName : savekioskcustomerobj.name,
              first_name : req.body.first_name,
              boxName : boxDetail && boxDetail.name,
              boxAddress : boxDetail && boxDetail.address ?
      (boxDetail.address.address_line_1 + ' '
        + boxDetail.address.address_line_2 + ','
        + boxDetail.address.city + ', '
        + boxDetail.address.state + ' '
        + boxDetail.address.zip + '') : '',

            }
            const userRes = await createAWSCognitoUser(userParams);
            console.log(JSON.stringify(userRes));
       }
       if(emailId_isExist === false)
       {
         customerDetail = await customer.query().insertGraphAndFetch(savekioskcustomerobj); 
         
         let signature = req.body.agreementobj.signature_url !== null ? new Buffer(req.body.agreementobj.signature_url.replace(/^data:image\/\w+;base64,/, ""),'base64'):null;
         let s3signatureLocation= null;

       if(signature!==null && signature!=='null' && signature!==undefined && signature!=='undefined' && signature!==''){
        let s3response = {};
        let params = {
          Bucket: process.env.IMAGES_BUCKET,
          Key: uuidv4(),
          Body: signature,
          ContentEncoding: 'base64',
          ContentType: 'image/jpeg',
          ACL: "public-read"
        };
        s3response = await s3.upload(params).promise();
        s3signatureLocation= s3response.Location;
      }

       const customerlegaldocsobj = {
        parent_account_id : req.body.parent_account_id,
        customer_id : customerDetail.id,
        is_docone_policy_accepted : req.body.agreementobj.is_docone_accepted,
        docone_policy_label : req.body.agreementobj.docone_label,
        docone_policy_accepted_hash : req.body.agreementobj.docone_hash,
        is_doctwo_policy_accepted: req.body.agreementobj.is_doctwo_accepted,
        doctwo_policy_label : req.body.agreementobj.doctwo_label,
        doctwo_policy_accepted_hash : req.body.agreementobj.doctwo_hash,
        is_docthree_policy_accepted : req.body.agreementobj.is_docthree_accepted,
        docthree_policy_label : req.body.agreementobj.docthree_label,
        docthree_policy_accepted_hash : req.body.agreementobj.docthree_hash,
        signature_url :  s3signatureLocation
      };        
       agreement = await CustomerLegalDocs.query().insertGraphAndFetch(customerlegaldocsobj);
     } 
      res.send(
        JSON.stringify({  
          emailId_isExist: emailId_isExist,      
          customer:customerDetail,agreement
        })
      );
    }catch(error){
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    } 
  }
});

//Update Customer's Legeal Agreement
router.post("/UpdatekioskcustomerDocs", async (req,res,next) => {
    try{
      let customer_agreement;
      let signature = req.body.signature_url !== null ? new Buffer(req.body.signature_url.replace(/^data:image\/\w+;base64,/, ""),'base64'):null;
      let s3signatureLocation= null;
  
         if(signature!==null && signature!=='null' && signature!==undefined && signature!=='undefined' && signature!==''){
          let s3response = {};
          let params = {
            Bucket: process.env.IMAGES_BUCKET,
            Key: uuidv4(),
            Body: signature,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg',
            ACL: "public-read"
          };
          s3response = await s3.upload(params).promise();
          s3signatureLocation= s3response.Location;
        }
  
        const custmer_legaldocs_ac =  await CustomerLegalDocs.query().where('customer_id',req.body.customer_id)
        console.log("custmer_legaldocs_ac ==>",custmer_legaldocs_ac);
        if(custmer_legaldocs_ac && custmer_legaldocs_ac.length > 0){
          //const customerlegaldocsdelete = await CustomerLegalDocs.query().deleteById(custmer_legaldocs_ac[0].id);
              
        const customerlegaldocsObj = {
          id: custmer_legaldocs_ac[0].id,
          parent_account_id : req.body.parent_account_id,
          //box_id : req.body.box_id,
          customer_id : req.body.customer_id,
          is_docone_policy_accepted : req.body.is_docone_accepted,
          docone_policy_label : req.body.docone_label,
          docone_policy_accepted_hash : req.body.docone_hash,
          is_doctwo_policy_accepted: req.body.is_doctwo_accepted,
          doctwo_policy_label : req.body.doctwo_label,
          doctwo_policy_accepted_hash : req.body.doctwo_hash,
          is_docthree_policy_accepted : req.body.is_docthree_accepted,
          docthree_policy_label : req.body.docthree_label,
          docthree_policy_accepted_hash : req.body.docthree_hash,
          signature_url :  s3signatureLocation,
          is_active: req.body.is_active
        }
  
        console.log("customerlegaldocsObj===>",customerlegaldocsObj);
        customer_agreement = await CustomerLegalDocs.query().upsertGraphAndFetch(customerlegaldocsObj);
      }
        res.send(
          JSON.stringify({        
            customer_agreement
          })
        );
      }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
    }
  });
// Get Enroll Customer from Kiosk
router.post("/getkioskcustomer", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
     const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
   const boxes = await box.query().select('parent_account_id').where('parent_account_id', 'in', accountIds.map(x => x.id))
   let getkioskcustomerDetail=[];
    getkioskcustomerDetail = await customer.query()
        .innerJoin("customer_contacts","customer_contacts.customer_id","customers.id")
        .innerJoin("contacts","customer_contacts.contact_id","contacts.id")
       .innerJoin("customer_type", 'customer_type.id', 'customers.type_id')
        .skipUndefined().where('customers.parent_account_id', 'in' ,boxes.map(x=>x.parent_account_id))
        .andWhere('customers.is_deleted', false).andWhere('customers.is_enabled', true)
        .andWhere(function () {
          this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.drivers_license_no', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('contacts.email', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('contacts.phone', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customer_type.name', 'like', '%' + req.body.searchtext + '%')
        })
        .select(
          "customers.first_name"
          ,"customers.last_name"
          ,"customers.drivers_license_no"
          ,customer.raw("to_char(customers.dob, 'MM/DD/YYYY') as dob")
          // ,"customers.dob"
          ,"contacts.email"
          ,"contacts.phone"
          ,"customer_type.name"
          ,"customers.created_on as enroll_date"
        )
        .orderBy("customers.created_on","desc");
      res.send(getkioskcustomerDetail); 
    }
});


//Update status from Kiosk side   Rx Stock Remove
router.post("/updateStatusOfRemoveRX", async (req, res, next) => {
  try{
    const RxRemove = await prescriptions_master.query()
    .update({is_deleted:req.body.is_deleted,type_id:req.body.type_id})
    .where('id',req.body.id);
    let customerDetails = await customer.query().where('id', order.customer_id).andWhere('is_deleted', false).andWhere('is_enabled', true).first();
    let event_type_id='';
    const evt_type = await event_type.query().where('name', req.body.event_params.event_type_name);  
    if(evt_type.length !== 0) {    
      event_type_id = evt_type[0].id;
    }
    if(event_type_id != '')
    {
      const eventCount =await events.query().
      where('tofor_id',req.body.event_params.tofor_id).
      andWhere('type_id',event_type_id).
      andWhere('object_id',req.body.event_params.object_id);
      
      console.log(`Event Count ${JSON.stringify(eventCount)}`);
      let events_res=[];
      if(eventCount.length === 0)
      {
          if(req.body.event_params.event_type_name ===  EventType.REMOVE_RX)
          {
              console.log('condition satisfied')
              const eventRes =  await EventLogHandlerModule(req.body.event_params, null);
              const theBox = await box.query().where('id', req.body.event_params.box_id).skipUndefined().eager("[address, type, account.contact.address, schedule, account.brand]").first();
              if(customerDetails.email_notification === true) {
                await sendRemoveRxNotification(theBox,req.body.event_params.box_id,req.body.id)
              }
          }
      }    
    }
    res.send(
      JSON.stringify({
        prescriptions_master : RxRemove
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Update Status of User Disable
router.post("/userdis", async (req, res, next) => {
  try {
    const test = await user.query()
      .update({ is_enabled: req.body.is_enabled })
      .where('id', req.body.id);

    const cognitoParam = {
      Username: req.body.id,
      UserPoolId: process.env.USER_POOL_ID
    };
    if(req.body.is_enabled === true) {
      await cisp.adminEnableUser(cognitoParam).promise();
    } else {
      await cisp.adminDisableUser(cognitoParam).promise();
    }    
    res.send(
      JSON.stringify({
        user: test
      })
    );
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Get box list for dashboard
router.post("/BoxList", async (req, res, next) => {  
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      let pageNo = req.body.pageNo;
      let pageSize = req.body.pageSize;

      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxesResult = await box.query().where('parent_account_id', 'in', accountIds.map(x=>x.id)).andWhere('is_deleted','!=','true').eager("[account,address, type, schedule]").orderBy('last_access_date','asc').page(pageNo-1,pageSize);
      const boxes = boxesResult.results;
      for(let i=0;i<boxes.length;i++){
        boxes[i].sum = 0;
        boxes[i].totalRecords = boxesResult.total;
        boxes[i].status = null;
        boxes[i].available_Bins=null;
        const prescription_master = await prescriptions_master.query().where('box_id',boxes[i].id).skipUndefined().where('is_deleted',false).eager("[items]");
        let sys_date = new Date();
        let actualcreateddate = '';
        boxes[i].lastCommunicationDate = null;
        if(boxes[i].last_access_date!==null && boxes[i].last_access_date!==undefined && boxes[i].last_access_date!=='null' && boxes[i].last_access_date!=='' && boxes[i].last_access_date!=='undefined'){
          boxes[i].lastCommunicationDate = boxes[i].last_access_date;
          actualcreateddate = new Date(boxes[i].last_access_date);
          let parseSysDate = new Date(sys_date)
          let parseActualDate = new Date(actualcreateddate);
          console.log(`sys_date: ${sys_date} and paresys_date :${parseSysDate} || last:${boxes[i].last_access_date} and actualcreateddate:${actualcreateddate} and parseActualDate :${parseActualDate}`);
          
          let time_diff = parseSysDate.getTime() - parseActualDate.getTime();
          let minutes = Math.ceil(time_diff / (1000 * 60));
          console.log("Minutes : => " + boxes[i].status);
          if(minutes > 5){      
            boxes[i].status = 0; //When status is Offline
            boxes[i].index = 1;
            console.log("Offline Status : ==> " + boxes[i].status);
          }else if(minutes < 2){
            boxes[i].index = 2;
            boxes[i].status = 1; //When status is Active
            console.log("Online Status : ==> " + boxes[i].status);
          }else if(minutes > 2 &&  minutes < 5){
            boxes[i].index = 3;
            boxes[i].status = 2; //When status is Pending
            console.log("Pending Status : ==> " + boxes[i].status);
          }
        }else{
          boxes[i].status = 3; //When status is New
          boxes[i].index = 4;
          console.log("New Status : ==> " + boxes[i].status);
        }
        boxes[i].available_Bins = await getAvailableBins(boxes[i],prescription_master); //adde by naresh        
        // if(boxes[i].IsActive === true){
          // await sendBoxNotification(boxes[i],boxes[i].parent_account_id,boxes[i].status);
        // }        
      }
      console.log("Total Boxes Available :===> " + boxes.length);
      res.send(boxes);
    }
  }
  catch(error){
    res.send(error.message);
  }
});
//added by Naresh
async function getAvailableBins(box,prescriptionDetail)
{
  let availableBins = 0;
  let use_Bins = 0;
    prescriptionDetail && prescriptionDetail.forEach(element => {
       if(element.type_id === RXSTATUS.ASSIGNED || element.type_id === RXSTATUS.STOCKED || element.type_id === RXSTATUS.HOLD){
        use_Bins = use_Bins + 1;
       }
  }); 
  availableBins += getBinsCountByType(box.type_id);
  const box_expansions = await getBoxExpansions(box.id);
  if(box_expansions && typeof(box_expansions) !== 'string' && box_expansions.length > 0)
  {
    box_expansions.forEach(element => {
      availableBins += getBinsCountByType(element.expansion_id);
    });
  }
  if(use_Bins > 0)
  {
    availableBins = availableBins - use_Bins;
  }
  return availableBins
}
//added by Naresh
function getBinsCountByType(type_id)
{
  let doorCount = 0;
  switch (type_id) {
    case BoxTypeEnum.MAIN_46:
      doorCount = 46;
      break;
    case BoxTypeEnum.L_60:
      doorCount = 60;
      break;
    case BoxTypeEnum.COOL_8:
      doorCount = 8;
      break;
    case BoxTypeEnum.HD_36:
      doorCount = 36;
      break;
    default:
      doorCount = 46;
  }
  return doorCount;
}
//Save upload Image Of Customer
router.post("/uploadImageOfCustomer", upload.any() ,async (req, res, next) => {
  let picture = new Buffer(req.body.picture_url.replace(/^data:image\/\w+;base64,/, ""),'base64');
  let signature = new Buffer(req.body.signature_url.replace(/^data:image\/\w+;base64,/, ""),'base64');
  let s3pictureLocation=null;
  let s3signatureLocation=null;
  if(picture!==null && picture!=='null' && picture!==undefined && picture!=='undefined' && picture!==''){
    let s3response = {};
    let params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(),
      Body: picture,
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg',
      ACL: "public-read"
    };
    s3response = await s3.upload(params).promise();
    s3pictureLocation= s3response.Location;
  }
 if(signature!==null && signature!=='null' && signature!==undefined && signature!=='undefined' && signature!==''){
    let s3response = {};
    let params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(),
      Body: signature,
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg',
      ACL: "public-read"
    };
    s3response = await s3.upload(params).promise();
    s3signatureLocation= s3response.Location;
  }
  const uploadimgCustomer = {
    id:req.body.id,
    picture_url:s3pictureLocation,
    signature_url: s3signatureLocation
  }
  try{
    const usor = await customer.query()
    .update({picture_url:uploadimgCustomer.picture_url,
      signature_url:uploadimgCustomer.signature_url})
    .where('id', uploadimgCustomer.id);
    res.send(
      JSON.stringify({
        customers : usor
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

/*
Created By : Dhaval Pandya
Added Date : 27/2/2019
Desc : Get events by parrentsaccount 
*/

router.get("/getevents/:id", async (req, res, next) => {

  const evt_type = await event_type.query().where('name','ASSIGNED').first(); 
  const boxevents = await events.query().skipUndefined().where('box_id', req.params.id)
  .where('type_id','!=',evt_type.id).orderBy("created_on");
  res.send(boxevents);
});

/*
Created By : Dhaval Pandya
Added Date : 12/03/2019
Desc : Get events for box  offline notification based on users loged in .
*/

router.get("/geteventsNotification/:id", async (req, res, next) => {
  console.log(req.params.id)
  //const boxes = await box.query().skipUndefined().select('id').where('parent_account_id',req.params.id);
  const boxes = await box.query().skipUndefined().where('parent_account_id',req.params.id);
  let boxesOffline = [];

  for(let i=0 ; i < boxes.length ; i++)
  {
     let boxdetail = boxes[i];
     const boxevent = await events.query().skipUndefined().where('box_id',boxes[i].id);
     console.log(boxevent);
     if(boxevent !== null && boxevent.length > 0 )
     {
      if(boxevent[boxevent.length - 1].created_on !== null && boxevent[boxevent.length - 1].created_on !== undefined)
      {
        let sys_date = new Date().toUTCString();
        let actualcreateddate = new Date(boxevent[boxevent.length - 1].created_on).toUTCString();
        let time_diff = Math.abs(sys_date.getTime() - actualcreateddate.getTime()); 
        let minutes = Math.ceil(time_diff / (1000 * 60));

        if(boxes[i].is_enabled === false)
        {
           boxdetail.boxstatus = 'Inactive'
        }
        else
        {
        if(minutes >= 1 && minutes < 60)
        {
          boxdetail.boxstatus = "Online";
        }
        else if(minutes >= 60 && minutes < (24 * 60))
        {
          boxdetail.boxstatus = "Unknown";
        }
        else if(minutes > (24 * 60))
        {
          boxdetail.boxstatus = "Offline";
        }
        }
        
      }
    }
    else
    {
      boxdetail.boxstatus = "New";
    }

    boxesOffline.push(boxdetail);
  }
  console.log(boxesOffline);
     //boxev = boxevent;
     res.send(boxesOffline);
  });

/*
Created By : Dhaval Pandya
Added Date : 15/03/2019
Desc : Update Box Isactive flag 
*/
router.post("/updateBoxIsActive", async (req, res, next) => {
  try{    
    const boxes = await box.query()
    .update({IsActive: req.body.IsActive})
    .where('kiosk_box_id', req.body.kiosk_box_id);
    console.log(boxes);
    res.send(
      JSON.stringify({
        box : boxes
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }    
});

//Get All Account connections | Added by Naresh | 18-03-2019
router.get("/getAccountconnections", async (req, res, next) => {  
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[roles]").first();
      
      const AccountConnections = await AccountConnection.query().select('id','name');
      res.send(AccountConnections);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//Get flag value from account | Added by Naresh | 18-03-2019
router.get("/getConnection", async (req, res, next) => {
 try
 {
  const token = req.headers.authorization.split(' ')[1];
  const decodedToken = jwt.decode(token);
  const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
  const accountId = u.parent_account_id;
    const accountConnectionsId = await account.query()
    .innerJoin('account_connections','account_connections.id','accounts.account_connections_id')
    .where('accounts.id', accountId)
    .where('account_connections.id' , 'in' , [ConnectionType.UNPLUGGED,ConnectionType.PIONEERRX,ConnectionType.QS1, ConnectionType.EPIC, ConnectionType.MEDBANK ])
    .select('accounts.id', 'account_connections_id','is_payment_required').first();

    res.send(accountConnectionsId);
 }
 catch(error){
  res.send(
    JSON.stringify({
      message: error.message,
      stack: error.stack
    })
  );
}
});
//Get All Customers for unpluged version  | Added by Naresh | 18-03-2019
router.get("/Getcustomers", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    console.log('decodedToken:-'+JSON.stringify(decodedToken));
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
    // const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
    const customers = await customer.query()
     .where('parent_account_id',u.parent_account_id).andWhere('is_deleted', false).andWhere('is_enabled', true).eager("[roles, contacts, legaldocs, customerType,customerLocations,address,alternetAgents]");

    res.send(customers);
  }
});
/*
  Added By : Jaydutt Shukla
  Added Date: 11/03/2019
  Last Updated: 18/04/2019 
  Description : Get Details for email for Prescription status : Hold/Unhold ;
*/
router.post("/emailHoldStatus", async (req, res, next) => {
  try{

    if (!req.headers.authorization) {
      next();
    }
    else
    {
    const theBox = await box.query().where('id', req.body.box_id).skipUndefined().eager("[address, type, account.contact.address, schedule, account.brand]").first();  
    await sendHoldNotification(theBox,req.body.box_id,req.body.pucode,req.body.onHold)
    // res.send(JSON.stringify(config));
    res.send('Email Sent')  
    }
  }
  catch(error)
  {
    res.send(error);
  }
});
async function sendHoldNotificationEmail(configuration){
  try{
    const theBox = await box.query().where('id', configuration.box_id).skipUndefined().eager("[address, type, account.contact.address, schedule, account.brand]").first();  
    await sendHoldNotification(theBox,configuration.box_id,configuration.pucode,configuration.onHold)
    return;
  } 
  catch(error)
  {
    return error;
  }
}
function ReplacePhone(phone) {
  let newphone='';
  if(phone.length > 0)
  {
    newphone = phone.replace('(','').replace(')','').replace(' ','').replace('-','');
  }
  return newphone;
}
//Get role permissions from account | Added by varshit   | 27-03-2019
router.get("/getPermissions/", async (req, res, next) => {
  try
  {
   const token = req.headers.authorization.split(' ')[1];
   const decodedToken = jwt.decode(token);
   const u = await user.query().where('username', decodedToken.email).eager("[parentAccount,roles]").first();
 
     const Permissons = await account.query()
     .innerJoin('role_permission_details','role_permission_details.account_id','accounts.id')
     .innerJoin('permissions','permissions.id','role_permission_details.permission_id')     
     .where('accounts.id', u.parent_account_id)
     .where('permissions.type_id','!=','e15e25c6-98ab-45bd-8e39-edab66f02604')
     .select('role_permission_details.id','role_permission_details.role_id','role_permission_details.permission_id','permissions.name as permission_name','role_permission_details.permission_entity_type_id','role_permission_details.is_allowed')
     .orderBy('permissions.order_id','asc');
     res.send(JSON.stringify({
      user: u,
      permission: Permissons
    }))
    // res.send(Permissons);   
  }
  catch(error){
   res.send(
     JSON.stringify({
       message: error.message,
       stack: error.stack
     })
   );
 }
 });

// Get user
router.post("/getusers",async (req,res, next) =>{
  if(!req.headers.authorization){
    next();
  }
  else{
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
    const accountIds = await account.query()
      .select('id')
      .where('account_lineage', '<@', u.parentAccount.account_lineage);
      let users = null

          users = await user.query()
          .leftJoin("contacts","users.contact_id","contacts.id")
          // .innerJoin("user_roles","user_roles.user_id","users.id")
          // .innerJoin("roles","user_roles.role_id","roles.id")
          .where('parent_account_id', 'in', accountIds.map(x => x.id))
          .andWhere('users.is_deleted', false)
          .andWhere(function (){
             this.orWhere('users.name','like', '%' + req.body.searchtext + '%')
             this.orWhere('users.username','like', '%' +  req.body.searchtext + '%')
             this.orWhere('contacts.email', 'like', '%' + req.body.searchtext + '%')
             this.orWhere('contacts.phone','like', '%' + req.body.searchtext + '%')
            //  this.orWhere('roles.name','like', '%' + req.body.searchtext + '%')
          
          })
          .eager("[roles, contact, parentAccount]");
      try {
        res.send(users);
      } catch (error) {
        res.send(
          JSON.stringify({
            error,
            stack: error.stack
          })
        );
      }
  };
})


// Get accounts
router.post("/getaccounts",async (req,res, next) =>{
  if(!req.headers.authorization){
    next();
  }
  else{
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
    let accounts = null
    accounts = await account.query()
      .innerJoin("account_type", 'account_type.id', 'accounts.type_id')
      .whereRaw(`account_lineage <@ '${u.parentAccount.account_lineage}'`)
      .andWhere('accounts.is_deleted', '!=', 'true')
      .andWhere(function () {
        this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
        this.orWhere('account_type.name', 'like', '%' + req.body.searchtext + '%')
      })
      .eager("[parent, type, contact, contact.address, brand, brand_detail,connection,connection_details,hoursof_operations,health_and_informations,legal_compliance,gbpconfig, template, accContactPersons]")
    try {
      res.send(accounts);
    } catch (error) {
      res.send(
        JSON.stringify({
          error,
          stack: error.stack
        })
      );
    }
  };
})

// Get patients
router.post("/getpatients", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);

    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    const accountIds = await account.query()
    .select('id')
    .where('account_lineage', '<@', u.parentAccount.account_lineage);

    const customers = await customer.query()
    .innerJoin("customer_contacts","customer_contacts.customer_id","customers.id")
    .innerJoin("contacts","customer_contacts.contact_id","contacts.id")
    .where('parent_account_id', 'in', accountIds.map(x => x.id))
    .andWhere('customers.is_deleted', false).andWhere('customers.is_enabled', true)
    .andWhere(function (){
      this.orWhere('customers.name','like', '%' + req.body.searchtext + '%')
      this.orWhere('customers.username','like', '%' + req.body.searchtext + '%')
      this.orWhere('contacts.email', 'like', '%' + req.body.searchtext + '%')
      this.orWhere('contacts.phone', 'like', '%' + req.body.searchtext + '%')
})
// .select(
//   'customers.name'
//   ,'customers.username'
//   ,customer.raw("to_char(customers.dob, 'MM/DD/YYYY') as dob")
// )
    .eager("[roles, contacts,legaldocs, customerType,customerLocations,address,alternetAgents]");
    res.send(customers);
    }
  });

    //Get flag value and email,mobile from account for Demo Rx | Added by Kishan | 01-Apr-2019
router.get("/getConnectionForDemoRx/", async (req, res, next) => {
  try
  {
   const token = req.headers.authorization.split(' ')[1];
   const decodedToken = jwt.decode(token);
   const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
 
     const accountConnectionsId = await account.query()
     .innerJoin('account_connections','account_connections.id','accounts.account_connections_id')
     .where('accounts.id', u.parent_account_id)
     .where('account_connections.name','iLocalBox Pharmacy')
     .select('account_connections_id','demo_email','demo_phone');
 
     res.send(accountConnectionsId);
  }
  catch(error){
   res.send(
     JSON.stringify({
       message: error.message,
       stack: error.stack
     })
   );
 }
 });

   
//  Get Stock List Report detail | DI-357 | Added by:04-03-2019 | Naresh Makwana
router.post("/getStockedExpiredItems", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
     
        const StockedExpiredDetail = await events.query()
          .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
          .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
          .innerJoin('accounts', 'accounts.id','customers.parent_account_id')
          .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
          .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
          // .innerJoin('users','events.subject_id','users.id')
          .innerJoin('event_type','event_type.id','events.type_id')
          .leftJoin('locationtype_master', 'locationtype_master.id', 'customers.locationtype_id')
          .where('prescriptions_master.type_id',RXSTATUS.HOLD)
          .where('event_type.name',EventType.RX_EXPIRED)
          .andWhere(
            function()
            {
              if(req.body.store_ids.length > 0)
              {
                this.where('accounts.id', 'in', req.body.store_ids)
              }
              else{
                this.where('accounts.id', 'in', accountIds.map(x => x.id))
              }
              if(req.body.box_ids.length > 0)
              {
                this.where('boxes.id', 'in', req.body.box_ids)
              }
              else
              {
                this.where('boxes.id', 'in',boxes.map(x=>x.id))
               }
                if(req.body.fromdate !== ""){
                  this.whereRaw('??::date >= ?', ['events.created_on', req.body.fromdate])
                }
                if(req.body.todate !== ""){
                  this.whereRaw('??::date <= ?', ['events.created_on', req.body.todate])
                }
            }
          )
          .andWhere(function () {
            if(req.body.searchtext !== "")
            {
              this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescriptions_master.bin_id', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
              // this.orWhere('prescription_detail.fillnumber', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.prescriptionnumber', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescriptions_master.stock_code', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescriptions_master.pucode', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('users.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('locationtype_master.name', 'like', '%' + req.body.searchtext + '%')
            }
          })
          .select(
             'accounts.name as store'
            , 'boxes.name as box'
            , 'prescriptions_master.bin_id as binid'
            , 'prescription_detail.rx_no as rxno'
            , 'prescription_detail.fillnumber as fillid'
            , 'prescription_detail.prescriptionnumber'            
            , 'prescriptions_master.fill_date as filldate'
            , 'prescriptions_master.stocked_date as stockeddate'
            , 'prescriptions_master.stock_code'
            , 'prescriptions_master.pucode as pucode'
            ,'customers.first_name as firstname'
            , 'customers.last_name as lastname'
            , 'prescription_detail.ndc_code as ndccode'
            , 'prescription_detail.drug_name as drugname'
            , 'prescription_detail.qty as qty'
            , 'prescription_detail.price as price'
            // ,'users.name as username'
            ,'locationtype_master.name as deliverytype'
          )
          .orderBy(
            'events.created_on','desc'
          );
        res.send(StockedExpiredDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
 
});

//Kiosk Save Customer Collected Detail | DI-496 |
router.post("/saveKioskCustomerCollectedDetail",upload.any(), async (req, res, next) => { 
  let picture=null;
     picture = (req.body.picture_url !== null && req.body.picture_url !== 'null') ? new Buffer(req.body.picture_url.replace(/^data:image\/\w+;base64,/, ""),'base64'):null;
  let signature =null;
    signature = (req.body.signature_url !== null && req.body.signature_url !== 'null') ? new Buffer(req.body.signature_url.replace(/^data:image\/\w+;base64,/, ""),'base64'):null;
  let s3pictureLocation=null;
  let s3signatureLocation=null;
  let kioskBoxAccountConnection = req.body.kioskBoxAccountConnection;
  if(picture!==null && picture!=='null' && picture!==undefined && picture!=='undefined' && picture!==''){
    let s3response = {};
    let params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(),
      Body: picture,
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg',
      ACL: "public-read"
    };
    s3response = await s3.upload(params).promise();
    s3pictureLocation= s3response.Location;
  }
 if(signature!==null && signature!=='null' && signature!==undefined && signature!=='undefined' && signature!==''){
    let s3response = {};
    let params = {
      Bucket: process.env.IMAGES_BUCKET,
      Key: uuidv4(),
      Body: signature,
      ContentEncoding: 'base64',
      ContentType: 'image/jpeg',
      ACL: "public-read"
    };
    s3response = await s3.upload(params).promise();
    s3signatureLocation= s3response.Location;
  }
  let orderIds = req.body.orderIds.length > 0 ? JSON.parse(req.body.orderIds) : [];
  let isConsultRequested;
  if (req.body.is_consult_requested === undefined || req.body.is_consult_requested === 'undefined' || req.body.is_consult_requested === null || req.body.is_consult_requested === 'null' || req.body.is_consult_requested == '') {
    isConsultRequested = false;
  } else {
    isConsultRequested = req.body.is_consult_requested;
  }
  if(orderIds.length > 0)
  {
    for(let i=0;i< orderIds.length ;i++)
    {
      const savekioskcustomerobj = { 
        customer_id:req.body.customer_id,
        order_id:orderIds[i].id,
        picture_url:s3pictureLocation,
        signature_url:s3signatureLocation, 
        drivers_license_no:req.body.drivers_license_no,
        first_name:req.body.first_name,
        middle_name:req.body.middle_name,
        last_name:req.body.last_name,
        name: `${req.body.first_name} ${req.body.last_name}`,
        dob:req.body.dob,
        relationship:req.body.relationship,
        box_id:req.body.box_id,
        account_id:req.body.account_id,
        selectedstatecode: req.body.selectedStateCode,
        dlrawdata: req.body.dlrawdata,
        /*
          Modified By    : Belani Jaimin
          Modified Date  : 02/10/2020
          Description    : Record of declined consultations. Each pick up where the patient says "I'm Good" is a decline
        */
        is_consult_requested: isConsultRequested
      };
      console.log(`savekioskcustomerobj : ${JSON.stringify(savekioskcustomerobj, null, 2)}`);
      try{
        const _checkSameOrderExistForThisCustomer = await customer_collect_detail.query().where('order_id', savekioskcustomerobj.order_id);
        if(_checkSameOrderExistForThisCustomer && _checkSameOrderExistForThisCustomer.length === 0) {
          let pms, rxComplete;
          pms = await customer_collect_detail.query().insertGraphAndFetch(savekioskcustomerobj);
          if(kioskBoxAccountConnection!=='' && 
          kioskBoxAccountConnection!==null && 
          kioskBoxAccountConnection!=='null' && 
          kioskBoxAccountConnection!==undefined && 
          kioskBoxAccountConnection!=='undefined'){
            if(kioskBoxAccountConnection === ConnectionType.PIONEERRX)
            if(pms && pms!==undefined && pms!==null){
              rxComplete = await PioneerRxComplete(savekioskcustomerobj);
            }
            if(kioskBoxAccountConnection === ConnectionType.EPIC){
              rxComplete = await EpicRxComplete(savekioskcustomerobj);
            }
          }        
          res.send(
            JSON.stringify({        
               pms             
            })
          );
        } else {
          console.log(`length of _checkSameOrderExistForThisCustomer is ${_checkSameOrderExistForThisCustomer.length} and _checkSameOrderExistForThisCustomer : ${JSON.stringify(_checkSameOrderExistForThisCustomer, null, 2)}`);
          const _sameOrderUpdate = await customer_collect_detail.query().where('order_id', savekioskcustomerobj.order_id).patch({
            picture_url: savekioskcustomerobj.picture_url,
            signature_url: savekioskcustomerobj.signature_url,
            drivers_license_no: savekioskcustomerobj.drivers_license_no,
            first_name: savekioskcustomerobj.first_name,
            middle_name: savekioskcustomerobj.middle_name,
            last_name: savekioskcustomerobj.last_name,
            name: `${savekioskcustomerobj.first_name} ${savekioskcustomerobj.last_name}`,
            dob: savekioskcustomerobj.dob,
            relationship: savekioskcustomerobj.relationship,
            selectedstatecode: savekioskcustomerobj.selectedstatecode,
            dlrawdata: savekioskcustomerobj.dlrawdata,
          });
          res.send(
            JSON.stringify({
              pms: _sameOrderUpdate
            })
          );
          // res.send(
          //   JSON.stringify({
          //     pms: `This pickup entry is already exist`
          //   })
          // );
        }
      }catch(error){
        res.send(
          JSON.stringify({
            message: error.message,
            stack: error.stack
          })
        );
      } 
    }
  }
   
});
//End Naresh

//Check if Email  is available or not in DB
router.get("/checkemailexistornot/:id", async (req, res, next) => {
  let isExist = false;
  const users = await user.query()
  .where('username', req.params.id)
  console.log(users);
  if(users.length > 0){
    isExist = true;
  }else{
    isExist = false;
  }
  res.send(
    JSON.stringify({
      email_Is_Exist : isExist
    })
  )

});
//added by:Naresh | DI-270 | Save Customer ratting | 
router.post("/saveCustomerRatting", async (req, res, next) => { 
  const rattingobj = {
    id:req.body.order_id,
    score:req.body.score,
    is_ratting_given:req.body.score > 0 ? true:false
  }
  try{
    const usor = await prescriptions_master.query()
    .upsertGraphAndFetch(rattingobj,
    {
      relate:true,
      unrelate:false
    });
    res.send(
      JSON.stringify({        
        usor
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
});
//Added by :Naresh | DI-270 | date:08-04-2019 |
router.post("/getSurveyReport", async (req, res, next) => { 
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))

      const SurveyDetail = await events.query()
          .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
          .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
          .innerJoin('accounts',  'accounts.id','customers.parent_account_id')
          .innerJoin("boxes", 'boxes.id', 'events.box_id')
          .innerJoin('event_type', 'event_type.id', 'events.type_id')
          .leftJoin('customer_collect_detail','customer_collect_detail.order_id','events.tofor_id')    
          .leftJoin('locationtype_master', 'locationtype_master.id', 'customers.locationtype_id')
          .where('event_type.name', EventType.PICKED_UP)
          .where('prescriptions_master.is_ratting_given',true)
          .andWhere(
            function()
            {
              if(req.body.store_ids.length > 0)
              {
                this.where('accounts.id', 'in', req.body.store_ids)
              }
              else{
                this.where('accounts.id', 'in', accountIds.map(x => x.id))
              }
              if(req.body.box_ids.length > 0)
              {
                this.where('boxes.id', 'in', req.body.box_ids)
              }
              else
              {
                this.where('boxes.id', 'in',boxes.map(x=>x.id))
               }
                if(req.body.fromdate !== ""){
                  this.whereRaw('??::date >= ?', ['events.created_on', req.body.fromdate])
                }
                if(req.body.todate !== ""){
                  this.whereRaw('??::date <= ?', ['events.created_on', req.body.todate])
                }
            }
          )
          .andWhere(function () {
            if(req.body.searchtext !== "")
            {
              this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customer_collect_detail.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('locationtype_master.name', 'like', '%' + req.body.searchtext + '%')
            }
          })
          .select(
              'customers.first_name as firstname'
            ,'customers.last_name as lastname'
            ,'events.created_on'
            ,'prescriptions_master.score'
            ,'accounts.name as store'
            ,'boxes.name as box'
            ,'customer_collect_detail.name'
            ,'locationtype_master.name as deliverytype'
          )
          .orderBy(
            'events.created_on','desc'
          );
        res.send(SurveyDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//End Naresh

//Get role permissions from account at kioskside | Added by varshit   | 10-04-2019
router.get("/getRolesPermissions/", async (req, res, next) => {
  console.log("Callsuccess");
  try
  {
    let isSuperAdmin = false;
    let _isSuper = 'ef8cc458-4e0d-428e-b40a-051d99a5c37c';
   const token = req.headers.authorization.split(' ')[1];
   const decodedToken = jwt.decode(token);
   const u = await user.query().where('username', decodedToken.email).eager("[parentAccount,roles]").first();
 
     const Permissons = await account.query()
     .innerJoin('role_permission_details','role_permission_details.account_id','accounts.id')
     .innerJoin('permissions','permissions.id','role_permission_details.permission_id')     
     .where('accounts.id', u.parent_account_id)
     .where('permissions.type_id','!=','6b163db7-f5e2-4cf8-b6d4-0345ed0bb6f3')
     .select(account.raw(
     'CASE WHEN role_permission_details.is_allowed = true THEN 1 ELSE 0 END AS is_allowed'
     ))
     .orderBy('permissions.order_id','asc');
           
     if(u.roles.length > 0){
      for(let i=0; i<u.roles.length; i++){
        console.log(`Role of ${ i } permission id is ${ u.roles[i].id }`)
        if(u.roles[i].id===_isSuper){
          isSuperAdmin = true
        }
      }
     }     

     res.send(JSON.stringify({
      superAdmin: isSuperAdmin,
      permission: Permissons
    }))
    // res.send(Permissons);   
  }
  catch(error){
   res.send(
     JSON.stringify({
       message: error.message,
       stack: error.stack
     })
   );
 }
 });

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get chart data for Transactions
  JIRA ID : DI-246 
*/
router.get("/dashlate/:fromdate/:todate/:id", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      let type = req.params.id;  
      let _dates = [];
      _dates.push(req.params.fromdate);
      _dates.push(req.params.todate);
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);

      let event = []
      if(type==='0' || type==='1' || type==='2'){
         event = await prescriptions_master.query()
                .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
                .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
                .innerJoin('accounts','accounts.id','boxes.parent_account_id')
                .select(
                  prescriptions_master.raw('COUNT(prescription_detail.rx_no) as t_count'),
                  prescriptions_master.raw("COUNT(DISTINCT(prescriptions_master.customer_id)) as c_count"),
                  prescriptions_master.raw("to_timestamp(prescriptions_master.pickup_date::text, 'yyyy-mm-dd')")
                ).orderBy('to_timestamp', 'desc')
                .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.DELIVERED, RXSTATUS.COMPLETED])
                // .where('accounts.id', u.parent_account_id)
                .where('accounts.id', 'in' , accountIds.map(x=>x.id))
                .andWhere(function(){
                  this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
                  this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]])
                }).groupBy(
                  prescriptions_master.raw("to_timestamp(prescriptions_master.pickup_date::text, 'yyyy-mm-dd')")
                )
      }else if(type==='3'){
        event = await prescriptions_master.query()
                .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
                .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
                .innerJoin('accounts','accounts.id','boxes.parent_account_id')
                .select(
                  prescriptions_master.raw('COUNT(prescription_detail.rx_no) as t_count'),
                  prescriptions_master.raw("COUNT(DISTINCT(prescriptions_master.customer_id)) as c_count"),
                  prescriptions_master.raw('EXTRACT(MONTH from prescriptions_master.pickup_date) as date_part'),                                          
                  prescriptions_master.raw('EXTRACT(YEAR from prescriptions_master.pickup_date) as date_part_Year'),
                ).orderBy('date_part','desc')
                // .where('prescriptions_master.type_id',RXSTATUS.PICKED_UP)
                .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.DELIVERED, RXSTATUS.COMPLETED])
                // .where('accounts.id', u.parent_account_id)
                .where('accounts.id', 'in' , accountIds.map(x=>x.id))
                .andWhere(function(){
                  this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[1]])
                  this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[0]])
                }).groupBy(
                  prescriptions_master.raw('EXTRACT(MONTH from prescriptions_master.pickup_date), EXTRACT(YEAR from prescriptions_master.pickup_date)')
                )
      }else if(type==='4'){
        event = await prescriptions_master.query()
                .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
                .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
                .innerJoin('accounts','accounts.id','boxes.parent_account_id')
                .select(
                  prescriptions_master.raw('COUNT(prescription_detail.rx_no) as t_count'),
                  prescriptions_master.raw("COUNT(DISTINCT(prescriptions_master.customer_id)) as c_count"),
                  prescriptions_master.raw('EXTRACT(YEAR from prescriptions_master.pickup_date) as date_part'),                                          
                ).orderBy('date_part','desc')
                // .where('prescriptions_master.type_id',RXSTATUS.PICKED_UP)
                .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.DELIVERED, RXSTATUS.COMPLETED])
                // .where('accounts.id', u.parent_account_id)
                .where('accounts.id', 'in' , accountIds.map(x=>x.id))
                .andWhere(function(){
                  this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
                  this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]])
                }).groupBy(
                  prescriptions_master.raw('EXTRACT(YEAR from prescriptions_master.pickup_date)')
                )
                
      }  
      console.log("Getting Events : =====> " + JSON.stringify(event))
      res.send(event);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
});

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get today date for from and to date
  JIRA ID : DI-246 
*/
async function todayDates(){
  let _today = new Date();
  let _today1 = new Date();
  return [_today, _today1];
}

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get dates of current week
  JIRA ID : DI-246 
*/
async function currentWeek(date){
  let now = date? new Date(date) : new Date();
  now.setHours(0,0,0,0);
  let monday = new Date(now);
  monday.setDate(monday.getUTCDate() - monday.getUTCDay() + 1);
  let sunday = new Date(now);
  sunday.setDate(sunday.getUTCDate() - sunday.getUTCDay() + 7);
  return [monday, sunday];
}

async function lastThereeMonths(date){  
  let backdate = new Date();
  backdate.setMonth(backdate.getMonth() - 3);
  return [date, backdate];
}

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get dates of current month
  JIRA ID : DI-246 
*/

async function currentMonth(date){
  let nowdate = date;
  let monthStartDay = new Date(nowdate.getUTCFullYear(), nowdate.getUTCMonth(), 1);
  let monthEndDay = new Date(nowdate.getUTCFullYear(), nowdate.getUTCMonth() + 1, 0);  
  return [monthStartDay, monthEndDay];
}

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get dates of current year from JAN-01 to DEC-31
  JIRA ID : DI-246 
*/

async function currentYear(){
  let yearStartDate = new Date(new Date().getUTCFullYear(), 0, 1, 0, 0, 0, 0);
  let yearEndDate = new Date(new Date().getUTCFullYear(), 12, 0, 0, 0, 0, 0);
  return [yearStartDate, yearEndDate];
}

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get reports for Stocked Transactions
  JIRA ID : DI-246 
*/

router.get("/stocktransactions/:id", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      let type = req.params.id;  
      let _dates = [];
      if(type==='0'){
        _dates = await todayDates();
      }else if(type==='1'){    
        _dates = await currentWeek(new Date());    
      }else if(type==='2'){    
        _dates = await currentMonth(new Date());
      }else if(type==='3'){    
        _dates = await currentYear();
      }

      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      console.log("Decoded Token : " + JSON.stringify(decodedToken));
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      console.log("Getting details for account id : " + JSON.stringify(accountIds));
      const TransactionDetail = await events.query()
        .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
        .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
        .innerJoin('accounts', 'accounts.id','customers.parent_account_id')
        .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
        .innerJoin('users','events.subject_id','users.id')
        .innerJoin('event_type','event_type.id','events.type_id')        
        .where('accounts.id', '=', u.parent_account_id)          
        .whereRaw('??::date >= ?', ['prescriptions_master.stocked_date', _dates[0]])
        .whereRaw('??::date <= ?', ['prescriptions_master.stocked_date', _dates[1]])          
        .where('prescriptions_master.type_id',RXSTATUS.STOCKED)
        // .where('prescriptions_master.type_id',RXSTATUS.PICKED_UP)                
        .where('event_type.name',EventType.STOCKED)
        // .where('event_type.name',EventType.PICKED_UP)        
        .select(
            'accounts.name as store'
          , 'boxes.name as box'
          , 'prescriptions_master.bin_id as binid'
          , 'prescription_detail.rx_no as rxno'
          , 'prescriptions_master.fill_date as filldate'
          , 'prescriptions_master.stocked_date as stockeddate'
          , 'prescriptions_master.stock_code'
          , 'prescriptions_master.pucode as pucode'
          ,'customers.first_name as firstname'
          , 'customers.last_name as lastname'
          , 'prescription_detail.ndc_code as ndccode'
          , 'prescription_detail.qty as qty'
          , 'prescription_detail.price as price'
          ,'users.name as username'
        )
        .orderBy(
          'prescriptions_master.stocked_date','desc'
        );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
 
});

/*
  Added By : Belani Jaimin
  Added Date : 26/03/2019
  Description : To get reports for Picked up Transactions
  JIRA ID : DI-246 
*/

router.get("/pickuptransactions/:id", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      let type = req.params.id;  
      let _dates = [];
      if(type==='0'){
        _dates = await todayDates();
      }else if(type==='1'){    
        _dates = await currentWeek(new Date());    
      }else if(type==='2'){    
        _dates = await currentMonth(new Date());
      }else if(type==='3'){
        _dates = await lastThereeMonths(new Date());
      }else if(type==='4'){    
        _dates = await currentYear();
      }
      
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      console.log("Decoded Token : " + JSON.stringify(decodedToken));
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      console.log("Getting details for account id : " + JSON.stringify(accountIds));
      const TransactionDetail = await events.query()
        .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')
        .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
        .innerJoin('accounts', 'accounts.id','customers.parent_account_id')
        .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
        .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
        // .innerJoin('users','events.subject_id','users.id')
        .innerJoin('event_type','event_type.id','events.type_id')    
        .leftJoin('customer_collect_detail','customer_collect_detail.order_id','prescriptions_master.id')    
        // .where('accounts.id', '=', u.parent_account_id)     
        .where('accounts.id', 'in' , accountIds.map(x=>x.id))
        .andWhere(function(){
          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]])
        })     
        // .where('prescriptions_master.type_id',RXSTATUS.STOCKED)
        .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.DELIVERED, RXSTATUS.COMPLETED])
        // .where('event_type.name',EventType.STOCKED)
        .where('event_type.name', 'in', [EventType.PICKED_UP, EventType.DELIVERED, EventType.COMPLETED])        
        .select(
            'accounts.name as store'
          , 'boxes.name as box'
          , 'prescriptions_master.bin_id as binid'
          , 'prescription_detail.rx_no as rxno'
          , 'prescriptions_master.fill_date as filldate'
          , 'prescriptions_master.pickup_date as pickupdate'
          , 'prescriptions_master.stock_code'
          , 'prescriptions_master.pucode as pucode'
          , 'customers.first_name as firstname'
          , 'customers.last_name as lastname'
          , 'prescription_detail.ndc_code as ndccode'
          , 'prescription_detail.qty as qty'
          , 'prescription_detail.price as price'
          , 'customers.name as username'
          , 'customer_collect_detail.signature_url as signatureurl'
          , 'customer_collect_detail.picture_url as pictureurl'
        )
        .orderBy(
          'prescriptions_master.pickup_date','desc'
        );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }  
});

router.get("/sales/:fromdate/:todate/:id", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      let prescriptions = []
      let type = req.params.id;  
      let _dates = [];
      _dates.push(req.params.fromdate);
      _dates.push(req.params.todate);
      
      console.log("Type is : "+ type + " and " + "Dates are : " + JSON.stringify(_dates));
      let event = [];
      let prescription_summary = { 
        total_prescriptions : 0,
        total_prescriptions_sales : 0
      }
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);

      const summary = await prescriptions_master.query()
                      .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
                      .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
                      .innerJoin('accounts','accounts.id','boxes.parent_account_id')
                      .select(                      
                        prescriptions_master.raw('CASE WHEN SUM(prescription_detail.price) IS NULL THEN 0 ELSE SUM(prescription_detail.price) END AS total_prescriptions_sales'),
                        prescriptions_master.raw('COUNT(prescription_detail.rx_no) as total_prescriptions')
                      )
                      .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.COMPLETED, RXSTATUS.DELIVERED])  
                      // .where('accounts.id', u.parent_account_id)
                      .where('accounts.id', 'in' , accountIds.map(x=>x.id))
                      .andWhere(function () {
                        if(type==='0' || type==='1' || type==='2') {
                          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
                          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]]) 
                        } else if(type==='3'){
                          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[0]])
                          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[1]]) 
                        } else if(type==='4') {
                          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
                          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]]) 
                        }
                      })
      prescription_summary.total_prescriptions = summary[0];
      prescription_summary.total_prescriptions_sales = summary[1];
      console.log("Prescription Summary :==> " + JSON.stringify(prescription_summary));
      if(type==='0' || type==='1' || type==='2'){
        event = await prescriptions_master.query()
        .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
        .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
        .innerJoin('accounts','accounts.id','boxes.parent_account_id')
        .select(                      
          prescriptions_master.raw('CASE WHEN SUM(prescription_detail.price) IS NULL THEN 0 ELSE SUM(prescription_detail.price) END AS total_sales'),
          prescriptions_master.raw("to_timestamp(prescriptions_master.pickup_date::text, 'yyyy-mm-dd') as date")
        ).orderBy('date','desc')
        .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.COMPLETED, RXSTATUS.DELIVERED])  
        // .where('accounts.id', u.parent_account_id)
        .where('accounts.id', 'in' , accountIds.map(x=>x.id))
        .andWhere(function () {
          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]])
        })
        .groupBy(                                                            
          prescriptions_master.raw("to_timestamp(prescriptions_master.pickup_date::text, 'yyyy-mm-dd')")
        ); 
      }else if(type==='3'){
        event = await prescriptions_master.query()
        .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
        .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
        .innerJoin('accounts','accounts.id','boxes.parent_account_id')
        .select(
          prescriptions_master.raw('CASE WHEN SUM(prescription_detail.price) IS NULL THEN 0 ELSE SUM(prescription_detail.price) END AS total_sales'),
          prescriptions_master.raw('EXTRACT(MONTH from prescriptions_master.pickup_date) AS date'),
          prescriptions_master.raw('EXTRACT(YEAR from prescriptions_master.pickup_date) AS date_year')
        ).orderBy('date','desc')
        .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.COMPLETED, RXSTATUS.DELIVERED])  
        // .where('accounts.id', u.parent_account_id)
        .where('accounts.id', 'in' , accountIds.map(x=>x.id))
        .andWhere(function(){
          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[1]])
          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[0]])
        }).groupBy(                                                            
          prescriptions_master.raw('EXTRACT(MONTH from prescriptions_master.pickup_date), EXTRACT(YEAR from prescriptions_master.pickup_date)')
        );        
      }else if(type==='4'){
        event = await prescriptions_master.query()
        .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
        .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
        .innerJoin('accounts','accounts.id','boxes.parent_account_id')
        .select(
          prescriptions_master.raw('CASE WHEN SUM(prescription_detail.price) IS NULL THEN 0 ELSE SUM(prescription_detail.price) END AS total_sales'),
          prescriptions_master.raw('EXTRACT(YEAR from prescriptions_master.pickup_date) AS date')
        ).orderBy('date','desc')
        .where('prescriptions_master.type_id','in',[RXSTATUS.PICKED_UP, RXSTATUS.COMPLETED, RXSTATUS.DELIVERED])  
        // .where('accounts.id', u.parent_account_id)
        .where('accounts.id', 'in' , accountIds.map(x=>x.id))
        .andWhere(function(){
          this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
          this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]])
        }).groupBy(                                                            
          prescriptions_master.raw('EXTRACT(YEAR from prescriptions_master.pickup_date)')
        );
      }
      prescriptions.push(prescription_summary)
      prescriptions.push(event);
      res.send(
        JSON.stringify({
          Prescriptions: prescriptions,          
        })
      );  
    }
        
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

/*
  Added By : Belani Jaimin
  Added Date : 04/17/2019
  Description : To get box vise sales summary
  JIRA ID : DI-246 
*/
router.get("/box-sales", async (req, res, next) => {
  try{   
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);

      let _dates = [];
      _dates = await todayDates();

      const boxSales = await prescriptions_master.query()
      .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
      .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
      .innerJoin('accounts','accounts.id','boxes.parent_account_id')
      .where('prescriptions_master.type_id',RXSTATUS.PICKED_UP)
      // .where('accounts.id', u.parent_account_id)
      .where('accounts.id', 'in' , accountIds.map(x=>x.id))
      .andWhere(function () {
        this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', _dates[0]])
        this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', _dates[1]])
      })
      .orderBy('boxes.name')
      .select(
        "boxes.name as app",
        prescriptions_master.raw("prescriptions_master.pickup_date as date"),
        prescriptions_master.raw("count(prescription_detail.rx_no) as value"),
        prescriptions_master.raw('CASE WHEN SUM(prescription_detail.price) IS NULL THEN 0 ELSE SUM(prescription_detail.price) END AS total_daily_sales'),
      ).groupBy(
        'boxes.name',
        prescriptions_master.raw("prescriptions_master.pickup_date")
      )
      res.send(
        JSON.stringify({
          boxsales: boxSales,          
        })
      );  
	  }   
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//Get Branding in offline mode | Added by Varshit | 16-04-2019
router.get("/getofflinebrandbybox/:id", async (req, res, next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
     const  theBox = await box.query()
      .innerJoin('accounts','accounts.id','boxes.parent_account_id')
      .leftJoin('brands','brands.id','accounts.brand_id')
      .leftJoin('brand_detail','accounts.id','brand_detail.parent_account_id')
      .select('brands.logo_url as brand_logo_url','brand_detail.logo_url as brand_detail_logo_url','brand_detail.ad_number as ad_number') //Naresh:Add new 'ad_number' | Date:30-09-2019
      .where('boxes.kiosk_box_id',req.params.id);

      try{
        let boxes = await box.query().where('kiosk_box_id',req.params.id).eager('[account]').first();
        console.log(`Get Box details box id ${req.params.id} is ${JSON.stringify(boxes)}`);
        if(boxes!==null){
          const acc = await account.query()
          .update({is_user_logged_in: true}).where('id', boxes.parent_account_id);          
        }              
      }
      catch(error){
        console.log(JSON.stringify(error));
      }                            
      res.send(JSON.stringify({theBox}));     
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
 });


 //Get Health & Information Videos in offline mode | Added by Aamir | 10-08-2019
router.get("/getofflinevideosbybox/:id", async (req, res, next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
     const  thevideos = await box.query()
     .innerJoin('accounts','accounts.id','boxes.parent_account_id')
     .innerJoin('health_and_information_master','health_and_information_master.account_id','accounts.id')
     .innerJoin('category_master','category_master.id','health_and_information_master.category_id')
     .select('health_and_information_master.id','health_and_information_master.category_id as categoryid','category_master.name as categoryname','health_and_information_master.title','health_and_information_master.description','health_and_information_master.video_url as url','health_and_information_master.primarylibrary_id','health_and_information_master.primarylibrary_title1','health_and_information_master.primarylibrary_title2')
     .where('boxes.kiosk_box_id',req.params.id);

      res.send(JSON.stringify({thevideos}));     
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
 });

 //Added by Naresh | DI-461 | Date:16-04-2019 | Kiosk Status Notification (Offline)
 router.get("/getKioskNotification", async (req, res, next) => {  
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountDetail = await account.query().where('id', u.parent_account_id)
                                  .eager("[parent, type, contact, contact.address, brand]");
      const schedule = await notifications.query().innerJoin('customers','customers.id','notifications.patient_id').where('account_id', u.parent_account_id).where('is_read',false).select('notifications.id','customers.name','notifications.schedule');                            
      let boxes = await box.query().where('parent_account_id', u.parent_account_id).where('IsActive',true).eager("[address, type, schedule]");      
      let boxesOffline =[];
      for(let i=0;i<boxes.length;i++)
      {
        boxes[i].lastCommunicationDate = null;
        boxes[i].status = null;
        const event = await events.query()
                      .where('box_id',boxes[i].id)
                      .andWhere('object_id','!=',RXSTATUS.ASSIGNED)
                      .orderBy('created_on','desc');

        if(event && event.length > 0){
          boxes[i].lastCommunicationDate = event[0].created_on;
        }     
        // let sys_date = new Date().toUTCString();
        // let actualcreateddate = '';
        if(boxes[i].lastCommunicationDate!==null 
          && boxes[i].lastCommunicationDate!==undefined 
          && boxes[i].lastCommunicationDate!=='null' 
          && boxes[i].lastCommunicationDate!=='' 
          && boxes[i].lastCommunicationDate!=='undefined')
        {
        
          // actualcreateddate = new Date(boxes[i].lastCommunicationDate).toUTCString();
          // let parseSysDate = new Date(sys_date)
          // let parseActualDate = new Date(actualcreateddate);
          // let time_diff = parseSysDate.getTime() - parseActualDate.getTime();
          // let minutes = Math.ceil(time_diff / (1000 * 60));
          //24 Hours && boxes[i].is_email_send === false
          // if(minutes > (24 * 60)){   
          //   if(boxes[i].is_email_send === false)
          //   {
          //     const Config = {
          //       firstName: accountDetail[0].contact.name,
          //       lastName:  accountDetail[0].contact.name,
          //       emailId: accountDetail[0].contact.email,
          //       companyLogo: '',
          //        companyName: '',
          //        companyContact: '',
          //        adressLine1: '',
          //        adressLine2: '',
          //        city: '',
          //        state: '',
          //        zipCode: '',
          //       boxName: boxes[i].name,
          //       boxAddress: boxes[i].address ?
          //                    (boxes[i].address.address_line_1+' '
          //                    + boxes[i].address.address_line_2+', '
          //                    + boxes[i].address.city+', '
          //                    + boxes[i].address.state+' '
          //                    + boxes[i].address.zip+''):'',
          //       kiosk_box_id: boxes[i].kiosk_box_id,
          //       last_access_date:  getDateTime(boxes[i].last_access_date),
          //       status :'offline',
          //       pharmAddress: {
          //          picture_url:accountDetail[0].picture_url ? accountDetail[0].picture_url : 'https://s3.amazonaws.com/rpx-dev-ext-images-qr/NoImageFound.png',
          //          companyName:accountDetail[0].name,
          //         adressLine1:accountDetail[0].contact && accountDetail[0].contact.address && accountDetail[0].contact.address.address_line_1 ? accountDetail[0].contact.address.address_line_1:'', 
          //         adressLine2:accountDetail[0].contact && accountDetail[0].contact.address && accountDetail[0].contact.address.address_line_2 ? accountDetail[0].contact.address.address_line_2:'',
          //         city:accountDetail[0].contact && accountDetail[0].contact.address && accountDetail[0].contact.address.city ? accountDetail[0].contact.address.city:'',
          //         state:accountDetail[0].contact && accountDetail[0].contact.address && accountDetail[0].contact.address.state ? accountDetail[0].contact.address.state:'',
          //         zipcode:accountDetail[0].contact && accountDetail[0].contact.address && accountDetail[0].contact.address.zip ? accountDetail[0].contact.address.zip:'',
          //        },
          //        supportPharmacyEmail: accountDetail[0] && accountDetail[0].brand && accountDetail[0].brand.email
          //     };
          //     // if(accountDetail[0].contact.phone !== null && accountDetail[0].contact.phone!== undefined && accountDetail[0].contact.phone !== "")
          //     // {
          //     //   /* SMS Info starts */
          //     //   const PhoneNumber = '+1' + ReplacePhone(accountDetail[0].contact.phone);
          //     //   const Message = 'Box '+Config.boxName+' is offline. '+
          //     //                   'Box Address: '+Config.boxAddress+' '+
          //     //                   'Box ID: '+Config.kiosk_box_id;
          //     //   sendAWSSMS(PhoneNumber,Message);
          //     // /* SMS Info ends */
          //     // }
          //     if(accountDetail[0].contact.email !== null && accountDetail[0].contact.email!== undefined && accountDetail[0].contact.email !== "")
          //     {
          //       sendAWSEmailKioskOffline(Config);
          //       const boxObject = {
          //         id: boxes[i].id,
          //         is_email_send:true,
          //         email_send_date:new Date()
          //       }
          //       const b = await box.query()
          //         .upsertGraphAndFetch(boxObject, {
          //           relate: true,
          //           unrelate: true
          //         });
          //        boxes[i].is_email_send =  b.is_email_send;
          //        boxes[i].email_send_date =  b.email_send_date;
          //       }
          //     }
          //     boxes[i].status='Offline';
          //   boxesOffline.push(boxes[i]);
          // }
          // else
          // {
          //   const boxObject = {
          //     id: boxes[i].id,
          //     is_email_send:false,
          //     email_send_date:null
          //   }
          //   const b = await box.query()
          //   .upsertGraphAndFetch(boxObject, {
          //     relate: true,
          //     unrelate: true
          //   });
          //   console.log(b);
          // }
        }
      }
      res.send(
        JSON.stringify({
          boxesOffline : boxesOffline,
          schedule : schedule
        })
      );
    }
  }
  catch(error){
    res.send(error.message);
  }
});
//End Naresh | DI-461 
/* Added by: Jaydutt
Aim : Optimize Email Process */
router.post("/sendEmail", async (req, res, next) => {
  const configuration = req.body.pickupConfig;
  try {
    console.log('Configurations for Email: '+ JSON.stringify(configuration))
    prescriptionNotification(configuration);
    res.send('Email SuccessFully Sent');
  }
    catch(e){
      console.log('Couldnt send email: '+e)
    }
});
async function sendEmail(pickupConfig){
  const configuration = pickupConfig;
  try {
    configuration.qrCode = await generateQR(configuration.pickupcode)
    console.log('Configurations for Email: '+ JSON.stringify(configuration))
    prescriptionNotification(configuration);
  }
    catch(e){
      console.log('Couldnt send email: '+e)
    }
}
async function generateQR(text) {
  let s3response = { Location: '' };
  let qrCodeS3Location = null;
  
  console.log('MyPickupCode : '+ text)
  try {
    let tempQr = null;
    tempQr = await QRCode.toDataURL(text);
    const type = tempQr.split(';')[0].split('/')[1];
    const base64Data = new Buffer(
      tempQr.replace(/^data:image\/\w+;base64,/, ''),
      'base64'
    );
    var params = {
      Bucket: AWSconfig.bucket, // Bucket name
      Key: uuidv4(), // type is not required
      Body: base64Data,
      ACL: 'public-read',
      ContentEncoding: 'base64', // required
      ContentType: `image/${type}` // required. Notice the back ticks
    };
    s3response = await s3.upload(params).promise();
    qrCodeS3Location = s3response.Location;
    return qrCodeS3Location;
  } catch (err) {
    console.error(err);
    return err;
  }
};
// JD | DI-566
  //End Naresh | DI-461 

  /*
  Added By : Naresh Makwana
  Added Date : 30/04/2019
  Description : To get reports for Consultations
  JIRA ID : DI-91 
*/
router.post("/getConsultationsReports", async (req, res, next) => {
  let page = req.body.page;
  let pageSize = req.body.pageSize;
  let type = req.body.date;
  let status = req.body.status;
  let _dates = [];
  if(type==='0'){
    _dates = await todayDates();  
  }else if(type==='1'){    
    _dates = await currentWeek(new Date());    
  }else if(type==='2'){    
    _dates = await currentMonth(new Date());
  }else if(type==='3'){
    _dates = await lastThereeMonths(new Date());
  }else if(type==='4'){    
    _dates = await currentYear();
  }
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      let consultations=[];
      consultations = await user_consult_master.query()
      .select(
        'user_consult_master_id',
        'consult_start_date',
        'consult_end_date',
        user_consult_master.raw("to_char(user_consult_master.created_on,'MM/DD/YYYY HH24:MI:SS') as date"),
        user_consult_master.raw("to_char(user_consult_master.consult_start_date,'HH24:MI:SS') as start_time"),
        user_consult_master.raw("to_char(user_consult_master.consult_end_date,'HH24:MI:SS') as end_time"),
        'pickup_code',
        'user_name',
        'call_duration',
        'user_id',
        'consultation_name',
        'Status',
        'store_name',
        'kiosk_name',
        'accounts_id',
        'box_id',
        user_consult_master.raw("to_char(user_consult_master.created_on,'MM/DD/YYYY HH24:MI:SS') as created_on"),
      )
      .where('user_consult_master.accounts_id','in',accountIds.map(x=>x.id))
      .andWhere(function(){
        
        if(_dates.length > 0)
        {
          if(type==='0' || type==='1' || type==='2'){
            this.whereRaw('??::date >= ?', ['user_consult_master.created_on', _dates[0]])
            this.whereRaw('??::date <= ?', ['user_consult_master.created_on', _dates[1]])
          }else if(type==="3"){
            this.whereRaw('??::date >= ?', ['user_consult_master.created_on', _dates[1]])
            this.whereRaw('??::date <= ?', ['user_consult_master.created_on', _dates[0]])
          }else if(type==='4'){
            this.whereRaw('??::date >= ?', ['user_consult_master.created_on', _dates[0]])
            this.whereRaw('??::date <= ?', ['user_consult_master.created_on', _dates[1]])
          }
        }
      })
      .andWhere(function(){
        if(status === 'Missed')
         {
            this.where("user_consult_master.Status","=",'Missed')
        }else
        {
          this.where("user_consult_master.Status","!=",'Missed')
        }
      })
      .andWhere(function () {
        if(req.body.searchtxt !== "")
        {
          console.log("Nareshsearchtxt:-",req.body.searchtxt);
          
          this.orWhere('user_consult_master.store_name', 'like', '%' + req.body.searchtxt + '%')
          this.orWhere('user_consult_master.kiosk_name', 'like', '%' + req.body.searchtxt + '%')
          this.orWhere('user_consult_master.pickup_code', 'like', '%' + req.body.searchtxt + '%')
          this.orWhere('user_consult_master.user_name', 'like', '%' + req.body.searchtxt + '%')
          if(status !== 'Missed')
          {
            this.orWhere('user_consult_master.consultation_name', 'like', '%' + req.body.searchtxt + '%')
          }
        }})
      .page(page-1, pageSize);;
      res.send(consultations);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
 
});
//Consultations Dashboard detail
router.get("/getConsultationsDashboard/:fromdate/:todate/:id", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      let type = req.params.id;
      let _dates = [];
      _dates.push(req.params.fromdate);
      _dates.push(req.params.todate);
      // if(type==='0'){
      //   _dates = await todayDates();
      // }else if(type==='1'){    
      //   _dates = await currentWeek(new Date());    
      // }else if(type==='2'){    
      //   _dates = await currentMonth(new Date());
      // }else if(type==='3'){
      //   _dates = await lastThereeMonths(new Date());
      // }else if(type==='4'){    
      //   _dates = await currentYear();
      // }

      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      let consultations=[];
      consultations = await user_consult_master.query()
      .select(
        'user_consult_master_id',
        'consult_start_date',
        'consult_end_date',
        'pickup_code',
        'user_name',
        'call_duration',
        'user_id',
        'consultation_name',
        'Status',
        'store_name',
        'kiosk_name',
        'accounts_id',
        'box_id'
      )
      .where('user_consult_master.accounts_id','in',accountIds.map(x=>x.id))
      .andWhere(function(){
        if(_dates.length > 0)
        {
          if(type==='0' || type==='1' || type==='2'){
            this.whereRaw('??::date >= ?', ['user_consult_master.created_on', _dates[0]])
            this.whereRaw('??::date <= ?', ['user_consult_master.created_on', _dates[1]])
          }else if(type==='3'){
            this.whereRaw('??::date >= ?', ['user_consult_master.created_on', _dates[1]])
            this.whereRaw('??::date <= ?', ['user_consult_master.created_on', _dates[0]])
          }else if(type==='4'){
            this.whereRaw('??::date >= ?', ['user_consult_master.created_on', _dates[0]])
            this.whereRaw('??::date <= ?', ['user_consult_master.created_on', _dates[1]])
          }
        }
      });
      const Detail = await dashboardDetail(consultations);
      res.send(Detail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
 
});
async function dashboardDetail  (consultations){
  let second=0;
  let sumoftime="00:00:00";
  let ShortCount=0;
  let MediumCount=0;
  let LongCount=0;  
  let Misscall=0;
  let totalcount=0;
  let totalRecords = consultations.length;
  let dashboarddetail={
    ChartData:null,
    totalcount:totalcount,
    Misscall:Misscall,
    avgDuration:sumoftime,
    totalRecords:totalRecords
  };
  if(consultations.length > 0)
  {
    for(let i=0;i<consultations.length;i++)
    {
      let DurationInsec =  consultations[i].call_duration !== null
                      && consultations[i].call_duration !== undefined
                      &&  consultations[i].call_duration !== "" ?await convertHHMMSSToSeconds(consultations[i].call_duration):await convertHHMMSSToSeconds(sumoftime)
      second = second + DurationInsec;
      if(DurationInsec > 0 &&  DurationInsec <= (2.5 * 60))
      {
        ShortCount = ShortCount+1;
      }
      else if(DurationInsec > (2.5*60) && DurationInsec <= (5 * 60)){
        MediumCount = MediumCount+1;
      }
      else if(DurationInsec > (5*60)){
        LongCount = LongCount+1;
      }
      Misscall = consultations[i].Status==='Missed' ?  Misscall + 1 : Misscall;
    }
  }
  dashboarddetail.ChartData = [
          {
              "status": "Medium duration",
              "icon": "<i class='status-mark border-blue-300 mr-2'></i>",
              "value": MediumCount,
              "color": "#29B6F6"
          }, {
              "status": "Short duration",
              "icon": "<i class='status-mark border-success-300 mr-2'></i>",
              "value": ShortCount,
              "color": "#66BB6A"
          }, {
              "status": "Long duration",
              "icon": "<i class='status-mark border-danger-300 mr-2'></i>",
              "value": LongCount,
              "color": "#EF5350"
          }
      ];
      totalcount =  ShortCount+MediumCount+LongCount;
      dashboarddetail.totalcount =totalcount;
      if(totalcount > 0)
      {
        second = second/totalcount;
      }
      dashboarddetail.avgDuration=await convertSecondsToHHMMSS(second);
      dashboarddetail.Misscall = Misscall;
  return dashboarddetail;
}
async function convertHHMMSSToSeconds(hhmmss)
{
  let a = hhmmss.split(':'); 
  let seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
  return seconds;
}
async function convertSecondsToHHMMSS(sec)
{
  var hrs = Math.floor(sec / 3600);
      var min = Math.floor((sec - (hrs * 3600)) / 60);
      var seconds = sec - (hrs * 3600) - (min * 60);
      seconds = Math.round(seconds * 100) / 100
     
      var result = (hrs < 10 ? "0" + hrs : hrs);
      result += ":" + (min < 10 ? "0" + min : min);
      result += ":" + (seconds < 10 ? "0" + seconds : seconds);
      return result;
}
// End Naresh

/* Added by : Jaydutt Shukla
   Added on : 02/05/19 */

async function sendHoldNotification(theBox,box_id,pucode,onHold)
{
  let configurations = {};
  let customerPhone;
  const prescription_masterID = await prescriptions_master.query()
    .select('*')
    .where('box_id',box_id) 
    .skipUndefined()
    .where ('is_deleted',false)
    .where('pucode',pucode) 
    .eager("[items]");  

     const customerDetail = await customer.query()
     .select('*')
     .where('id',prescription_masterID[0].customer_id)
     .skipUndefined()
     .andWhere('is_deleted', false)
     .andWhere('is_enabled', true)
     .eager("[contacts]")

     customerPhone = customerDetail[0].contacts[0].phone ? customerDetail[0].contacts[0].phone :'';

      if (theBox) {
        //configurations = await dataForEmail(theBox,customerDetail,prescription_masterID,onHold);
         configurations = gatherEmailConfig(theBox,customerDetail,prescription_masterID,onHold);
       
         let s3response = { Location: '' };
         configurations.qrCode = await QRCode.toDataURL(prescription_masterID[0].pucode);
         const type = configurations.qrCode.split(';')[0].split('/')[1];
         const base64Data = new Buffer(
          configurations.qrCode.replace(/^data:image\/\w+;base64,/, ''),
            'base64'
         );
         var params = {
           Bucket: AWSconfig.bucket, // Bucket name
           Key: uuidv4(), // type is not required
           Body: base64Data,
           ACL: 'public-read',
           ContentEncoding: 'base64', // required
           ContentType: `image/${type}` // required. Notice the back ticks
         };

        if (customerDetail[0].sms_notification === true) {
          /* SMS Info starts */
          const PhoneNumber = '+1' + ReplacePhone(customerPhone);
          const Message = onHold ?
            `Your prescription has been placed on hold by the pharmacy. Please contact us directly at ${configurations.pharmAddress.companyContact} to discuss your prescription.
         Pickup Code : ${configurations.pickupcode}
         Thank you.`
            : `Your prescription has been Released by the pharmacy.
         Pickup Code: ${configurations.pickupcode}`;
          sendAWSSMS(PhoneNumber, Message);
          /* SMS Info ends */
        }
         
         s3response = await s3.upload(params).promise();
         configurations.qrCode = await s3response.Location;
         if(customerDetail[0].email_notification === true) {
          sendAWSEmailHoldStatus(configurations);
         }
         return configurations;
        }
}
//End JD

// Added by Naresh | Date:15-05-2019 | DI-87

async function sendPickUpReminderNotification(accountDetail,boxes,priscriptios)
{
  const customerDetail = await customer.query()
  .where('id',priscriptios.customer_id)
  .skipUndefined()
  .andWhere('is_deleted', false)
  .andWhere('is_enabled', true)
  .eager("[contacts]")

const pickupConfig = {
  firstName: customerDetail[0].first_name,
  lastName:  customerDetail[0].last_name,
  emailId: customerDetail[0].contacts && customerDetail[0].contacts[0].email,
  companyLogo: '',
  companyName: '',
  companyContact: '',
  adressLine1: '',
  adressLine2: '',
  city: '',
  state: '',
  zipCode: '',
  orderHtml: '',
  orderTotalHtml: '',
  orderLocation: '',
  boxName: boxes.name,
  boxAddress: boxes.address ?
            (boxes.address.address_line_1+' '
            + boxes.address.address_line_2+' '
            + boxes.address.city+', '
            + boxes.address.state+' '
            + boxes.address.zip):'',
  pharmAddress: {
    picture_url:accountDetail.picture_url ? accountDetail.picture_url : 'https://s3.amazonaws.com/rpx-dev-ext-images-qr/NoImageFound.png',
    companyName:accountDetail.name,
    adressLine1:accountDetail.contact && accountDetail.contact.address && accountDetail.contact.address.address_line_1 ? accountDetail.contact.address.address_line_1:'', 
    adressLine2:accountDetail.contact && accountDetail.contact.address && accountDetail.contact.address.address_line_2 ? accountDetail.contact.address.address_line_2:'',
    city:accountDetail.contact && accountDetail.contact.address && accountDetail.contact.address.city ? accountDetail.contact.address.city:'',
    state:accountDetail.contact && accountDetail.contact.address && accountDetail.contact.address.state ? accountDetail.contact.address.state:'',
    zipcode:accountDetail.contact && accountDetail.contact.address && accountDetail.contact.address.zip ? accountDetail.contact.address.zip:'',
    companyContact:accountDetail.contact && accountDetail.contact.phone ? accountDetail.contact.phone : ''
  },
  qrCode: '',
  pickupcode: priscriptios.pucode,
  isDisplay: 'display',
  orders: [
    {
      orderId: '',
      pickupCode: '',
      items: []
    }
  ]
};

if(accountDetail.contact.email !== null && accountDetail.contact.email!== undefined && accountDetail.contact.email !== "")
  {
    let reminder_date=new Date().toUTCString();
    let sys_date = new Date().toUTCString();
    let parseSysDate = new Date(sys_date);
    let pick_up_reminder_day = accountDetail.pick_up_reminder_day !== null ? parseInt(accountDetail.pick_up_reminder_day) :1;
    if(pick_up_reminder_day !== -1)
    {
      if(priscriptios.pick_up_reminder_date !== null)
      {
        reminder_date =priscriptios.pick_up_reminder_date;
        let time_diff = parseSysDate.getTime() - priscriptios.pick_up_reminder_date.getTime();
        let minutes = Math.ceil(time_diff / (1000 * 60));
        let parsepick_up_reminder_date = priscriptios.pick_up_reminder_date;
        if(pick_up_reminder_day === 1 && minutes >= 0)
        {
          reminder_date.setDate(reminder_date.getDate() + pick_up_reminder_day);
          const pmsd = await prescriptions_master.query()
          .update({pick_up_reminder_date:reminder_date})
          .where('id',priscriptios.id);

          let s3response = { Location: '' };
          pickupConfig.qrCode = await QRCode.toDataURL(priscriptios.pucode);
          const type = pickupConfig.qrCode.split(';')[0].split('/')[1];
          const base64Data = new Buffer(
            pickupConfig.qrCode.replace(/^data:image\/\w+;base64,/, ''),
            'base64'
          );
          var params = {
            Bucket: AWSconfig.bucket, // Bucket name
            Key: uuidv4(), // type is not required
            Body: base64Data,
            ACL: 'public-read',
            ContentEncoding: 'base64', // required
            ContentType: `image/${type}` // required. Notice the back ticks
          };
          s3response = await s3.upload(params).promise();
          pickupConfig.qrCode = s3response.Location;
          for (let j = 0; j < priscriptios.items.length; j++) {
            let orderItemItems = {};
            orderItemItems.rxNo = priscriptios.items[j].rx_no;
            orderItemItems.qty = priscriptios.items[j].qty;
            orderItemItems.price = priscriptios.items[j].price;
            pickupConfig.orders[0].items.push(orderItemItems);
            orderItemItems = null;
          }
         sendAWSEmailpickUpReminder(pickupConfig);
        }
        else{
          if(parsepick_up_reminder_date.setHours(0,0,0,0) === parseSysDate.setHours(0,0,0,0))
          {
              reminder_date.setDate(reminder_date.getDate() + pick_up_reminder_day);
              const pmsd = await prescriptions_master.query()
              .update({pick_up_reminder_date:reminder_date})
              .where('id',priscriptios.id);
  
              let s3response = { Location: '' };
              pickupConfig.qrCode = await QRCode.toDataURL(priscriptios.pucode);
              const type = pickupConfig.qrCode.split(';')[0].split('/')[1];
              const base64Data = new Buffer(
                pickupConfig.qrCode.replace(/^data:image\/\w+;base64,/, ''),
                'base64'
              );
              var params = {
                Bucket: AWSconfig.bucket, // Bucket name
                Key: uuidv4(), // type is not required
                Body: base64Data,
                ACL: 'public-read',
                ContentEncoding: 'base64', // required
                ContentType: `image/${type}` // required. Notice the back ticks
              };
              s3response = await s3.upload(params).promise();
              pickupConfig.qrCode = s3response.Location;
              for (let j = 0; j < priscriptios.items.length; j++) {
                let orderItemItems = {};
                orderItemItems.rxNo = priscriptios.items[j].rx_no;
                orderItemItems.qty = priscriptios.items[j].qty;
                orderItemItems.price = priscriptios.items[j].price;
                pickupConfig.orders[0].items.push(orderItemItems);
                orderItemItems = null;
              }
             sendAWSEmailpickUpReminder(pickupConfig);
          }
        }
      }
    }
  }
} 
// End DI-87
async function ExpansionOrdering(box_expansions){
  box_expansions =JSON.parse(box_expansions);
  let UnitLayout=[];
  let L=[];
  if(box_expansions.length > 0)
  {
    let ExpIndex=0;
    let LeftSide =box_expansions.filter(x=>x.direction === 1);
    if(LeftSide.length > 0)
    {
        let boxlist=[];
        boxlist = LeftSide.filter(x=>x.connected_box_id === BoxTypeEnum.MAIN_46);
        boxlist[0].ExpIndex=ExpIndex-1;
         ExpIndex = ExpIndex -1;
         UnitLayout.push(boxlist[0]);
         if(UnitLayout.length >0)
         {
            LeftSide.map(l=>{
                 LeftSide.map(u=>{
                   if(l.expansion_id === u.connected_box_id)
                   {
                    let IndexE = UnitLayout.findIndex(x=>x.id === u.id);
                    if(IndexE === -1)
                    {
                      let list=[];
                      list = u;
                      list.ExpIndex=ExpIndex-1;
                       ExpIndex = ExpIndex -1;
                       UnitLayout.push(list);
                    }
                   }
                 });
            });
         }
    }
    let RightSide =box_expansions.filter(x=>x.direction === 2);
    if(RightSide.length > 0)
    {
        ExpIndex = 0;
        let boxlist=[];
        boxlist = RightSide.filter(x=>x.connected_box_id === BoxTypeEnum.MAIN_46);
        boxlist[0].ExpIndex=ExpIndex+1;
         ExpIndex = ExpIndex +1;
         UnitLayout.push(boxlist[0]);
         if(UnitLayout.length >0)
         {
          RightSide.map(l=>{
            RightSide.map(u=>{
                   if(l.expansion_id === u.connected_box_id)
                   {
                    let IndexE = UnitLayout.findIndex(x=>x.id === u.id);
                    if(IndexE === -1)
                    {
                      let list=[];
                      list = u;
                      list.ExpIndex=ExpIndex+1;
                       ExpIndex = ExpIndex +1;
                       UnitLayout.push(list);
                    }
                   }
                 });
            });
         }
    }
  }
  return UnitLayout;
}

router.post("/authUser", async (req, res, next) => {
  let username = req.body.username;
  let box_id = req.body.box_id;
  console.log(`Getting detais like username is ${username} and box_id is ${box_id}`);
  let objUser = { 
    isValidUser: false
    // token: ''
  };
  
  const u = await user.query().where('username', username).eager('[parentAccount]').first();
  if(u!==null && u!==undefined){
    console.log(`Getting user details for username ${username} with details ${JSON.stringify(u)}`); 
    if(box_id==='' || box_id==='null' || box_id===null || box_id===undefined || box_id==='undefined'){
      objUser.isValidUser = true;
    }else{
      let boxes = await box.query().where('kiosk_box_id',box_id).eager('[account]').first();

      if(boxes!==null && boxes!==undefined){
        console.log(`Boxes Details ${ JSON.stringify(boxes) }`);
        console.log(`Parent Account id of box is ${boxes.parent_account_id}`);
        console.log(`Parent Account id of user is ${u.parent_account_id}`);
        if(u.parentAccount.parent_account_id===null || u.parentAccount.parent_account_id==='null'){
          objUser.isValidUser = true;
        }        
        else if(boxes.account.parent_account_id === u.parent_account_id){
          objUser.isValidUser = true;
        }else if(boxes.account.id === u.parent_account_id){
          objUser.isValidUser = true;
        }else{
          objUser.isValidUser = false;
        }      
      }else{
        objUser.isValidUser = false;
      }
    }    
    // const payload = {
    //   username: username
    // }
    // const token = signAuth(payload);
    // objUser.token = token;
  }
  objUser.isValidUser = true;
  res.send(objUser);
});

router.get("/updateBoxStatus/:id", async (req, res, next) => {
  let status = false;
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){    
    try{
      let boxes = await box.query().where('kiosk_box_id',req.params.id).eager('[account]').first();
      console.log(`Get Box details box id ${req.params.id} is ${JSON.stringify(boxes)}`);
      if(boxes!==null){
        const acc = await account.query()
        .update({is_user_logged_in: false}).where('id', boxes.parent_account_id);
        status = true          
      }              
    }
    catch(error){
      status = false;
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }                            
    res.send(status);
  }
});

/* Added by : Jaydutt Shukla
   Added on : 02/05/19 */

   async function sendRemoveRxNotification(theBox,box_id,pm_id)
   {
     console.log('Coming to Remove Rx')
     let configurations = {};
     let customerPhone;
     const prescription_masterID = await prescriptions_master.query()
       .select('*')
       .where('id',pm_id) 
       .skipUndefined()
       .where ('is_deleted',true)
       .eager("[items]");
    
    console.log('Prescription Master ID : '+ pm_id)
    console.log('Prescription Master Details : '+ JSON.stringify(prescription_masterID))
        const customerDetail = await customer.query()
        .select('*')
        .where('id',prescription_masterID[0].customer_id)
        .skipUndefined()
        .andWhere('is_deleted', false)
        .andWhere('is_enabled', true)
        .eager("[contacts]")
   
        customerPhone = customerDetail[0].contacts[0].phone ? customerDetail[0].contacts[0].phone :'';
    
         if (theBox) {
           //configurations = await dataForEmail(theBox,customerDetail,prescription_masterID,onHold);
            configurations = gatherEmailConfig(theBox,customerDetail,prescription_masterID,true);
          
            let s3response = { Location: '' };
            configurations.qrCode = await QRCode.toDataURL(prescription_masterID[0].pucode);
            const type = configurations.qrCode.split(';')[0].split('/')[1];
            const base64Data = new Buffer(
             configurations.qrCode.replace(/^data:image\/\w+;base64,/, ''),
               'base64'
            );
            var params = {
              Bucket: AWSconfig.bucket, // Bucket name
              Key: uuidv4(), // type is not required
              Body: base64Data,
              ACL: 'public-read',
              ContentEncoding: 'base64', // required
              ContentType: `image/${type}` // required. Notice the back ticks
            };
            
           if (customerDetail[0].sms_notification === true) {
             /* SMS Info starts */
             const PhoneNumber = '+1' + ReplacePhone(customerPhone);
             const Message =
               `Your prescription has been removed by the pharmacy. Please contact us directly at ${configurations.pharmAddress.companyContact} to discuss your prescription.
            Pickup Code : ${configurations.pickupcode}
            Thank you.`
             sendAWSSMS(PhoneNumber, Message);
             /* SMS Info ends */
           }
            
            s3response = await s3.upload(params).promise();
            configurations.qrCode = await s3response.Location;
            configurations.isDisplay = 'REMOVE'; 
            
            console.log('Config for Rx Remove email')
           if (customerDetail[0].email_notification === true) {
             sendAWSEmailRemoveRxStatus(configurations)
           }
            return configurations;
           }
   }
   //End JD
/* Added by : Naresh Makwana 
 Created date :10-07-2019
 TaskID:DI-663
 Desc : Get prescription detail after patient login from kiosk using emailid.
 */
router.post("/getPrescriptionsForPatient", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const response = {};
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const u = await customer.query().where('username', decodedToken.email).andWhere('is_deleted', false).andWhere('is_enabled', true).eager("[parentAccount,roles,contacts,legaldocs]").first();
      response.customers=u;
      response.customers.orders=[];
      let Prescriptions=[];
      Prescriptions = await prescriptions_master.query()
                .innerJoin('boxes','boxes.id','prescriptions_master.box_id')
                .innerJoin('accounts','accounts.id','boxes.parent_account_id')
                .innerJoin('customers','customers.id','prescriptions_master.customer_id')
                .where('prescriptions_master.type_id',RXSTATUS.STOCKED)
                .where('customers.id', u.id)
                .where('accounts.id', u.parent_account_id)
                .eager('[items]')
      for(let i=0;i<Prescriptions.length;i++)
      {
        let totalPrice = 0;
        Prescriptions[i].items.length > 0 && Prescriptions[i].items.map(item=>{
          totalPrice = totalPrice + item.price; 
        });
        Prescriptions[i].totalPrice = totalPrice;
        Prescriptions[i].chkEnable = true;
        response.customers.orders.push(Prescriptions[i]);
      }
               
      console.log(JSON.stringify(response));
      res.send(response);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 

});

//Connect :Save Patient Manual Override | DI-677 | Added by Naresh
router.post("/savePatientManualOverrideDetail",upload.any(), async (req, res, next) => { 
  // let orderIds = req.body.orderIds.length > 0 ? JSON.parse(req.body.orderIds) : [];
  let order_id = req.body.order_id;
  const updateRxStatus = {
    id:order_id,
    type_id:req.body.type_id,
    pickup_date:new Date(),
  }
  const pmCount =await prescriptions_master.query().
               where('id',order_id).
               andWhere('type_id',req.body.type_id)
  if(pmCount && pmCount.length === 0)
  {
    let presMaster = await prescriptions_master.query()
    .upsertGraphAndFetch(updateRxStatus,
    {
      relate:true,
      unrelate:false
    });
    const saveObj = { 
      customer_id:req.body.customer_id,
      order_id:order_id,
      picture_url:null,
      signature_url:null, 
      drivers_license_no:req.body.drivers_license_no,
      first_name:req.body.first_name,
      middle_name:req.body.middle_name,
      last_name:req.body.last_name,
      name: `${req.body.first_name} ${req.body.last_name}`,
      dob:req.body.dob,
      relationship:req.body.relationship,
      box_id:req.body.box_id,
      account_id:req.body.account_id,
      selectedstatecode: req.body.selectedStateCode,//Added by Naresh
      dlrawdata: req.body.dlrawdata //Added by Naresh
    };
    try{
      let cusDetail,rxComplete;
      cusDetail = await customer_collect_detail.query().insertGraphAndFetch(saveObj);    
      if(cusDetail && cusDetail && EventType.MANUAL_OVERRIDE === req.body.MANUAL_OVERRIDE && req.body.event_params !== undefined)
      {

        const accounts = await account.query().where('id',req.body.account_id).eager("[contact,contact.address,connection]").first();//change by Naresh

        req.body.email_data.pharmAddress.picture_url = accounts && 
                                                        (accounts.picture_url !== '' && accounts.picture_url !== null && accounts.picture_url !== undefined && accounts.picture_url !== 'undefined') ?
                                                        accounts.picture_url:req.body.email_data.pharmAddress.picture_url;
        req.body.email_data.pharmAddress.adressLine1 = accounts && accounts.contact && accounts.contact.address.address_line_1;
        req.body.email_data.pharmAddress.adressLine2 = accounts && accounts.contact && accounts.contact.address.address_line_2;
        req.body.email_data.pharmAddress.city = accounts && accounts.contact && accounts.contact.address.citys;
        req.body.email_data.pharmAddress.state = accounts && accounts.contact && accounts.contact.address.state;
        req.body.email_data.pharmAddress.zipcode = accounts && accounts.contact && accounts.contact.address.zipcode;
        req.body.email_data.pharmAddress.companyContact = accounts && accounts.contact && accounts.contact.phone;
          const eventRes = await EventLogHandlerModule(req.body.event_params,req.body.email_data);
          console.log(JSON.stringify(eventRes));
          if(accounts && accounts.connection && accounts.connection.id === ConnectionType.PIONEERRX) //Added by Naresh
          {
            if(cusDetail && cusDetail!==undefined && cusDetail!==null){
              rxComplete = await PioneerRxComplete(saveObj);
            } 
          }
          else if(accounts && accounts.connection && accounts.connection.id === ConnectionType.EPIC) //Added by Varshit
          {
            if(cusDetail && cusDetail!==undefined && cusDetail!==null){
              rxComplete = await EpicRxComplete(saveObj);
            }

          }
      }
      res.send(
        JSON.stringify({        
           pmDetail:presMaster,
           cusDetail:cusDetail,
           rxComplete:rxComplete, //Added by Naresh
           status:'Success'
        })
      );
    }catch(error){
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    } 
  }
  else{
    res.send(
      JSON.stringify({
        message: 'Already Pick Up !!!',
        status:'Error'
      })
    )
  }
})
        
//End Naresh        
/* 
  Added by      : Jaimin Belani 
  Created date  : 08-07-2019  
  Description   : Get all end points of Third Party PHIs
*/
router.get("/thirdPartyPHIs", async (req, res, next) => {
  try{
    const tpphis = await thirdPartyPHIs.query();
    res.send(tpphis);
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack

      })
    );
  }
});



/* 
  Added by      : Jaimin Belani 
  Created date  : 08-09-2019  
  Description   : To Post Rx and Patient details from Pioneer Rx APIs
*/
async function PioneerRxComplete(patientConfiguration){
  try{    
    let patient = await customer.query().where('id', patientConfiguration.customer_id).andWhere('is_deleted', false).andWhere('is_enabled', true).first();
    let orders = await prescriptions_master.query().where('id',patientConfiguration.order_id).first();
    let order_details = await prescription_detail.query().where('parent_account_id',orders.id);
    let account_connection = await AccountConnectionDetails.query().where('account_id',patient.parent_account_id).first();
    let rxs = [];
    for(let i = 0; i < order_details.length; i++){
      const rxNumberWithFillNumber = order_details[i].rx_no;

      let rx = {
        rxNumber: rxNumberWithFillNumber.substring(0, rxNumberWithFillNumber.length - 2),
        fillNumber: parseInt(rxNumberWithFillNumber.substring(rxNumberWithFillNumber.length - 2, rxNumberWithFillNumber.length))
      }
      rxs.push(rx);
    }

    let relationTypeId;
    switch(patientConfiguration.relationship){
      case 'Patient' :
        relationTypeId = 1;
        break;
      case 'Parent_Legal_Gardian' :
        relationTypeId = 2;
        break;
      case 'Spouse' :
        relationTypeId = 3;
        break;              
      case 'Other' :
        relationTypeId = 5;
        break;  
      default :
        relationTypeId = 1;
    }

    let dlNumber = '';
    let selectedStateCode = '';
    if (patientConfiguration.drivers_license_no === null
      || patientConfiguration.drivers_license_no === 'null'
      || patientConfiguration.drivers_license_no === undefined
      || patientConfiguration.drivers_license_no === 'undefined'
      || patientConfiguration.drivers_license_no === ''
      || patientConfiguration.drivers_license_no === "") {
      dlNumber = 'iLocalBox';
    } else {
      dlNumber = patientConfiguration.drivers_license_no
    }
    if (patientConfiguration.selectedstatecode === null
      || patientConfiguration.selectedstatecode === 'null'
      || patientConfiguration.selectedstatecode === undefined
      || patientConfiguration.selectedstatecode === 'undefined'
      || patientConfiguration.selectedstatecode === ''
      || patientConfiguration.selectedstatecode === "") {
      selectedStateCode = 'CA';
    } else {
      selectedStateCode = patientConfiguration.selectedstatecode
    }
    let obj = {
      "version": 1,
      "externalPOSSaleID": orders.pucode,
      "rxs": rxs,
      "saleType": 1,    
      "patients": [{
          "patientID": patient.patient_id,
          "idTypeID": 11,
          "idValue": dlNumber,
          "passportOriginID": 0
      }],
      "pickedUpBy": {
          "relationshipTypeID": relationTypeId,        
          "firstName": patientConfiguration.first_name,
          "middleName": patientConfiguration.middle_name,
          "lastName": patientConfiguration.last_name,
          "idTypeID": 11,
          "idValue": dlNumber,   
          "idIssuingState": selectedStateCode,
          "passportOriginID": 0
      },   
      "counselingStatus": 1
    }
    

    const headers = {
      'Content-Type': 'application/json',
      'Vendor-Key': 'e54ba6b3-1d99-4da5-822e-5800fcbced58',
      'data-exchange-id': account_connection.data_exchange_id
    }

    console.log(`Getting headers for Rx Complete : ${headers}`);
    console.log(`Getting data for Rx Complete : ${obj}`)
    let RxCompleteResponse;
    let errorObjResponse;
    await axios.post( `${account_connection.endpoint}RxComplete`, obj, {
    // await axios.post( `https://enbuzowz3q4os.x.pipedream.net/RxComplete`, obj, {
      headers: headers
    }).then((response) => {
      if(response && response.data){
        RxCompleteResponse = response;
      }              
    }).catch((exception)=> {
      let errorObj = {
        Message: exception,
        created_on: new Date()
      }           
      errorObjResponse = errorObj
      console.log(errorObj);
    });
    if(errorObjResponse===null && errorObjResponse===undefined){
      if(RxCompleteResponse && RxCompleteResponse.data){
        let pioneerTrans = {
          version: RxCompleteResponse.data.version,
          SaleTransactionID: RxCompleteResponse.data.saleTransactionID,
          SaleReceiptNumber: RxCompleteResponse.data.saleReceiptNumber,
          externalPOSSaleID: obj.externalPOSSaleID,
          saleType: obj.saleType,
          counselingStatus: obj.counselingStatus,
          created_on: new Date(),
        }
        console.log(`Getting details for Pioneer Rx Sales Transaction : ${JSON.stringify(pioneerTrans)}`);
        const _pioneerTrans = await _pioneerSalesTrans.query().insertGraphAndFetch(pioneerTrans);
        console.log(`Getting details after saving Pioneer Rx Sales Transaction : ${JSON.stringify(_pioneerTrans)}`);
        if(_pioneerTrans){
          let pioneerTransDetails = {            
            patientID: obj.patients[0].patientID,
            idTypeID : obj.patients[0].idTypeID,
            idValue : obj.patients[0].idValue,
            passportOriginID : obj.patients[0].passportOriginID,
            pickedUprelationshipTypeID : obj.pickedUpBy.relationshipTypeID,
            pickedUpFirstName: obj.pickedUpBy.firstName,
            pickedUpMiddleName: obj.pickedUpBy.middleName,
            pickedUpLastName: obj.pickedUpBy.lastName,
            pickedUpByIdTypeID: obj.pickedUpBy.idTypeID,
            pickedUpByIdValue: obj.pickedUpBy.idValue,
            pickedUpIdIssuingState: obj.pickedUpBy.idIssuingState,
            created_on: new Date(),
          }          
          try{
            console.log(`Getting details for Pioneer Rx Sales Transaction Details : ${JSON.stringify(pioneerTransDetails)}`);
            const _pioneerTransDetails = await _pioneerSalesTransDetails.query().insertGraphAndFetch(pioneerTransDetails);
          }catch(error){
            console.log(`Getting error after saving Pioneer Rx Sales Transaction details : ${JSON.stringify(error)}`);     
          }                    
        }  
      }
    }else{
      //const _pioneerRxerror = await _pioneerSalesTransErrorLogHistory.query().insertGraphAndFetch(errorObjResponse);
      console.log(`Error at line number 6595 ${JSON.stringify(errorObjResponse)}`);
    }    
  }catch(response){
    let errorObj = {
      Message: response,
      created_on: new Date()
    }
    //const _pioneerRxerror = await _pioneerSalesTransErrorLogHistory.query().insertGraphAndFetch(errorObj);
    console.log(`Error in line number 6577 is ${JSON.stringify(response)}`);
  }
}

/*
  Added By     : Varshit Shah
  Created Date : 13-12-2019
  Description  : To Post Rx and Patient details from Epic APIs
*/
const isColumnValueUndefined = (columnName, dataType) => {
  let _columnValue;
  if (columnName === undefined || columnName === 'undefined' || columnName === null || columnName === 'null' || columnName === '') {
      if (dataType.toLowerCase() === 'string') {
          _columnValue = '';
      } else if (dataType.toLowerCase() === 'number') {
          _columnValue = 0;
      } else if (dataType.toLowerCase() === 'boolean') {
          _columnValue = false;
      } else {
          _columnValue = '';
      }
  } else {
      _columnValue = columnName;
  }
  return _columnValue;
}

function pad(number) {
  if (number < 10) {
  return '0' + number;
  }
  return number;
}

function getDate(dateStr) {
  let date = new Date(dateStr);
    return date.getFullYear() +
    '-' + pad(date.getMonth() + 1) +
    '-' + pad(date.getDate()) +
    'T' + pad(date.getHours()) +
    ':' + pad(date.getMinutes()) +
    ':' + pad(date.getSeconds());			
};

async function EpicRxComplete(patientConfiguration){
  try {
  
    let patient = await customer.query().where('id', patientConfiguration.customer_id).andWhere('is_deleted', false).andWhere('is_enabled', true).first();
    let orders = await prescriptions_master.query().where('id',patientConfiguration.order_id).first();
    let order_details = await prescription_detail.query().where('parent_account_id',orders.id);
    let account_connection = await AccountConnectionDetails.query().where('account_id',patient.parent_account_id).first();
    const _counselingInfo = await user_consult_master.query().where('pickup_code', orders.pucode).andWhere('Status', 'in' ,[Enum_CounselingStatus.DD || Enum_CounselingStatus.DE || Enum_CounselingStatus.MD]).first();
    const paymentModels = await paymentModel.query().where('customer_id', patientConfiguration.customer_id).andWhere('order_id', patientConfiguration.order_id).andWhere('is_paid', true).first()
    const custoerDLDetails = await customer_collect_detail.query().where('customer_id', patientConfiguration.customer_id).first();
    let rxs = [];
    let EpicResponse = [];
    for(let i = 0; i < order_details.length; i++){
      let rx = {
        rxNumber:order_details[i].rx_no,
        fillNumber: order_details[i].fillnumber
      }
      rxs.push(rx);
    }    
    for (let i=0; i < rxs.length; i++) {
      const url = account_connection.endpoint;
      let accountId = patientConfiguration.account_id;
      const template = await Templates.query().where('account_id', accountId).first();
      console.log(`DispenseFills: Template for SOAP request : ${JSON.stringify(template)}`);
      const requestHeaders = {
        'Content-Type': 'text/xml',
        SOAPAction: `Epic.Clinical.Pharmacy.WebServices${template.versions}.DispenseFills`,
        'Epic-Client-ID': account_connection.epic_client_id
      };
      console.log(`Epic : Dispense Fills request headers : ${requestHeaders} `);
      let counselingInfos;
      if(_counselingInfo && _counselingInfo !== null) {
        let _counselingStatus = (_counselingInfo.Status === Enum_CounselingStatus.DD || _counselingInfo.Status === Enum_CounselingStatus.DE || _counselingInfo.Status === Enum_CounselingStatus.MD) ? 'Counseled' : '';
        counselingInfos = ` <CounselingInfo>
                              <CounselingCompletionInstant>${getDate(_counselingInfo.consult_end_date) || null}</CounselingCompletionInstant>
                              <CounselingNotes>${_counselingInfo.consultation_notes || ''}</CounselingNotes>
                              <CounselingStatus>${_counselingStatus}</CounselingStatus>
                            </CounselingInfo>`
      } else {
        counselingInfos = `<CounselingInfo />`;
      }

      console.log(`Epic : Dispense Fills CounselingInfo : ${counselingInfos}`)

      let paymentInfos;
      if(paymentModels && paymentModels !== null) {
        paymentInfos = `<Payments>
                          <Payment>
                              <AdjustmentType/>
                              <Amount>${paymentModels && paymentModels.approved_amt}</Amount>
                              <CheckNumber></CheckNumber>
                              <Comments></Comments>
                              <CreditCardAuthorizationNumber>${paymentModels.approved_code}</CreditCardAuthorizationNumber>
                              <CreditCardCardholderName>${paymentModels.card_holder_name}</CreditCardCardholderName>
                              <CreditCardExpMonth>${paymentModels.exp_month}</CreditCardExpMonth>
                              <CreditCardExpYear>${paymentModels.exp_year}</CreditCardExpYear>
                              <CreditCardLastFourDigits>1004</CreditCardLastFourDigits>
                              <CreditCardTransactionId>${paymentModels.trans_id}</CreditCardTransactionId>
                              <CustomFields/>
                              <PaymentCompletedDateTime>${getDate(paymentModels.payment_date)}</PaymentCompletedDateTime>
                              <PaymentTenderType>${paymentModels.card_type}</PaymentTenderType>
                          </Payment>
                      </Payments>`
      } else {
        paymentInfos = `<Payments />`
      }
      console.log(`Epic : Dispense Fills Payments : ${paymentInfos}`)
      let CustomerIdType;
      if (custoerDLDetails.dlrawdata === null || custoerDLDetails.dlrawdata === 'null' || custoerDLDetails.dlrawdata === undefined || custoerDLDetails.dlrawdata === 'undefined' || custoerDLDetails.dlrawdata === '') {
        if (custoerDLDetails.drivers_license_no === null || custoerDLDetails.drivers_license_no === 'null' || custoerDLDetails.drivers_license_no === undefined || custoerDLDetails.drivers_license_no === 'undefined' || custoerDLDetails.drivers_license_no === '') {
          CustomerIdType = 'NA';
        } else {
          CustomerIdType = 'ML';
        }
      } else {
        CustomerIdType = 'DL';
      }
      let objToReplace = {
        username: account_connection.epic_username,
        pasword: account_connection.epic_password,
        payload: `<DispenseFills xmlns="Epic.Clinical.Pharmacy.WebServices2018">
                    <systemId>EPIC</systemId>
                    <requests xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                        <DispenseFillRequest>                    
                          ${counselingInfos}
                          <CustomFields />
                          <CustomerIdNumber>${custoerDLDetails.drivers_license_no || 'NA'}</CustomerIdNumber>
                          <CustomerIdType>${CustomerIdType}</CustomerIdType>
                          <CustomerIdFullName>${patient.first_name + ' ' + patient.last_name}</CustomerIdFullName>
                          <CustomerIdRelationshipToPatient>Self</CustomerIdRelationshipToPatient>
                          <DispensedDateTime>${getDate(orders.pickup_date)}</DispensedDateTime>
                          <FillId>${rxs[i].fillNumber}</FillId>
                          ${paymentInfos}
                          <PrescriptionId>${rxs[i].rxNumber}</PrescriptionId>
                        </DispenseFillRequest>
                    </requests>
                    <userId />
                    <userIdType />
                    <workstationId />
                </DispenseFills>`
      }
      console.log(`Epic : Dispense Fills Object to Replace with header teamplate : ${JSON.stringify(objToReplace, null, 2)}`);
      let parentTemplate = template.request_template;
        for (var prop in objToReplace) {
          if (objToReplace.hasOwnProperty(prop)) {
              parentTemplate = parentTemplate.replace(new RegExp('{{' + prop + '}}', "g"), objToReplace[prop]);
          }
      }
      console.log(`Epic: Dispense Fills Requst body : ${parentTemplate}`);
      let requestbody  =  parentTemplate;
      const { response } = await soapRequest({ url: url, headers: requestHeaders, xml: requestbody });
      const { headers, body, statusCode } = response;      
      let convert = require('xml-js');
      let responsedata = convert.xml2js(response.body, { compact: true });
      console.log("DispenseJSONData",JSON.stringify(responsedata));
      let parsedResponseData = responsedata['s:Envelope']['s:Body'];
      let parsedUpdatePrescriptionResultData =  responsedata['s:Envelope']['s:Body'].DispenseFillsResponse.DispenseFillsResult.UpdatePrescriptionResults.UpdatePrescriptionResult;
      console.log("dataresponse",parsedUpdatePrescriptionResultData)
      let updateprescriptionresult = {
        ErrorCode : isColumnValueUndefined(parsedUpdatePrescriptionResultData.ErrorCode['_text'], 'string'),
        ErrorMessage :  isColumnValueUndefined(parsedUpdatePrescriptionResultData.ErrorMessage['_text'], 'string'),
        FillId : isColumnValueUndefined(parsedUpdatePrescriptionResultData.FillId['_text'], 'string'),
        PrescriptionId : isColumnValueUndefined(parsedUpdatePrescriptionResultData.PrescriptionId['_text'], 'string'),
        UpdateTimestamp : isColumnValueUndefined(parsedUpdatePrescriptionResultData.UpdateTimestamp['_attributes']['i:nil'],'boolean'),
        WasUpdated : isColumnValueUndefined(parsedUpdatePrescriptionResultData.WasUpdated['_text'],'boolean'),
      } 
      EpicResponse.push(updateprescriptionresult) 
    }
    if(EpicResponse.length > 0) {
      for (let i=0; i < EpicResponse.length; i++){
        let obj = {
          customer_id : patient.id,
          phi_name : "EpicWillow",
          error_code : EpicResponse[i].ErrorCode,
          error_message : EpicResponse[i].ErrorMessage,
          fill_id : EpicResponse[i].FillId,
          prescription_id : EpicResponse[i].PrescriptionId,
          was_updated : EpicResponse[i].WasUpdated
        }
        const _PhiResponse = await _phisresponses.query().insertGraphAndFetch(obj);
        console.log("Epic Willow Response Saved" ,JSON.stringify(_PhiResponse));
      }
    }
  }catch(response){
    console.log(`Epic: Error while processing request ${JSON.stringify(response)}`);
    let errorObj = {
      Message: response,
      created_on: new Date()
    }
  }
}

router.get("/getBoxesByPatient", async (req, res, next) => {
  const response = {
    patient: {},    
    boxes: [],
    successStatus: {
      message : '',
      status : 200
    },
    errorResponse: {
      errorMessage:'',
      errorStatus : ''
    }
  };  
  try{    
    if (!req.headers.authorization) {
      response.errorResponse.errorMessage = 'Unauthorized Access!';
      response.errorResponse.errorStatus = 401;
      res.send(response);
    }else{      
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);            
      console.log(decodedToken.email);
      const u = await customer.query().where('username', decodedToken.email).andWhere('is_deleted', false).andWhere('is_enabled', true).eager('[parentAccount]').first();      
      console.log(JSON.stringify(u));
      if(u!==null && u!==undefined){
        const boxes = await box.query().where('parent_account_id', u.parentAccount.id).where('IsActive',true).eager("[address, type]");
        response.patient = u;      
        response.boxes = boxes;
        response.successStatus.message = 'Patient details with boxes!'
        response.successStatus.status = 200;
      }else{
        response.errorResponse.errorMessage = 'User not found!';
        response.errorResponse.errorStatus = 404
      }            
    }            
    res.send(response);
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
})

// Added By:  Varshit shah
// Date: 20/08/2019
// Task: DI-722
// Purpose: Save Enroll for Begin Quizz Test
router.post("/saveusertoquiz", async (req, res, next) => {
  const EnrollObject = {
    firstname:req.body.first_name,
    lastname:req.body.last_name,
    dob:req.body.dob,
    mobileno:req.body.phone,
    email: req.body.email
  }
  try{
    const ucas = await QuizEnroll.query().insertGraphAndFetch(EnrollObject);
    res.send(
      JSON.stringify({
        QuizEnroll : ucas
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
// Added by:Naresh Makwana
// Date: 27-08-2019
// Task ID:DI-742
// Desc :ADA: Patient Profile
router.post("/getADACompatiblePatients", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();

    const accountIds = await account.query()
    .select('id')
    .where('account_lineage', '<@', u.parentAccount.account_lineage);

    const ADAcustomers = await customer.query()
    .innerJoin("customer_contacts","customer_contacts.customer_id","customers.id")
    .innerJoin("contacts","customer_contacts.contact_id","contacts.id")
    .where('parent_account_id', 'in', accountIds.map(x => x.id))
    .andWhere('contacts.email',req.body.EmailID)
    .andWhere('customers.ada_compitible',true)
    .andWhere('customers.is_deleted', false)
    .andWhere('customers.is_enabled', true)
    .andWhere(
      function(){
        if(req.body.PatientID!== "" && req.body.PatientID !== null && req.body.PatientID !== undefined && req.body.PatientID !== 'null' && req.body.PatientID !== 'undefined')
        {
          //this.where('customers.patient_id',req.body.PatientID)
        }
      }
    )
    .select(
      'customers.id'
      ,'customers.username'
      ,'customers.ada_compitible'
      ,'customers.box_id'
      ,'customers.patient_id'
    );

    const BoxId= await getPreferredBoxForAssignPatient(accountIds,req.body.PatientID,req.body.EmailID);
    res.send(
      JSON.stringify({
        ADAcustomers : ADAcustomers,
        BoxId : BoxId
      })
    );
    // .eager("[roles, contacts]");
    // res.send(ADAcustomers);
    }
  });

// Added by:Naresh Makwana
// Date: 03-10-2019
// Task ID:DI-869
// Desc :Assign patient is not checking for "preferred box" under patient preferences  
  async function  getPreferredBoxForAssignPatient(accountIds,PatientID,EmailID)
  {
    const customerBoxId = await customer.query()
    .innerJoin("customer_contacts","customer_contacts.customer_id","customers.id")
    .innerJoin("contacts","customer_contacts.contact_id","contacts.id")
    .where('parent_account_id', 'in', accountIds.map(x => x.id))
    .andWhere('contacts.email',EmailID)
    .andWhere('customers.is_deleted', false)
    .andWhere('customers.is_enabled', true)
    .andWhere(
      function(){
        if(PatientID!== "" && PatientID !== null && PatientID !== undefined && PatientID !== 'null' && PatientID !== 'undefined')
        {
          this.where('customers.patient_id',PatientID)
        }
      }
    )
    .select(
      'customers.box_id'
    ).first();
    return customerBoxId;
  }
// Added by:Naresh Makwana
// Date: 06-09-2019
// Task ID:DI-742
// Desc :copy dob to dob_str in customer table
  router.get("/swapCustomerDOB", async (req, res, next) => {
    try{
     let updateDOB= await customer.query().update({
        dob_str:customer.raw("to_char(customers.dob,'MM/DD/YYYY')")
     });
      res.send('success');
    }catch(error){
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
  
        })
      );
    }
  });

// Added By:  Varshit shah
// Date: 10/02/2019
// Task: DI-841
// Purpose: Save POS Transaction
router.post("/saveTransaction", async (req, res, next) => {
  let TransactionDetail = [];
  const TransactionObject = {
    rawdata: req.body.rawdata,
    transactiondatetime:req.body.transactiondatetime,
    transactionevent:req.body.transactionevent,
    transactionamount:req.body.transactionamount,
    transactionID: req.body.transactionID,
    transactionapproved: req.body.transactionapproved
  }
  TransactionDetail.push(req.body.transactions);
  try{
    const Trans = await POSTransaction.query().insertGraphAndFetch(TransactionObject);
    
    for(let i=0 ; i<TransactionDetail.length ; i++){
      const tranDetailObj = {
        description: TransactionDetail[i].description,
        transactiondetailid:TransactionDetail[i].id,
        price: TransactionDetail[i].price,
        qty: TransactionDetail[i].qty,
        transactionID: Trans.id
      }
      const tran_detail = await POSTransactionDetail.query().insertGraphAndFetch(tranDetailObj);
    }
    res.send(
      JSON.stringify({
        POSTransaction : Trans
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
}); 

router.post('/CheckAccountAvailability', async (req, res, next) => {
  let response = {
    IsAvailable : false,
    ErrorMessage: '',
    StatusCode : null
  }

  try {
    let accountName = req.body.accountName;
    const isAccountAvailable = await account.query().where('name', accountName).first();
    if(isAccountAvailable !==null && isAccountAvailable !== undefined){
      response.IsAvailable = false;
      response.StatusCode = 200;
    }else{
      response.IsAvailable = true;
      response.StatusCode = 200;
    }
  } catch(error) {
    response.IsAvailable = false;
    response.ErrorMessage = error.message;
    response.StatusCode = 500;
  }
  res.send(response);
})

router.post("/openDoorTransaction", async (req, res, next) => {  
  const binId = req.body.binId;
  const boxId = req.body.boxId;
  const typeId =  req.body.typeId;
  const boxParentAccountId = req.body.boxParentAccountId;
  const eventCrudType = req.body.eventCrudType;
  const eventName = req.body.eventName;
  try { 
    const _prescriptionsMaster = await prescriptions_master.query()
    .innerJoin("prescription_detail","prescription_detail.parent_account_id","prescriptions_master.id")
    .innerJoin("customers","prescriptions_master.customer_id","customers.id")
    .innerJoin("boxes","prescriptions_master.box_id","boxes.id")
    .innerJoin("accounts","boxes.parent_account_id","accounts.id")
    .innerJoin("users","users.parent_account_id","accounts.id")
    .where('prescriptions_master.box_id', boxId)
    .andWhere('prescriptions_master.bin_id', binId)
    .andWhere('prescriptions_master.type_id', typeId)
    .select(
      'users.name as username', 
      'users.id as userid', 
      'customers.first_name', 
      'customers.last_name', 
      'prescriptions_master.id as orderId', 
      'prescriptions_master.box_id', 
      'prescriptions_master.type_id', 
      'prescriptions_master.customer_id', 
      'prescriptions_master.stock_code', 
      'prescription_detail.rx_no', 
      'prescriptions_master.bin_id' 
    );
    ;
    if(_prescriptionsMaster && _prescriptionsMaster.length > 0) {
      const evt_type = await event_type.query().where('name', eventName);    
      let eventParams = {
        name: eventCrudType,
        description: '',
        object_id: typeId,
        tofor_id: _prescriptionsMaster[0].orderId,
        subject_id: _prescriptionsMaster[0].userid,
        parent_account_id: boxParentAccountId,
        type_id: evt_type[0].id,
        box_id: boxId
      }      
      const events_res = await events.query().insertGraphAndFetch(eventParams);      
      res.send(
        JSON.stringify({
          _binId: binId,
          _boxId: boxId,
          _typeId: typeId,
          _prescriptionsMaster: _prescriptionsMaster,
          _events_res: events_res
        })
      );  
    } else {
      res.send(
        JSON.stringify({
          message: 'Invalid Bin'          
        })
      );
    }
      
  } catch(error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

 //Get Patient and Prescription details from Pickupcode | Added by Varshit | 13-02-2020
 router.get("/getprescriptiondetailbypucode/:id", async (req, res, next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
     const  thePrescriptiondetails = await prescriptions_master.query()
     .innerJoin('prescription_detail','prescription_detail.parent_account_id','prescriptions_master.id')
     .innerJoin('customers','prescriptions_master.customer_id','customers.id')
     .innerJoin('accounts','customers.parent_account_id','accounts.id')
     .innerJoin('contacts','contacts.id','accounts.contact_id')
     .select('customers.id',
             'customers.first_name',
             'customers.last_name',
             'customers.name',
             'customers.username as email',
             'customers.dob_str as dob',
             'prescription_detail.ndc_code',
             'prescription_detail.rx_no',
             'prescription_detail.drug_name',
             'prescription_detail.doseage_form',
             'prescription_detail.qty',
             'prescription_detail.price',
             'prescription_detail.non_proprietary_name',
             'prescription_detail.fillnumber',
             'contacts.phone')
     .where('prescriptions_master.pucode',req.params.id);

      //res.send(JSON.stringify({thePrescriptiondetails}));     
      res.send(thePrescriptiondetails);
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
 });

//Added by :Varshit | DI-930 | date:17-02-2020 | description: binding consultation report
router.post("/getConsultReport", async (req, res, next) => { 
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))

      const ConsultDetail = await user_consult_master.query()
      .innerJoin('accounts',  'accounts.id','user_consult_master.accounts_id')
      .innerJoin("boxes", 'boxes.id', 'user_consult_master.box_id')
          .andWhere(
            function()
            {
              if(req.body.store_ids.length > 0)
              {
                this.where('accounts.id', 'in', req.body.store_ids)
              }
              else{
                this.where('accounts.id', 'in', accountIds.map(x => x.id))
              }
              if(req.body.box_ids.length > 0)
              {
                this.where('boxes.id', 'in', req.body.box_ids)
              }
              else
              {
                this.where('boxes.id', 'in',boxes.map(x=>x.id))
               }
                if(req.body.fromdate !== ""){
                  this.whereRaw('??::date >= ?', ['user_consult_master.consult_start_date', req.body.fromdate])
                }
                if(req.body.todate !== ""){
                  this.whereRaw('??::date <= ?', ['user_consult_master.consult_end_date', req.body.todate])
                }
            }
          )
          .andWhere(function () {
            if(req.body.searchtext !== "")
            {
              this.orWhere('user_consult_master.user_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('user_consult_master.consultation_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('user_consult_master.Status', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('user_consult_master.consultation_notes', 'like', '%' + req.body.searchtext + '%')
              // this.orWhere('user_consult_master.is_releaseorder', 'like', '%' + req.body.searchtext + '%')
            }
          })
          .select(
              'user_consult_master.user_name as username'
            ,'user_consult_master.consultation_name as consultationname'
            ,'user_consult_master.store_name as store'
            ,'user_consult_master.kiosk_name as box'
            ,'user_consult_master.consult_start_date as consultstartdate'
            ,'user_consult_master.consult_end_date as consultenddate'
            ,'user_consult_master.call_duration as callduration'
            ,'user_consult_master.pickup_code as pickupcode'
            ,'user_consult_master.Status'
            ,'user_consult_master.consultation_notes as consultationnotes'
            ,'user_consult_master.is_releaseorder as isorderrelease'
          )
          .orderBy(
            'user_consult_master.created_on','desc'
          );
        res.send(ConsultDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//End varshit

//Update status from connect side if user release the order 
router.post("/updateReleaseOrderStatus", async (req, res, next) => {
  try{
    const ucn = await user_consult_notifications.query()
    .update({is_releaseorder: req.body.is_releaseorder})
    .where('user_consult_notification_id', req.body.user_consult_notification_id);
    res.send(
      JSON.stringify({
        user_consult_notifications : ucn
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

//Send SMS link for consult ||  TASK : DI-930
router.post("/SendMSGforConsult",async (req, res, next) => {
  try{
     /* SMS Info starts */
     const PhoneNumber = '+1' + ReplacePhone(req.body.phonenumber);
     const Message = `Hello ${req.body.patientfistname}, We received your request for consult. Please call the pharmacy at ${req.body.PharmacyPhonenumber} during our normal business hours of ${req.body.StoreOpenTime} to ${req.body.StoreCloseTime}. We look forward to hearing from you.`
     sendAWSSMS(PhoneNumber,Message);
     /* SMS Info ends */
    res.send(
      {
        msg: "Message send successfully"
      }
    )
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

async function AdvanceFillStatus(params) {
  let url = params.endPoint;
  let clientId = params.epicclientid;
  let userName = params.userName;
  let passwd = params.passwd;
  let status = params.status;
  let fillId = params.fillNumber;
  let prescriptionId = params.prescriptionId;
  let accountId = params.accountId;
  let template = await Templates.query().where('account_id', accountId).first();
  console.log(`AdvanceFillStatus: Template for SOAP headers : ${JSON.stringify(template)}`);
  const sampleHeaders = {
    'Content-Type': 'text/xml',
    SOAPAction: `Epic.Clinical.Pharmacy.WebServices${template.versions}.AdvanceFillStatus`,
    'Epic-Client-ID': clientId
  };
  console.log(`Epic: Request headers for AdvanceFillStatus : ${JSON.stringify(sampleHeaders, null, 2)}`);
  const objToReplace = {
    username: userName,
    pasword: passwd,
    payload: `<AdvanceFillStatus xmlns="Epic.Clinical.Pharmacy.WebServices2018">
                <systemId>EPIC</systemId>
                <requests xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                  <AdvanceFillStatusRequest>
                    <FillId>${fillId}</FillId>
                    <FillIdentifierType i:nil="false"></FillIdentifierType>
                    <PrescriptionId>${prescriptionId}</PrescriptionId>
                    <Status>${status}</Status>
                  </AdvanceFillStatusRequest>
                </requests>
                <pharmacyNCPDPId></pharmacyNCPDPId>
                <userId/>
                <userIdType/>
                <workstationId/>
              </AdvanceFillStatus>`,
  }
  console.log(`Epic: Header Template for XMl Body : ${template.request_template}`);
  let parentTemplate = template.request_template;
  for (var prop in objToReplace) {
      if (objToReplace.hasOwnProperty(prop)) {
          parentTemplate = parentTemplate.replace(new RegExp('{{' + prop + '}}', "g"), objToReplace[prop]);
      }
  }
  console.log(`Epic: Advance Fill Status XML Rquest Body: ${parentTemplate}`);
  let advanceFillStatusResponse = {};
  const xmlReauest = parentTemplate; 
  try {
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml: xmlReauest });    
    console.log("Epic: Advance fill status response body : ", response.body);
    let convert = require('xml-js');
    let responsedata = convert.xml2js(response.body, { compact: true });
    console.log("Epic: Advance fill status response data : ", JSON.stringify(responsedata));
    advanceFillStatusResponse.data = responsedata;
    console.log(`Epic: Response from advance fill status api is ${JSON.stringify(advanceFillStatusResponse)}`);
  } catch(InvalidRequestException) {
    console.log(`Epic: Error while executing xml request for advance fill status with error message ${InvalidRequestException}`);
  }
  
}

router.post("/SaveManualUserConsult", async (req, res, next) => {
try{
  const manualuserConsultID = await manual_user_consult.query().select('id').where('pickup_code', req.body.pickupCode ).first();
  if(manualuserConsultID && manualuserConsultID.id) {
    const UpdatemanualuserconsultmasterObject = {
      id:manualuserConsultID.id,
      pickup_code:req.body.pickupCode,
      user_name:req.body.user_name,
      consultation_name:req.body.consultation_name,
      store_name:req.body.store_name,
      kiosk_name:req.body.kiosk_name,
      accounts_id:req.body.accounts_id,
      box_id:req.body.box_id,
      consultation_notes:req.body.consultation_notes,
      is_releaseorder:req.body.is_releaseorder
    }
    const ucas = await manual_user_consult.query().upsertGraphAndFetch(UpdatemanualuserconsultmasterObject);  
    res.send(
      JSON.stringify({
        manual_user_consult : ucas
      })
    );
  }
  else{
    const manualuserconsultmasterObject = {
      pickup_code:req.body.pickupCode,
      user_name:req.body.user_name,
      consultation_name:req.body.consultation_name,
      store_name:req.body.store_name,
      kiosk_name:req.body.kiosk_name,
      accounts_id:req.body.accounts_id,
      box_id:req.body.box_id,
      consultation_notes:req.body.consultation_notes,
      is_releaseorder:req.body.is_releaseorder
    }
    const ucas = await manual_user_consult.query().insertGraphAndFetch(manualuserconsultmasterObject);
    res.send(
      JSON.stringify({
        manual_user_consult : ucas
      })
    );
  }     
}catch(error){
  res.send(
    JSON.stringify({
      message: error.message,
      stack: error.stack
    })
  );
}
});
 //Get manual user consult details from Pickupcode | Added by Varshit | 03-03-2020
 router.get("/getmanualreleaseorderdetailbypickupcode/:id", async (req, res, next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
     const  theManualOrderReleasedetails = await user_consult_master.query()
     .select('user_consult_master.user_consult_master_id',
             'user_consult_master.user_name',
             'user_consult_master.consultation_name',
             'user_consult_master.store_name',
             'user_consult_master.kiosk_name',
             'user_consult_master.accounts_id',
             'user_consult_master.box_id',
             'user_consult_master.consultation_notes',
             'user_consult_master.is_releaseorder')
     .where('user_consult_master.pickup_code',req.params.id)
     .andWhere('Status','Offline');

      //res.send(JSON.stringify({thePrescriptiondetails}));     
      res.send(theManualOrderReleasedetails);
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
 });

// Get Patient History Detail for Patient History Tab
router.post("/getPatientHistoryByPatientID/:id", async (req, res, next) => {
  try
  {
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
      const TransactionDetail = await events.query()
          .innerJoin('prescriptions_master', 'prescriptions_master.id', 'events.tofor_id')  
          .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
          .innerJoin('accounts',  'accounts.id','customers.parent_account_id')
          .innerJoin("boxes", 'boxes.id', 'prescriptions_master.box_id')
          .innerJoin("prescription_detail", 'prescription_detail.parent_account_id', 'prescriptions_master.id')
          .leftJoin('customer_collect_detail','customer_collect_detail.order_id','prescriptions_master.id')
          .innerJoin('event_type', 'event_type.id', 'events.type_id')
          .where('prescriptions_master.type_id',RXSTATUS.PICKED_UP)
          .where('event_type.name','in', [EventType.PICKED_UP,EventType.MANUAL_OVERRIDE])
          .where('prescriptions_master.customer_id',req.params.id)
          .andWhere(
            function()
            {
              if(req.body.store_ids.length > 0)
              {
                this.where('accounts.id', 'in', req.body.store_ids)
              }
              else{
                this.where('accounts.id', 'in', accountIds.map(x => x.id))
              }
              if(req.body.box_ids.length > 0)
              {
                this.where('boxes.id', 'in', req.body.box_ids)
              }
              else
              {
                this.where('boxes.id', 'in',boxes.map(x=>x.id))
               }
                if(req.body.fromdate !== ""){
                  this.whereRaw('??::date >= ?', ['prescriptions_master.pickup_date', req.body.fromdate])
                }
                if(req.body.todate !== ""){
                  this.whereRaw('??::date <= ?', ['prescriptions_master.pickup_date', req.body.todate])
                }
            }
          )
          .andWhere(function () {
            if(req.body.searchtext !== "")
            {
              this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
            }
          })
          .select('accounts.id', 'customers.first_name as firstname', 'customers.last_name as lastname', 'accounts.name as store', 'boxes.id as boxid', 'boxes.name as box'
            , 'prescriptions_master.bin_id as binid', 'customers.picture_url as pictureurl'
            , 'prescription_detail.rx_no as rxno', 'prescription_detail.ndc_code as ndccode', 'prescription_detail.drug_name as drugname', 'prescription_detail.qty as qty'
            , 'prescriptions_master.fill_date as filldate', 'prescriptions_master.stocked_date as stockeddate', 'prescriptions_master.pickup_date as pickupdate'
            , 'customers.signature_url as signatureurl'
            ,'prescriptions_master.stock_code'
            ,'prescriptions_master.pucode'
            ,'customers.name as username'
            ,'customer_collect_detail.order_id as pmid'
            ,'customer_collect_detail.first_name as pfirst_name'
            ,'customer_collect_detail.last_name as plast_name'
            ,'customer_collect_detail.drivers_license_no as pdrivers_license_no'
            ,'customer_collect_detail.dob as pdob'
            ,'customer_collect_detail.relationship as prelationship'
            ,'customer_collect_detail.picture_url as ppicture_url'
            ,'customer_collect_detail.signature_url as psignature_url'
            ,'customer_collect_detail.relationship as relationship'
            /*
              Modified By    : Belani Jaimin
              Modified Date  : 02/10/2020
              Description    : Record of declined consultations. Each pick up where the patient says "I'm Good" is a decline
            */
            ,'customer_collect_detail.is_consult_requested as isConsultRequested'
          )
          .orderBy(
            'prescriptions_master.pickup_date','desc'
          );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }  
  }); 
//Get Account ConnectionType from accountID
router.get("/getAccountConnectionTypeByaccountID/:id?", async (req, res, next) => {
  try
  {
   const token = req.headers.authorization.split(' ')[1];
   const decodedToken = jwt.decode(token);
   const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
   const accountId = req.params.id || u.parent_account_id;
   const accountConnectionsId = await AccountConnectionDetails.query().where('account_connection_details.account_id', accountId).select('account_connection_id');
 
     res.send(accountConnectionsId);
  }
  catch(error){
   res.send(
     JSON.stringify({
       message: error.message,
       stack: error.stack
     })
   );
 }
 });

 //Send Verification code for Reset Password
router.post("/SendCodeForResetPassword", async (req, res, next) => {
  try
  {
    if(req.body && req.body.email) {
      const userdetails = { 
          Username: req.body.email , 
          UserPoolId: process.env.PATIENT_POOL_ID,
          // DesiredDeliveryMediums: [
          //   //  "EMAIL"
          //   //   SMS | EMAIL,
          //   /* more items */
          // ],
          // // ForceAliasCreation: true || false,
          // MessageAction: 'SUPPRESS',
          // TemporaryPassword: generator.generate({
          //   length: 8,
          //   numbers: true,
          //   excludeSimilarCharacters: true,
          //   symbols : '!@#$%^&*()+_-=}{[]|:;"/?.,`~',
          //   //symbols: true,
          //   uppercase: true,
          //   strict: true
          // }),
          // UserAttributes: [
          //   {
          //     Name: "name" /* required */,
          //     Value: req.body.name
          //   },
          //   {
          //     Name: "email",
          //     Value: req.body.email
          //   },
          //   // {
          //   //   Name: "phone number",
          //   //   Value: "7600372820"
          //   // },
          //   {
          //     Name: "given_name",
          //     Value: "given name"
          //   },
          //   {
          //     Name: "picture",
          //     Value: "https://dev.localbox.net/static/email/header-ilocalbox-welcome.jpg"
          //   },
          //   {
          //     Name: "phone_number",
          //     Value:
          //       "+1" +
          //       req.body.phone
          //         .replace("(", "")
          //         .replace(")", "")
          //         .replace("-", "")
          //         .replace(" ", "")
          //   },
          //   {
          //     Name: "email_verified",
          //     Value: "true"
          //   },
          //   {
          //     Name: "phone_number_verified",
          //     Value: "true"
          //   }
          //   /* more items */
          // ]
       }
      const cisp = new AWS.CognitoIdentityServiceProvider();
      const cognitoUser = await cisp.adminResetUserPassword(userdetails).promise();
      
      // const accounts =  await account.query().where('id',req.body.accountID)
      // .eager("[brand, brand_detail,contact, contact.address]").first();

      // const Boxname = await  box.query().where('id',req.body.box).select('boxes.name').first();
      // const config ={
      //   emailId : req.body.email,
      //   username : req.body.email,
      //   firstName : req.body.firstname,
      //   lastName:  req.body.lastname, 
      //   TemporaryPassword: userdetails.TemporaryPassword,
      //   pharmacyName : accounts && accounts.name,
      //   pharmacyLogoUrl : accounts && accounts.brand && accounts.brand.logo_url && accounts.brand.logo_url,
      //   phoneNumber : accounts && accounts.brand && accounts.brand.phone && accounts.brand.phone,
      //   patientName : req.body.name,
      //   boxName : Boxname,
      //   accEmailId : accounts && accounts.contact && accounts.contact.email && accounts.contact.email
      //  }
      // const resEmail = sendAWSpatientResetPasswordMail(config);
      res.send(cognitoUser);
    }
 
    
  }
  catch(error){
   res.send(
     JSON.stringify({
       message: error.message,
       stack: error.stack
     })
   );
 }
 });

//TASK DI-1067- RESET PW | Added by Varshit | 31-03-2020
router.post("/UpdateResetPWDate", async (req, res, next) => {
  try {
      let userResonse;
      const USERID = await user.query().select('id').where('username', req.body.username).first();
      if (USERID && USERID.id) {
          const UserObject = {
              id: USERID.id,
              password_reset_on: new Date()
          }
          userResonse = await user.query().upsertGraphAndFetch(UserObject);
      }
      res.send(
          JSON.stringify({
              res: userResonse
          })
      );
  }
  catch (error) {
      res.send(
          JSON.stringify({
              message: error.message,
              stack: error.stack
          })
      );
  }
});

 //Get user reset password date | Added by Varshit | 31-03-2020
 router.post("/getresetpwdatefromuser", async (req, res, next) => {
  //if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
     const  theuserdetails = await user.query()
     .select('users.id',
             'users.password_reset_on')
     .where('users.username',req.body.username);    
      res.send(theuserdetails);
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }); 
//TASK DI-1067- RESET PW | Added by Varshit | 01-04-2020
router.post("/UpdatePatientResetPWDate", async (req, res, next) => {
  try {
      let userResonse;
      const USERID = await customer.query().select('id').where('username', req.body.username).andWhere('is_deleted', false).andWhere('is_enabled', true).first();
      if (USERID && USERID.id) {
          const UserObject = {
              id: USERID.id,
              password_reset_on: new Date()
          }
          userResonse = await customer.query().upsertGraphAndFetch(UserObject);
      }
      res.send(
          JSON.stringify({
              res: userResonse
          })
      );
  }
  catch (error) {
      res.send(
          JSON.stringify({
              message: error.message,
              stack: error.stack
          })
      );
  }
});
 //Get patient reset password date | Added by Varshit | 01-04-2020
 router.post("/getresetpwdatefrompatient", async (req, res, next) => {
  //if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
     const  theuserdetails = await customer.query()
     .select('customers.id',
             'customers.password_reset_on')
     .where('customers.username',req.body.username) 
     .andWhere('customers.is_deleted', false)
     .andWhere('customers.is_enabled', true)   
      res.send(theuserdetails);
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }); 

router.post("/validate-patient", async (req, res, next) => { 
  let _checkIfPatientIsExistOrNot = false;
  try {
    const patientExistOrNot = await customer.query()
    .where('customers.username', req.body.email.trim())
    .andWhere('parent_account_id', req.body.parent_account_id)
    .andWhere('is_deleted', false).andWhere('is_enabled', true)
    // let isExit = patientExistOrNot.filter(x => x.parent_account_id === req.body.parent_account_id);
    // if(isExit.length > 0) {
    //   _checkIfPatientIsExistOrNot = true;
    // }
    if(patientExistOrNot.length > 0 ) {
      _checkIfPatientIsExistOrNot = true;
    }
    res.send({ res: _checkIfPatientIsExistOrNot });
  } catch(Exception) {
    res.send(
      JSON.stringify({
        message: Exception.message,
        stack: Exception.stack
      })
    );
  }
});

/*
  Added By   : Varshit Shah
  Added Date : 14/04/2020
  Description: Schedule Notifications
*/
router.post("/SaveScheduleconsult", async (req, res, next) => {
  const ScheduleconsultObject = {
    patient_id : req.body.patient_id,
	  account_id :req.body.account_id, 
	  box_id : req.body.box_id, 
	  notification_type_id : req.body.notification_type_id, 
	  order_id : req.body.order_id, 
	  schedule : req.body.schedule 
  }
  try{
    const ntf = await notifications.query().insertGraphAndFetch(ScheduleconsultObject);
    res.send(
      JSON.stringify({
        notifications : ntf
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
}); 

/*
  Added By   : Varshit Shah
  Added Date : 15/04/2020
  Description: Schedule Consultation report
*/
router.post("/getScheduleConsult", async (req, res, next) => {
  try{
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
     
      const TransactionDetail = await notifications.query()
      .innerJoin('prescriptions_master', 'prescriptions_master.id', 'notifications.order_id')
      .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
      .innerJoin('accounts', 'accounts.id','customers.parent_account_id')
      .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
      .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
      //.innerJoin('users','events.subject_id','users.id')
      //.innerJoin('event_type','event_type.id','events.type_id')
      .leftJoin('addresses','addresses.id','boxes.address_id')
      .leftJoin('customer_contacts','customer_contacts.customer_id','customers.id')
      .leftJoin('contacts','contacts.id','customer_contacts.contact_id')
      .leftJoin('box_type','box_type.id','boxes.type_id')
      //.where('prescriptions_master.type_id',RXSTATUS.STOCKED)
      //.where ('event_type.name',EventType.STOCKED)
      .andWhere(
        function()
        {
          if(req.body.store_ids.length > 0)
          {
            this.where('accounts.id', 'in', req.body.store_ids)
          }
          else{
            this.where('accounts.id', 'in', accountIds.map(x => x.id))
          }
          if(req.body.box_ids.length > 0)
          {
            this.where('boxes.id', 'in', req.body.box_ids)
          }
          else
          {
            this.where('boxes.id', 'in',boxes.map(x=>x.id))
           }
            if(req.body.fromdate !== ""){
              this.whereRaw('??::date >= ?', ['prescriptions_master.stocked_date', req.body.fromdate])
            }
            if(req.body.todate !== ""){
              this.whereRaw('??::date <= ?', ['prescriptions_master.stocked_date', req.body.todate])
            }
        }
      )
      .andWhere(function () {
        if(req.body.searchtext !== "")
        {
          this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.bin_id', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.rx_no', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.stock_code', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.pucode', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.name', 'like', '%' + req.body.searchtext + '%')
          //this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.ndc_code', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescription_detail.drug_name', 'like', '%' + req.body.searchtext + '%')
          //this.orWhere('users.name', 'like', '%' + req.body.searchtext + '%')
        }
      })
      .select(
        'accounts.id as accid'
        ,'accounts.name as store'
        ,'boxes.id as boxid'
        , 'boxes.name as box'
        ,'prescriptions_master.id as pmid'
        , 'prescriptions_master.bin_id as binid'
        , 'prescription_detail.rx_no as rxno'
        , 'notifications.schedule as schedule'
        , 'prescriptions_master.stocked_date as stockeddate'
        , 'prescriptions_master.stock_code'
        , 'prescriptions_master.pucode as pucode'
        ,'customers.name as name'
        // , 'customers.last_name as lastname'
        , 'prescription_detail.ndc_code as ndccode'
        , 'prescription_detail.drug_name as drugname'
        , 'prescription_detail.qty as qty'
        // , 'prescription_detail.price as price'
        // ,'users.name as username'
        ,'addresses.address_line_1 as boxaddress_line_1'
        ,'addresses.address_line_2 as boxaddress_line_2'
        ,'addresses.city as boxcity'
        ,'addresses.state as boxstate' 
        ,'addresses.zip as boxzip' 
        ,'boxes.kiosk_box_id'
        ,'customers.dob'
        ,'customers.dob_str'
        ,'box_type.name as box_type'
        ,'contacts.email'
        ,'contacts.phone'
        ,'prescriptions_master.customer_id'
        ,'accounts.picture_url'
      )
      .orderBy(
        'notifications.created_on','desc'
      );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
});
//TASK DI-1037- Schedule Notification | Added by Varshit | 15-04-2020
router.post("/UpdateScheduleNotification", async (req, res, next) => {
  try {
      let scheduleResonse;
      const notificationID = await notifications.query().select('id').where('id', req.body.id).first();
      if (notificationID && notificationID.id) {
          const Obj = {
              id: notificationID.id,
              is_read: true
          }
          scheduleResonse = await notifications.query().upsertGraphAndFetch(Obj);
      }
      res.send(
          JSON.stringify({
              res: scheduleResonse
          })
      );
  }
  catch (error) {
      res.send(
          JSON.stringify({
              message: error.message,
              stack: error.stack
          })
      );
  }
});
router.get("/locationtypes", async (req, res, next) => {
  const locationtypes = await LocationTypeMaster.query();
  res.send(locationtypes);
});

async function GeneratePayNowLink(orderId, parentAccountId, transactionTypeId, brandId) {
  let _payNow = '';
  try {
    if (transactionTypeId === RXSTATUS.STOCKED || transactionTypeId === RXSTATUS.ASSIGNED || transactionTypeId === RXSTATUS.AWAITING) {
      const order = await prescriptions_master.query().where('id', orderId).eager("[items, customer]").first();
      const payment = await paymentModel.query().where('customer_id', order.customer_id).andWhere('order_id', orderId).first();
      const isPayentFacilityAllowed = await GbpConfig.query().where('parent_account_id', parentAccountId).first();
      const _accounts = await account.query().where('id', parentAccountId).first();
      const _accountBrand = await brand.query().where('id', brandId).first();
      let pharmacyEmail = '', pharmacyContactNo = '';
      if (_accountBrand && _accountBrand !== null) {
        pharmacyEmail = _accountBrand.email;
        pharmacyContactNo = _accountBrand.phone
      }
      /*
        If pharmacist has enabled the contactless pickup 
       */
      if (_accounts && _accounts.isContactLessPickupEnabled === true) {
        // allow_payment
        if (isPayentFacilityAllowed && isPayentFacilityAllowed !== null) {
          const _paymentBaseUrl = process.env.PAYMENT_BASE_URL;
          let isPaid = payment && payment.is_paid || false;
          if (isPayentFacilityAllowed.allow_payment === true) {
            const _payNowLink = isPaid === false ? `${_paymentBaseUrl}?q=${orderId}` : `${_paymentBaseUrl}?q=${orderId}&contactless`;
            let _payNowTemplate = `
              <tr>
                <td valign='top' style='padding: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: flex;align-items: center;justify-content: center;'>
                  <p style='font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;'>
                    ${
                      isPaid === false ? 
                      `If you would like to pay for it now, please click on&nbsp;<a target='_blank' style="text-decoration:none;color:#819695;" href='${_payNowLink}'>
                      <b>Pay Now</b></a>&nbsp;and we can take care of that for you. This saves you time at the kiosk when picking up.` 
                      : 
                      `Have you tried Contactless pick up? This saves you time at the kiosk by checking in ahead of time. 
                      Click on&nbsp;<a target='_blank' style="text-decoration:none;color:#819695;" href='${_payNowLink}'><b>Go Contactless</b></a>&nbsp;to get started.`
                    }
                  </p>
                </td>
              </tr>
              <tr>
                ${isPaid === false ? `
                  <td valign='top' style='padding: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <a style='color: #fff; background-color: #2F5597; border-color: #2F5597;box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
                      position: relative; margin-bottom: .3125rem; text-decoration: none; letter-spacing: 0; cursor: pointer; border: 0; outline: 0;
                      transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);
                      will-change: box-shadow, transform;padding: .46875rem 1rem; font: 400 14px Arial;' 
                      target='_blank' href='${_payNowLink}'>
                        Pay Now
                    </a>
                  </td>
                ` 
                : 
                `
                  <td valign='top' style='padding: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                    <a style='color: #fff; background-color: #2F5597; border-color: #2F5597;box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
                      position: relative; margin-bottom: .3125rem; text-decoration: none; letter-spacing: 0; cursor: pointer; border: 0; outline: 0; 
                      transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1); 
                      will-change: box-shadow, transform;padding: .46875rem 1rem; font: 400 14px Arial;' 
                      target='_blank' href='${_payNowLink}'>
                        Go Contactless
                    </a>
                  </td>
              `}
              </tr>
              <tr>
                <td valign='top' style='padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                  <p style='font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;'>
                    If you have any questions or concerns contact us at
                      &nbsp;<a href='mailto:${pharmacyEmail}'
                        style='color:#819695;'>${pharmacyEmail}</a>
                        ${(pharmacyContactNo !== undefined && pharmacyContactNo !== '') ? `&nbsp;or you can reach us at ${pharmacyContactNo}` : ''}
                  </p>
                </td>
              </tr>
            `;
            _payNow = _payNowTemplate;
          } else {
            const _paymentBaseUrl = process.env.PAYMENT_BASE_URL;
            const _payNowLink = `${_paymentBaseUrl}?q=${orderId}&contactless&direct`;
            let _payNowTemplate = `
              <tr>
                <td valign='top' style='padding: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: flex;align-items: center;justify-content: center;'>
                  <p style='font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;'>
                      Have you tried Contactless pick up? This saves you time at the kiosk by checking in ahead of time. Click on&nbsp;<a target='_blank' style="text-decoration:none;color:#819695;" href='${_payNowLink}'>
                      <b>Go Contactless</b></a>&nbsp;to get started.
                  </p>
                </td>
              </tr>
              <tr>
                <td valign='top' style='padding: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                  <a style='color: #fff; background-color: #2F5597; border-color: #2F5597;box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
                    position: relative; margin-bottom: .3125rem; text-decoration: none; letter-spacing: 0; cursor: pointer; border: 0; outline: 0;
                    transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);
                    will-change: box-shadow, transform;padding: .46875rem 1rem; font: 400 14px Arial;' 
                    target='_blank' href='${_payNowLink}'>
                      Go Contactless
                  </a>
                </td>
              </tr>
              <tr>
                <td valign='top' style='padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                  <p style='font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;'>
                    If you have any questions or concerns contact us at
                      &nbsp;<a href='mailto:${pharmacyEmail}'
                        style='color:#819695;'>${pharmacyEmail}</a>
                        ${(pharmacyContactNo !== undefined && pharmacyContactNo !== '') ? `&nbsp;or you can reach us at ${pharmacyContactNo}` : ''}
                  </p>
                </td>
              </tr>
            `;
            _payNow = _payNowTemplate;
          }
        } else {
          const _paymentBaseUrl = process.env.PAYMENT_BASE_URL;
          const _payNowLink = `${_paymentBaseUrl}?q=${orderId}&contactless&direct`;
          let _payNowTemplate = `
            <tr>
              <td valign='top' style='padding: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                <a style='color: #fff; background-color: #2F5597; border-color: #2F5597;box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
                  position: relative; margin-bottom: .3125rem; text-decoration: none; letter-spacing: 0; cursor: pointer; border: 0; outline: 0;
                  transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);
                  will-change: box-shadow, transform;padding: .46875rem 1rem; font: 400 14px Arial;'
                  target='_blank' href='${_payNowLink}'>
                    Go Contactless
                </a>
              </td>
            </tr>
            <tr>
              <td valign='top' style='padding-right: 15px;padding-left: 15px;padding-top:15px;padding-bottom:30px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
                <p style='font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;'>
                  If you have any questions or concerns contact us at
                    &nbsp;<a href='mailto:${pharmacyEmail}'
                      style='color:#819695;'>${pharmacyEmail}</a>                    
                </p>
              </td>
            </tr>
          `;
          _payNow = _payNowTemplate;
        }
      } else {
        let _payNowTemplate = `
          <tr>
            <td valign='top' style='padding-left: 15px;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;'>
              <p style='font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height: 18px; font-weight: normal; text-align: left; color:#819695; margin:0;margin-top: -15px;'>
                If you have any questions or concerns contact us at
                  &nbsp;<a href='mailto:${pharmacyEmail}'
                    style='color:#819695;'>${pharmacyEmail}</a>                    
              </p>
            </td>
          </tr>
        `;
        _payNow = _payNowTemplate;
      }
    }
  } catch (error) {
    _payNow = '';
  }
  return _payNow;
}

//TASK DI-1094 | Added by Varshit | 29-05-2020
router.post("/getCustomerLocation", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    try {
      let response;
      const CustomerID = await customer.query().select('id').where('patient_id', req.body.patientID).andWhere('parent_account_id' , req.body.accountID).andWhere('is_deleted', false).andWhere('is_enabled', true).first();
      if (CustomerID && CustomerID.id) {
          response = await Customer_Locations_master.query().where('customer_id' , CustomerID.id);
      }
      res.send(
          JSON.stringify({
              res: response
          })
      );
  }
  catch (error) {
      res.send(
          JSON.stringify({
              message: error.message,
              stack: error.stack
          })
      );
  }
}
});
router.get("/boxes/:box_id/token", async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      res.status(401).send({
        message: 'You are not authorized to perform this action request.'
      })
    } else {
      // Verify AWS token - Pending.
      // Verify Box_id permission  - Pending.
      let box_id = req.params.box_id;
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      let payload = {
        username: decodedToken.email,
        box_id: box_id
      }
      const pollToken = signAuth(payload);
      res.status(200).send({
        token: pollToken
      })
    }
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//TASK DI-1152 | Added by Varshit | 11-06-2020
router.post("/emailPickupNotification", async (req, res, next) => {
  try{

    if (!req.headers.authorization) {
      next();
    }
    else
    {
      // const name = await box.query().select('name').where('id', req.body.box_id).first();
      let boxName = req.body.AccountName; 
      let boxAddress = req.body.AccountAddress ;
      let boxAddress2 = req.body.AccountAddress2 ;
      let orderLocationTemplate = `
      <td width = '60%'  style='text-align: left; line-height: 2px'>
      <p style='font-family: Helvetica, Arial, sans-serif;font-size:16px;line-height: 25px; font-weight: normal; text-align: left; color:#2F5597; margin:0;'>
        Your order is located at <b> ${boxName}</b>
      </p>
      <p style='font-family: Helvetica, Arial, sans-serif;font-size:16px;line-height: 25px; font-weight: normal; text-align: left; color:#2F5597; margin:0;'>
      ${boxAddress}
      </p>
      <p style='font-family: Helvetica, Arial, sans-serif;font-size:16px;line-height: 25px; font-weight: normal; text-align: left; color:#2F5597; margin:0;'>
      ${boxAddress2}
      </p>
    </td>
`
    let config = {
        emailId : req.body.emailID,
        firstName : req.body.firstName,
        lastName :  req.body.lastName,
        orderLocation : orderLocationTemplate,
        pickupcode : req.body.pucode
    }; 
    let s3response = { Location: '' };
    config.qrCode = await QRCode.toDataURL(req.body.pucode);
    const type = config.qrCode.split(';')[0].split('/')[1];
    const base64Data = new Buffer(
      config.qrCode.replace(/^data:image\/\w+;base64,/, ''),
      'base64'
    );
    var params = {
        Bucket: AWSconfig.bucket, // Bucket name
        Key: uuidv4(), // type is not required
        Body: base64Data,
        ACL: 'public-read',
        ContentEncoding: 'base64', // required
        ContentType: `image/${type}` // required. Notice the back ticks
    };
    s3response = await s3.upload(params).promise();
    config.qrCode = s3response.Location;

    sendNewPickupNotification(config)
    res.send('Email Sent') 
    // return config; 
    }
  }
  catch(error)
  {
    res.send(error);
  }
});

function parseDate(date) {
  let day, month, year, finalDate;
  if(date) {
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();
    finalDate = (month < 10 ? '0' + month : '' + month) + '/' + day + '/' + year;
  } else {
    date = new Date();
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();
    finalDate = (month < 10 ? '0' + month : '' + month) + '/' + day + '/' + year;
  }
  return finalDate;
}

//TASK DI-1182 | Added by Varshit | 16-06-2020
router.get("/ReceiptPickedupnotification", async (req, res, next) => {
  let order_id = req.query.order_id;
  try {
    const receiptOrder = await prescriptions_master.query()
      .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
      .innerJoin('customers', 'customers.id', 'prescriptions_master.customer_id')
      .innerJoin('accounts', 'accounts.id', 'boxes.parent_account_id')
      .innerJoin('brands', 'brands.id', 'accounts.brand_id')
      .where('prescriptions_master.id', order_id)
      .select(
        'accounts.picture_url',
        'brands.name as PharmacyName',
        'brands.phone as pharmacyPhone',
        'boxes.name as boxName',
        'prescriptions_master.pucode as PickupCode',
        'customers.username as emailID',
        'customers.id as customer_id'
      ).first();
    const orders = await prescription_detail.query().where('prescription_detail.parent_account_id', order_id);
    const payment = await paymentModel.query().where('customer_id', receiptOrder.customer_id).andWhere('order_id', order_id).first();
    const ParsepaymentDetails = payment && JSON.parse(payment.receipt);
    let paymentDetail;
    if(ParsepaymentDetails !== null && ParsepaymentDetails !== 'null' && ParsepaymentDetails !=''){
    const paymentDetailsobj = ParsepaymentDetails && ParsepaymentDetails.labeled_attributes;
    let payamount  = paymentDetailsobj && paymentDetailsobj.total_amount && paymentDetailsobj.total_amount.value; 
    let totalpaidamount = payamount && payamount.replace("USD","");
    paymentDetail = {
      ReceiptNumber : ParsepaymentDetails && ParsepaymentDetails.id,
      Batch : paymentDetailsobj && paymentDetailsobj.batch_number && paymentDetailsobj.batch_number.value,
      TransID: paymentDetailsobj && paymentDetailsobj.transaction_id && paymentDetailsobj.transaction_id.value,
      OrderID: paymentDetailsobj && paymentDetailsobj.reference_id && paymentDetailsobj.reference_id.value,
      TransType: paymentDetailsobj && paymentDetailsobj.transaction_type && paymentDetailsobj.transaction_type.value,
      Date_Time: paymentDetailsobj && paymentDetailsobj.transaction_datetime && paymentDetailsobj.transaction_datetime.value,
      CardType: paymentDetailsobj && paymentDetailsobj.card_type && paymentDetailsobj.card_type.value,
      CardNumber: paymentDetailsobj && paymentDetailsobj.masked_card_number && paymentDetailsobj.masked_card_number.value,
      EntryMethod: paymentDetailsobj && paymentDetailsobj.entry_method && paymentDetailsobj.entry_method.value,
      ApprovalCode: paymentDetailsobj && paymentDetailsobj.approval_code && paymentDetailsobj.approval_code.value,
      TotalAmount : totalpaidamount,        
    }
  }else{
    paymentDetail = {
      ReceiptNumber : '',
      Batch : payment && payment.batch_no,
      TransID: payment && (payment.transaction_id || payment.trans_id),
      OrderID: payment && payment.reference_id,
      TransType: '',
      Date_Time: payment && payment.payment_date ? moment(payment.payment_date).format('MM/DD/YYYY HH:mm:ss') : '',
      CardType: payment && payment.card_type,
      CardNumber: payment && (payment.masked_card_number || payment.account_no),
      EntryMethod: payment && payment.entry_type,
      ApprovalCode: payment && payment.approved_code,
      TotalAmount : payment && payment.approved_amt ? '$' + parseFloat(payment.approved_amt).toFixed(2) : '$0.00',        
    } 
  }

    let config = {
      emailId: receiptOrder.emailID,
      picture_url: receiptOrder.picture_url,
      pharmacyName: receiptOrder.PharmacyName,
      pharmacyPhone : receiptOrder.pharmacyPhone,
      pickUpCode: receiptOrder.PickupCode,
      location: receiptOrder.boxName,
      Date: payment && parseDate(payment.payment_date),
      terminal_id : paymentDetail.Batch,
      transaction_id : paymentDetail.TransID ,
      receipt_number : paymentDetail.ReceiptNumber,
      transaction_type : paymentDetail.TransType,
      transaction_date_time : paymentDetail.Date_Time,
      card_type: paymentDetail.CardType,
      card_number: paymentDetail.CardNumber,
      entry_method: paymentDetail.EntryMethod,
      approval_code: paymentDetail.ApprovalCode,
      total_amount: paymentDetail.TotalAmount,
      orderHtml: ''
    };

    let TotalAmount = 0;
    (orders).map(function (item) {
      let ttlAmount = item.price;
      TotalAmount = TotalAmount + ttlAmount;
      ttlAmount = JSON.stringify(ttlAmount);

      config.orderHtml += '<tr><td valign="top" style="border-bottom:1px solid #e5e0de;padding-right:0;padding-left:0;padding-top:0;padding-bottom:0;text-align:center"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;min-width:100%;border-collapse:collapse"><tbody><tr><td valign="top" style="padding-right:10px;width:60%;padding-left:10px;padding-top:10px;padding-bottom:10px;text-align:center"><p style="font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:18px;font-weight:normal;text-align:left;color:#819695;margin:0">'
        + item.rx_no
        + '</p></td><td valign="top" width="160" style="padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px;text-align:center"><p style="font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:18px;font-weight:normal;text-align:left;color:#819695;margin:0">'
        + item.qty
        + '</p></td><td valign="top" width="70" style="padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px;text-align:center"><p style="font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:18px;font-weight:normal;text-align:left;color:#819695;margin:0">$'
        + item.price + '.00'
        + '</p></td></tr></tbody></table></td></tr>'
    });

    config.orderTotalHtml =
      '<tr><td valign="top" style="border-bottom:1px solid #e5e0de;padding-right:0;padding-left:0;padding-top:0;padding-bottom:0;text-align:center"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;min-width:100%;border-collapse:collapse"><tbody><tr><td valign="top" style="padding-right:10px;width:60%;padding-left:10px;padding-top:10px;padding-bottom:10px;text-align:center"><p style="font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:18px;font-weight:normal;text-align:left;color:#819695;margin:0">Total'
      + '</p></td><td valign="top" width="160" style="padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px;text-align:center"><p style="font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:18px;font-weight:normal;text-align:left;color:#819695;margin:0"></p></td><td valign="top" width="70" style="padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px;text-align:center"><p style="font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:18px;font-weight:normal;text-align:left;color:#819695;margin:0">$'
      + TotalAmount + '.00</p></td></tr></tbody></table></td></tr>'

    config.orderLocation =
      '<td style="width:60%;text-align:left;line-height:2px"><p style="font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:25px;font-weight:normal;text-align:left;color:#2F5597;margin:0">'
      + 'Your order is located at  <b>' + config.boxName
      + '</b></p><p style="font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:25px;font-weight:normal;text-align:left;color:#2F5597;margin:0">'
      + config.boxAddress + '</p></td>'

    sendPickUpReceipt(config)
    res.send('Email Sent')
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//  Get iPay Transactions Report Data | ADDED BY: Varshit Shah | TASK: DI-1184
router.post("/getiPayTransaction", async (req, res, next) => {
  try
  {
    if (!req.headers.authorization) {
      next();
    }
    else {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
  
      const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
      const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
      const boxes = await box.query().select('id').where('parent_account_id', 'in', accountIds.map(x => x.id))
      const TransactionDetail = await paymentModel.query()
          .innerJoin('customers', 'customers.id', 'payments.customer_id')
          .innerJoin('accounts',  'accounts.id','customers.parent_account_id')
          .innerJoin('prescriptions_master', 'prescriptions_master.id', 'payments.order_id')
          .innerJoin("boxes", 'boxes.id', 'prescriptions_master.box_id')
          .innerJoin("prescription_detail", 'prescription_detail.parent_account_id', 'prescriptions_master.id')
          .andWhere(
            function()
            {
              if(req.body.store_ids.length > 0)
              {
                this.where('accounts.id', 'in', req.body.store_ids)
              }
              else{
                this.where('accounts.id', 'in', accountIds.map(x => x.id))
              }
              if(req.body.box_ids.length > 0)
              {
                this.where('boxes.id', 'in', req.body.box_ids)
              }
              else
              {
                this.where('boxes.id', 'in',boxes.map(x=>x.id))
               }
                if(req.body.fromdate !== ""){
                  this.whereRaw('??::date >= ?', ['payments.payment_date', req.body.fromdate])
                }
                if(req.body.todate !== ""){
                  this.whereRaw('??::date <= ?', ['payments.payment_date', req.body.todate])
                }
            }
          )
          .andWhere(function () {
            if(req.body.searchtext !== "")
            {
              this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('accounts.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('boxes.name', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.sales_id', 'like', '%' + req.body.searchtext + '%')
              // this.orWhere('payments.approved_amt', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.approved_code', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.card_type', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.card_brand', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.entry_type', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.trans_status', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.masked_card_number', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.currency_code', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.reference_id', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('payments.payment_type', 'like', '%' + req.body.searchtext + '%')
              this.orWhere('prescriptions_master.pucode', 'like', '%' + req.body.searchtext + '%')
            }
          })
          .select('customers.first_name as firstname', 
                  'customers.last_name as lastname', 
                  'accounts.name as store', 
                  'boxes.id as boxid', 
                  'boxes.name as box',
                  'payments.sales_id as transaction_id',
                  'payments.payment_date',
                  'payments.approved_amt',
                  'payments.approved_code',
                  'payments.card_type',
                  'payments.card_brand',
                  'payments.entry_type',
                  'payments.exp_month',
                  'payments.exp_year',
                  'payments.trans_status',
                  'payments.masked_card_number',
                  'payments.currency_code',
                  'payments.reference_id',
                  'payments.payment_type',
                  'payments.receipt',
                  'payments.links',
                  'prescriptions_master.pucode as pucode',
                  'payments.batch_no',
                  'payments.batch_amt',
                  'payments.result_message',
                  'payments.account_no',
                  'payments.avs_response_code',
                  'payments.receipt_text',
                  'payments.trans_id',
                  'payments.host_response_description'
          )
          .orderBy(
            'payments.payment_date','desc'
          );
        res.send(TransactionDetail);
    }
  }
  catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  } 
   
  });
 //TASK DI-748- Last Login | Added by Varshit | 03-07-2020
router.post("/UpdateLastLoginDateTime", async (req, res, next) => {
  try {
    let userResonse;
    const USERID = await user.query().select('id').where('username', req.body.username).first();
    if (USERID && USERID.id) {
        const UserObject = {
            id: USERID.id,
            last_login_on: new Date()
        }
        userResonse = await user.query().upsertGraphAndFetch(UserObject);
        const userParentAccount = await user.query().where('username', req.body.username).eager('[parentAccount]').first();
        userResonse.parentAccount = userParentAccount;
    }
    res.send(
        JSON.stringify({
            res: userResonse
        })
    );
}
catch (error) {
    res.send(
        JSON.stringify({
            message: error.message,
            stack: error.stack
        })
    );
}
}); 
function getDateTime(dateStr) {
  // let date = new Date(dateStr);
  // return date.getMonth() + 1 +
  //   '/' + pad(date.getDate()) +
  //   '/' + pad(date. getFullYear()) +
  //   ' at ' + pad(date.getHours()) +
  //   ':' + pad(date.getMinutes())
  const mDate = moment(dateStr);
  // return `${mDate.format('MM/DD/YYYY')} at ${mDate.format('hh:mm:ss A')} UTC (${mDate.fromNow()})`;
  return `${mDate.format('MM/DD/YYYY')} at ${mDate.format('hh:mm:ss A')} UTC`;
};

/*
  Added By   : Varshit Shah
  Added Date : 29/07/2020
  Description: DI-1075-Check last 15 password - save Password
*/
router.post("/savepassword", async (req, res, next) => {
  const Object = {	  	  	  
    user_id : req.body.userid,
    password_hash : req.body.password  
  }
  try{
    const ph = await user_password_history.query().insertGraphAndFetch(Object);
    const totalrecords = await user_password_history.query().where('user_password_history.user_id',req.body.userid).orderBy('user_password_history.created_on','asc');
    if (totalrecords && totalrecords.length > 15) {
      const deleterecord = await user_password_history.query().deleteById(totalrecords[0].id);
    }
    res.send(
      JSON.stringify({
        password : ph
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
 //Get user password history from userid | Added by Varshit | 30-07-2020
 router.get("/getLastPasswords/:id/:password", async (req, res, next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
    let PW_Match = false;
     const  thePasswordHistory = await user_password_history.query()
     .select('user_password_history.id',
             'user_password_history.user_id',
             'user_password_history.password_hash',
             )
     .where('user_password_history.user_id',req.params.id)
     .andWhere('user_password_history.password_hash',req.params.password)
     .orderBy(
      'user_password_history.created_on','desc'
    )
    if(thePasswordHistory.length > 0){
      PW_Match = true;
    }else{
      PW_Match = false
    }
    res.send({ res: PW_Match });
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
 });
 router.post("/savePatientpassword", async (req, res, next) => {
  const Object = {	  	  	  
    customer_id : req.body.patientid,
    password_hash : req.body.password
  }
  try{
    const ph = await customer_password_history.query().insertGraphAndFetch(Object);
    const totalrecords = await customer_password_history.query().where('customer_password_history.customer_id',req.body.req.body.patientid).orderBy('customer_password_history.created_on','asc');
    if (totalrecords && totalrecords.length > 15) {
      const deleterecord = await customer_password_history.query().deleteById(totalrecords[0].id);
    }
    res.send(
      JSON.stringify({
        password : ph
      })
    );
  }catch(error){
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
 //Get Patient password history from Patientid | Added by Varshit | 30-07-2020
 router.get("/getPatientLastPasswords/:id/:password", async (req, res, next) => {
  if(req.params.id !== '' && req.params.id !== null && req.params.id !== 'null' && req.params.id !== undefined){
    try{
      let PW_Match = false;
     const  thePasswordHistory = await customer_password_history.query()
     .select('customer_password_history.id',
             'customer_password_history.customer_id',
             'customer_password_history.password_hash',
             )
     .where('customer_password_history.customer_id',req.params.id)
     .andWhere('customer_password_history.password_hash',req.params.password)
     .orderBy(
      'customer_password_history.created_on','desc'
    )  
    if(thePasswordHistory.length > 0){
      PW_Match = true;
    }else{
      PW_Match = false
    }
    res.send({ res: PW_Match });
    }
    catch(error)
    {
      res.send(
        JSON.stringify({
          message: error.message,
          stack: error.stack
        })
      );
    }
  }
 });

router.post("/disable/patient", async (req, res, next) => {
  try {
    const test = await customer.query()
      .update({ is_enabled: req.body.is_enabled })
      .where('id', req.body.id);

    const cognitoParam = {
      Username: req.body.id,
      UserPoolId: process.env.PATIENT_POOL_ID
    };
    if (req.body.is_enabled === true) {
      await cisp.adminEnableUser(cognitoParam).promise();
    } else {
      await cisp.adminDisableUser(cognitoParam).promise();
    }
    res.send(
      JSON.stringify({
        user: test
      })
    );
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});

// Created By: Amit Maurya | Created Date: 09/03/2020 | Task ID: DI-899: Pick Up all orders: The ability to retrieve other orders for me off one pick up code
router.post("/customers/all/orders-by-customer-id", async (req, res, next) => {
  try {
    let customer_id = req.body.customer_id;
    let box_id = req.body.box_id;
    let allData = await prescriptions_master.query()
      .innerJoin('prescription_detail', 'prescription_detail.parent_account_id', 'prescriptions_master.id')
      .innerJoin('boxes', 'boxes.id', 'prescriptions_master.box_id')
      .where('prescriptions_master.customer_id', customer_id)
      .andWhere('prescriptions_master.box_id', box_id)
      .andWhere('prescriptions_master.type_id', RXSTATUS.STOCKED).eager('[items]').groupBy('prescriptions_master.id');
    if (allData && allData.length > 0) {
      for (let i = 0; i < allData.length; i++) {
        let tmp = allData[i].items;
        let totalPrice = 0;
        for (let j = 0; j < tmp.length; j++) {
          totalPrice = totalPrice + tmp[j].price;
        }
        allData[i].totalPrice = totalPrice;
        allData[i].chkEnable = true;
      }
    }
    res.send(allData);
  }
  catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }
});
//TASK DI-1216-Search in Delivery Prescription | Added by Varshit | 09-23-2020
router.post("/getPrescriptionSearchResult", async (req, res, next) => {
  if (!req.headers.authorization) {
    next();
  }
  else {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.decode(token);
    const u = await user.query().where('username', decodedToken.email).eager("[parentAccount]").first();
    const accountIds = await account.query().select('id').where('account_lineage', '<@', u.parentAccount.account_lineage);
    const boxes = await box.query().select('parent_account_id').where('parent_account_id', 'in', accountIds.map(x => x.id))
   let getSearchResult=[];
   getSearchResult = await customer.query()
        .innerJoin("customer_contacts","customer_contacts.customer_id","customers.id")
        .innerJoin("contacts","customer_contacts.contact_id","contacts.id")
        .innerJoin("prescriptions_master","prescriptions_master.customer_id","customers.id")
        .innerJoin("boxes","boxes.id","prescriptions_master.box_id")
        .skipUndefined().where('boxes.IsActive', true)
        .andWhere('customers.parent_account_id', 'in' ,boxes.map(x=>x.parent_account_id))
        .andWhere('prescriptions_master.type_id','!=',RXSTATUS.PICKED_UP)
        .andWhere('prescriptions_master.type_id', '!=',RXSTATUS.REMOVE_RX)
        .andWhere('customers.is_deleted', false).andWhere('customers.is_enabled', true)
        .andWhere(function () {
          this.orWhere('customers.first_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('customers.last_name', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('contacts.email', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('contacts.phone', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.stock_code', 'like', '%' + req.body.searchtext + '%')
          this.orWhere('prescriptions_master.pucode', 'like', '%' + req.body.searchtext + '%')
        })
        .select(
          "customers.first_name"
          ,"customers.last_name"
          ,"customers.dob_str"
          ,"contacts.email"
          ,"contacts.phone"
          ,"prescriptions_master.id as pm_id"
          ,"prescriptions_master.bin_id"
          ,"prescriptions_master.stock_code"
          ,"prescriptions_master.pucode"
          ,"prescriptions_master.box_id"
          ,"prescriptions_master.type_id"
        )
        .limit(5)
      res.send(getSearchResult); 
    }
});

router.post('/patient-exist', async (req, res, next) => {
  let patientId = req.body.patientId;
  const response = {}
  try {
    const patient = await customer.query().where('patient_id', patientId).andWhere('is_deleted', false).andWhere('is_enabled', true).first();
    if(patient) {
      response.isPatientExist = true;
    } else {
      response.isPatientExist = false;
    }
    res.send(response);
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack
      })
    );
  }  
})

export default router;
