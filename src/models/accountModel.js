const Model = require("./model").default;

export default class Account extends Model {
  static get tableName() {
    return "accounts";
  }

  static get idColumn() {
    return "id";
  }

  // static get idColumn(){
  //     return 'id';
  // }
}
