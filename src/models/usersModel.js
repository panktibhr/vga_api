const Model = require('../models/model').default;
const UserType = require('../models/usersTypeModel').default;

export default class Users extends Model {
    static get tableName() {
        return 'users';
    }

    static get idColumn() {
        return 'id';
    }

    static get last_login_onColumn() {
        return 'last_login_on';
    }

    static get emailColumn() {
        return 'email';
    }

    static get passwordColumn() {
        return 'password'
    }
    static get relationMappings() {
        return {
            usersType: {
                relation: Model.BelongsToOneRelation,
                modelClass: UserType,
                join: {
                    from:'user_type.id',
                    to:'users.userstypeid'
                }
            }
        }
    }
}