const express = require("express");
const AccountRoute = express.Router();
let jwt = require("jsonwebtoken");
const Account = require("./../models/accountModel").default;

AccountRoute.use(async (req, res, next) => {
  next();
});

AccountRoute.get("/get-account", async (req, res, next) => {
  console.log("You in get_allAccount Detail GET Method.!");
  let accountDetails = await Account.query()
    .select()
    .where("is_deleted", false);
  try {
    if (accountDetails && accountDetails.length > 0) {
      res.status(200).send({
        success: true,
        account: accountDetails,
      });
    }
  } catch (error) {
    res.status(401).send({
      success: false,
      error: error.message,
      error: error.stack,
    });
  }
});

AccountRoute.post("/account", async (req, res, next) => {
  console.log("You in _post_Account Detail POST Method..!");
  const accountObj = {
    name: req.body.name,
    description: req.body.description,
    email: req.body.email,
    phone: req.body.phone,
    parent_account_id: req.body.parent_account_id,
  };
  let _post_AccountObj = await Account.query().insertGraphAndFetch(accountObj);
  try {
    if (_post_AccountObj && _post_AccountObj.length > 0) {
      res.status(200).send({
        success: true,
        message: "Account created successfully.!",
        account: _post_AccountObj,
      });
    }
  } catch (error) {
    res.status(401).send({
      success: false,
      message: error.message,
      stack: error.stack,
    });
  }
});

export default AccountRoute;
