const express = require("express");
const index = express.Router();
const Users = require("./UsersController").default;
const Auth = require("./AuthController").default;
const Account = require("./AccountController").default;

index.use(async (req, res, next) => {
  next();
});

index.get("/", async (req, res, next) => {
  res.send("Welcome to Vga Task.");
});

const router = (app) => {
  app.use("/", index);
  app.use("/users", Users);
  app.use("/auth", Auth);
  app.use("/account", Account);
};

export default router;
