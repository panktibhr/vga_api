const express = require('express');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const AuthRoute = express.Router();
let jwt = require('jsonwebtoken');
const users = require('../models/usersModel').default;

AuthRoute.use(async (req, res, next) => {
    next();
})

AuthRoute.post('/login', async (req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;
    let u = await users.query().skipUndefined().where('username', username).where('password', password).eager('[usersType]').first();
    if (username && password && u) {
        if (username === u.username && password === u.password) {
            let token = jwt.sign({ username: u.username },
                process.env.SECRET,
                {
                    expiresIn: '24h'
                }
            );
            res.json({
                success: true,
                message: 'Authentication successful!',
                token: token,
                user: u
            });
        } else {
            res.send(403).json({
                success: false,
                message: 'Incorrect username or password'
            });
        }
    } else {
        res.send(400).json({
            success: false,
            message: 'Authentication failed! Please check the request'
        });
    }
})

AuthRoute.post('/forgot-password', async (req, res, next) => {
    let mail_id = req.body.email;
    let data = await users.query().where('email', mail_id);
    let user_id;


    try {
        if (data.length > 0) {
            // console.log(data);
            data.forEach(id => {
                user_id = id.id;
            });
            console.log(user_id);

            let smtpTransport = nodemailer.createTransport({
                type: 'oauth2',
                host: "smtp.gmail.com",
                port: 587,
                secure: false,
                service: 'gmail', // true for 465, false for other ports
                auth: {
                    user: process.env.AUTH_USERNAME, // generated ethereal user
                    pass: process.env.AUTH_PASSWORD, // generated ethereal password
                },
                tls: true,
            })
            // console.log(user_id);
            let info = await smtpTransport.sendMail({
                from: 'nikeshthakkar1998@gmail.com', // sender address
                to: mail_id, // list of receivers
                subject: "Hello ✔", // Subject line
                text: "Hello, Don't Worry Here Is Your Reser Password Link...", // plain text body"
                html: `<h2>Reset Password link : <a href='http://localhost:4200/change-password?id=${user_id}'> click here </a></h2>`, // html body
            });

            console.log("Message sent: ", mail_id);
        } else {
            res.send('Email is invalid..');
        }
    } catch (error) {
        console.log(error);
    }

    res.status(200).send("Message sent successfully.");

});


AuthRoute.post('/update-password:id', async (req, res) => {
    let pass = req.body.password;
    let user_id = req.params.id;
    console.log(user_id);
    let s;

    try {
        s = await users.query().skipUndefined().where('id', user_id).patch({ password: pass });
        console.log(s);
        res.send("Update Password Successfully.");
    } catch (error) {
        console.log('***** error *******',error);
    }
})

export default AuthRoute;