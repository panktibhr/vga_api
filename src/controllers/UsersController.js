import Users from "../models/usersModel";

const express = require("express");
const UsersRoute = express.Router();
const users = require("../models/usersModel").default;
const Account = require("./../models/accountModel").default;
// const validateToken = require('../middleware').default;

UsersRoute.use(async (req, res, next) => {
  next();
});

UsersRoute.get("/", async (req, res, next) => {
  let u;
  try {
    u = await users.query().eager("[usersType]");
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack,
      })
    );
  }
  res.send(u);
});

UsersRoute.post("/update/last-login-date", async (req, res, next) => {
  const user_id = req.body.user_id;
  let t;
  try {
    console.log(user_id);
    t = await users
      .query()
      .patchAndFetchById(user_id, { 'last_login_on': new Date() });
    console.log(t);
  } catch (error) {
    res.send(
      JSON.stringify({
        message: error.message,
        stack: error.stack,
      })
    );
  }
  res.send(t);
});

// UsersRoute.get("/get-account", async (req, res, next) => {
//   console.log("You in get_allAccount Detail GET Method.!");
//   let _get_allAccountObj = await Account.query()
//     .select()
//     .where("is_deleted", false);
//   try {
//     if (_get_allAccountObj) {
//       res.send({
//         success: true,
//         account: _get_allAccountObj,
//       });
//     }
//   } catch (error) {
//     res.send({
//       success: false,
//       error: error.message,
//       error: error.stack,
//     });
//   }
// });

// UsersRoute.post("/account", async (req, res, next) => {
//   console.log("You in _post_Account Detail POST Method..!");
//   const accountObj = {
//     name: req.body.name,
//     description: req.body.description,
//     email: req.body.email,
//     phone: req.body.phone,
//     parent_account_id: req.body.parent_account_id,
//   };
//   let _post_AccountObj = await Account.query().insertGraphAndFetch(accountObj);
//   try {
//     if (_post_AccountObj) {
//       res.send({
//         success: true,
//         message: "Account created successfully.!",
//         account: _post_AccountObj,
//       });
//     }
//   } catch (error) {
//     res.send(
//       JSON.stringify({
//         success: false,
//         message: error.message,
//         stack: error.stack,
//       })
//     );
//   }
// });

export default UsersRoute;
